<?php
class Permission {
    public $permissions = array("read" => FALSE, "write" => FALSE, "delete" => FALSE, "update" => FALSE, "status" => FALSE, "master" => FALSE);

    function getPermissions($bitMask = 0) {
        /*
        *This table shows how bitmasks will represent a particular permission.
        *read		00000001 -- 2^0 -- 1
        *write		00000010 -- 2^1 -- 2
        *delete		00000100 -- 2^2 -- 4
        *update		00001000 -- 2^3 -- 8		
        *status		00001000 -- 2^4 -- 16		
        *master		00010000 -- 2^5 -- 32
        */
        $i = 0;
        foreach ($this->permissions as $key => $value) {
            $this->permissions[$key] = ((32 == $bitMask) OR (($bitMask & pow(2, $i)) != 0)) ? TRUE : FALSE;
            $i++; 				
        }
        return $this->permissions;
    }
    function toBitmask() {
        $bitmask = 0;
        $i = 0;
        foreach ($this->permissions as $key => $value) {
            if ($value) {
                $bitmask += pow(2, $i);
            }
            $i++;	
        }
        return $bitmask;
    }
    function isPermission($permission, $priv) {
        /*
         * $priv = 'read', 'write', 'delete', 'update', 'status'
         * */
        if ($permission[$priv]) return TRUE;
        return FALSE;
    }	
}
// END Permission Class

/* End of file Permission.php */