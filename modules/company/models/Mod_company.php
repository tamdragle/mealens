<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
get_instance()->load->iface('DefaultInterface');
class Mod_company extends MY_Model implements DefaultInterface {
    var $table = 'company';
    var $column = array('cname','cmail','ctel'); //set column field database for order and search
    var $order = array('cid' => 'desc'); // default order 

    //Datatable
    private function _get_datatables_query() {
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column as $item) {//loop column 
            if (@$_POST['search']['value']) {//if datatable send POST for search
                if ($i === 0 ) {//first loop
                    $this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND. 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                //last loop
                if (count($this->column) - 1 == $i) $this->db->group_end(); //close bracket
            }
            $column[$i] = $item; //set column array variable to order processing
            $i++;
        }
        if (isset($_POST['order'])) {//here order processing
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        elseif (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables() {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    function _create($values) {
        if (!$this->db->insert($this->table, array(
            'cname' => $values['name'],
            'cmail' => $values['mail'],
            'ctel' => $values['tel'],
            'caddress' => $values['address']
        ))) return FALSE;
        return TRUE;
    }
    function _update($values) {
        $data = array(
            'cname' => $values['name'],
            'cmail' => $values['mail'],
            'ctel' => $values['tel'],
            'caddress' => $values['address']
        );
        $this->db->where('cid', $values['cid']);
        $this->db->update($this->table, $data);
        return ($this->db->affected_rows()) ? TRUE : FALSE;        
    }
    function _gets($id = FALSE) {
        $this->db->select('*');
        $this->db->from($this->table);
        if ($id) $this->db->where('cid', $id);
        $query = $this->db->get();
        $result = ($id) ? $query->first_row() : $query->result_array();
        $query->free_result();
	return ($result) ? $result : false;
    }
    function _countStaff($id) {
        $this->db->from('member');
        $this->db->where('cid', $id);
        return $this->db->count_all_results();
    }
}
/* End of file */