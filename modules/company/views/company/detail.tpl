{* Extend our master template *}
{extends file="master.tpl"}
{block name=script}
<script src="{$assets_path}plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{$assets_path}plugins/datatables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar_company").addClass("active");
});
function load_person(id) {
    table = $('#table').DataTable({ 

        "processing": true,
        "serverSide": true,
        //"paging": true,
        //"lengthChange": false,
        //"searching": false,
        //"ordering": true,
        //"info": true,
        //"autoWidth": true,
        "order": [],

        "ajax": {
            "url": "{site_url()}company/nhanvien_list?id="+id,
            "type": "POST"
        },
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    });
}
function reload_table() {
    table.ajax.reload(null,false); //reload datatable ajax 
}
function nhanvien(id) {
    $('#saoke').html("<center><i class='ace-icon fa fa-spinner fa-spin orange bigger-125'></i></center>");
    $.get('{site_url()}company/nhanvien?id='+ id, {}, 
    function(response) {
        if (response == 'denied') document.location.href = site_url + 'denied?w=read';
        else $('#saoke').html(response);
    });
}
</script>
{/block}
{block name=body}
<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li><a href="{site_url()}company">Danh sách công ty</a></li>
<li class="active">Chi tiết</li>
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<h3 class="header smaller lighter blue">{$profile->cname}</h3>
<div class="clearfix">
<div class="pull-left tableTools-container"></div>
</div>
<div class="col-xs-12 col-sm-12">
<div class="profile-user-info">
<div class="profile-info-row">
<div class="profile-info-name">Address</div>
<div class="profile-info-value">
<i class="fa fa-map-marker light-orange bigger-110"></i>
<span>{$profile->caddress}</span>
</div>
</div>
<div class="profile-info-row">
<div class="profile-info-name">Email</div>
<div class="profile-info-value">
<i class="fa fa-envelope light-blue bigger-110"></i>
<span><a href="#" target="_blank">{$profile->cmail}</a></span>
</div>
</div>
<div class="profile-info-row">
<div class="profile-info-name">Tel</div>
<div class="profile-info-value">
<i class="fa fa-phone light-green bigger-110"></i>
<span>{$profile->ctel}</span>
</div>
</div>
<div class="profile-info-row">
<div class="profile-info-name">Nhân viên</div>
<div class="profile-info-value">
<i class="fa fa-users light-orange bigger-110"></i>
<a href="javascript:;" onclick="nhanvien('{$profile->cid}');">{$staff}</a>
</div>
</div>
</div>
<!-- PAGE CONTENT ENDS -->
</div>
</div>
</div>
<div class="space-8"></div>
<div class="row">
<div id="saoke" class="col-xs-12"></div>
</div>
</div>
</div>
</div>
{/block}