<script type="text/javascript">
$(document).ready(function() {
    //datatables
    load_person({$com});
});
</script>
<h3 class="header smaller lighter blue">Danh sách nhan vien</h3>
<div class="clearfix">
<div class="pull-left tableTools-container"></div>
</div>
<div style="margin-bottom:5px;">
    <button class="btn btn-xs btn-warning" onclick="reload_table()"><i class="ace-icon fa fa-refresh"></i> Reload</button>
</div>
<div>
<table id="table" class="table table-bordered table-striped">
<thead>
<tr>
<th>Ten</th>
<th>Email</th>
<th>Dien thoai</th>
<th>Ngay dang ky</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>