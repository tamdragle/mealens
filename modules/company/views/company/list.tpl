{* Extend our master template *}
{extends file="master.tpl"}
{block name=script}
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <form id="form" method="post">
            <input type="hidden" id="isSent" name="isSent" />
            <input type="hidden" id="cid" name="cid" />
            <div class="modal-body form">
                <div class="form-group">
                    <label>Name:</label>
                    <input class="form-control" type="text" id="name" name="name" />
                </div>
                <div class="form-group">
                    <label>Email:</label>
                    <input class="form-control" type="text" id="mail" name="mail" />
                </div>
                <div class="form-group">
                    <label>Phone:</label>
                    <input class="form-control" type="text" id="tel" name="tel" />
                </div>
                <div class="form-group">
                    <label>Address:</label>
                    <input class="form-control" type="text" id="address" name="address" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-xs btn-primary">Submit</button>
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<script src="{$assets_path}plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{$assets_path}plugins/datatables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar_company").addClass("active");
    
    load_person();
});
function load_person() {
    table = $('#table').DataTable({ 

        "processing": true,
        "serverSide": true,
        //"paging": true,
        //"lengthChange": false,
        //"searching": false,
        //"ordering": true,
        //"info": true,
        //"autoWidth": true,
        "order": [],

        "ajax": {
            "url": "{site_url()}company/ajax_list",
            "type": "POST"
        },
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    });
}
function add_person() {
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Thêm mới công ty'); // Set Title to Bootstrap modal title
}
function edit_person(id) {
    $.ajax({
        url : "{site_url()}company/getInfo",
        type: "POST",
        data: {
            cid:id
        },
        dataType: "JSON",
        success: function(data) {
            $('#form')[0].reset(); // reset form on modals
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string
            
            $('#isSent').val('edit');
            $('#cid').val(data.user.cid);
            $('#name').val(data.user.cname);
            $('#mail').val(data.user.cmail);
            $('#tel').val(data.user.ctel);
            $('#address').val(data.user.caddress);
            
            $('#modal_form').modal('show'); // show bootstrap modal
            $('.modal-title').text('Cap nhat công ty'); // Set Title to Bootstrap modal title
            
            
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the cap nhat');
        }
    });
}
function save() {
    $.ajax({
        url : "{site_url()}company/ajax_action",
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data) {
           //if success close modal and reload ajax table
           $('#modal_form').modal('hide');
           if (data.status == 'deniedAdd') document.location.href = '{site_url()}admin/denied?w=write';
           else if (data.status == 'deniedUpdate') document.location.href = '{site_url()}admin/denied?w=update';
           else reload_table();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error adding / update data');
        }
    });
}
function reload_table() {
    table.ajax.reload(null,false); //reload datatable ajax 
}
</script>
{/block}
{block name=body}
<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li class="active">Danh sách công ty</li>
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<h3 class="header smaller lighter blue">Danh sách công ty</h3>
<div class="clearfix">
<div class="pull-left tableTools-container"></div>
</div>
    <div style="margin-bottom:5px;">
        <button class="btn btn-xs btn-primary" onclick="add_person()"><i class="ace-icon fa fa-bolt"></i> Create</button>
        <button class="btn btn-xs btn-warning" onclick="reload_table()"><i class="ace-icon fa fa-refresh"></i> Reload</button>
    </div>
    <div>
    <table id="table" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
    <th>Name</th>
    <th>Email</th>
    <th>Phone</th>
    <th>Address</th>
    <th></th>
    </tr>
    </thead>
    <tbody>
    </tbody>
    </table>
    </div>
<!-- PAGE CONTENT ENDS -->
</div>
</div>
</div>
</div>
</div>
{/block}