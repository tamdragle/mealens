<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends MY_Controller {
    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('ssAdminId')) redirect(site_url().'admin');
        $this->permarr = $this->permission->getPermissions($this->permissions[_MOD_COMPANY]);
        $this->load->model('mod_company');
    }
    function index() {
        if (!$this->permarr['read']) redirect(site_url().'admin/denied?w=read');
        $this->parser->parse('company/list');
    }
    function ajax_list() {
        $list = $this->mod_company->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $c) {
            $no++;
            $row = array();
            $row[] = "<a href='".site_url()."company/detail?id={$c->cid}'>{$c->cname}</strong>";
            $row[] = $c->cmail;
            $row[] = $c->ctel;
            $row[] = $c->caddress;
            $row[] = '
                <div class="hidden-sm hidden-xs action-buttons">
                <a class="green" href="#" onclick="edit_person('.$c->cid.')" title="Edit"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
                <a class="red" href="#"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
                </div>
            ';

            $data[] = $row;
        }

        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mod_company->count_all(),
            "recordsFiltered" => $this->mod_company->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    function ajax_action() {
        $items = $this->input->post();
        if ($items['isSent'] == 'edit') {
            if (!$this->permarr['update']) {
                echo json_encode(array("status" => 'deniedUpdate'));
                exit;
            }
            $this->mod_company->_update($items);            
        }
        else {
            if (!$this->permarr['update']) {
                echo json_encode(array("status" => 'deniedUpdate'));
                exit;
            }
            $this->mod_company->_update($items);            
        }
        echo json_encode(array("status" => TRUE));
    }
    function detail() {
        $id = $this->input->get('id');
        $this->parser->assign('profile', $this->mod_company->_gets($id));
        $this->parser->assign('staff', $this->mod_company->_countStaff($id));
        $this->parser->parse('company/detail');
    }
    function getInfo() {
        $info = $this->mod_company->_gets($this->input->post('cid'));
        if ($info)
            $data = array(
                'code' => '200',
                'user' => $info);
        else
            $data = array('code' => '101');

        echo json_encode($data);
    }
    function nhanvien() {
        if (!$this->permarr['read']) {
            echo 'denied';
            exit;
        }
        $this->parser->assign('com', $this->input->get('id'));
        $this->parser->parse('company/nhanvien');
    }
    function nhanvien_list() {
        $this->load->model('meal/mod_member');
        $this->mod_member->company = $this->input->get('id');
        $list = $this->mod_member->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $m) {
            $no++;
            $row = array();
            $row[] = "<a href='".site_url()."meal/member/detail?id={$m->mid}'>{$m->mname}</a>";
            $row[] = $m->mmail;
            $row[] = $m->mtel;
            $row[] = date('d/m/Y', strtotime($m->mreg));
            
            $data[] = $row;
        }

        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mod_member->count_all(),
            "recordsFiltered" => $this->mod_member->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
}
