<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
get_instance()->load->iface('DefaultInterface');
class Mod_saoke extends MY_Model implements DefaultInterface {
    var $id;
    var $table1 = 'users_transaction';
    var $table2 = 'users';
    var $column = array('t1.trandate','t1.tranvalue','t1.tranbalance','t1.trantype','t1.trannote'); //set column field database for order and search
    var $order = array('t1.trandate' => 'desc'); // default order 
    //Datatable
    private function _get_datatables_query() {
        $this->db->select('t1.trandate,t1.tranvalue,t1.tranbalance,t1.trantype,t1.trannote,t2.uname AS user');
        $this->db->from($this->table1.' AS t1');
        $this->db->join($this->table2.' AS t2', 't1.aid = t2.uid', 'left');
        $this->db->where('t1.uid', $this->id);
        $i = 0;
        foreach ($this->column as $item) {//loop column 
            if (@$_POST['search']['value']) {//if datatable send POST for search
                if ($i === 0 ) {//first loop
                    $this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND. 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                //last loop
                if (count($this->column) - 1 == $i) $this->db->group_end(); //close bracket
            }
            $column[$i] = $item; //set column array variable to order processing
            $i++;
        }
        if (isset($_POST['order'])) {//here order processing
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        elseif (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables() {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all() {
        $this->db->from($this->table1.' AS t1');
        $this->db->join($this->table2.' AS t2', 't1.aid = t2.uid', 'left');
        $this->db->where('t1.uid', $this->id);
        return $this->db->count_all_results();
    }
}
/* End of file */