<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
get_instance()->load->iface('DefaultInterface');
class Mod_users extends MY_Model implements DefaultInterface {
    var $company = FALSE;
    var $parent = FALSE;
    var $table = 'users';
    var $column = array('uname','umail','utel','ureg'); //set column field database for order and search
    var $order = array('uid' => 'desc'); // default order 

    //Datatable
    private function _get_datatables_query() {
        $this->db->from($this->table);
        if ($this->company) $this->db->where('cid', $this->company);
        if ($this->parent) $this->db->where('uidparent', $this->parent);
        $i = 0;
        foreach ($this->column as $item) {//loop column 
            if (@$_POST['search']['value']) {//if datatable send POST for search
                if ($i === 0 ) {//first loop
                    $this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND. 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                //last loop
                if (count($this->column) - 1 == $i) $this->db->group_end(); //close bracket
            }
            $column[$i] = $item; //set column array variable to order processing
            $i++;
        }
        if (isset($_POST['order'])) {//here order processing
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        elseif (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables() {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all() {
        $this->db->from($this->table);
        if ($this->company) $this->db->where('cid', $this->company);
        if ($this->parent) $this->db->where('uidparent', $this->parent);
        return $this->db->count_all_results();
    }
    function _create($values) {
        if (!$this->db->insert($this->table, array(
            'uidparent' => $values['idparent'],
            'uref' => $values['ref'],
            'ulevel' => $values['level'],
            'uname' => $values['name'],
            'umail' => $values['email'],
            'utel' => $values['tel'],
            'uaddress' => $values['address'],
            'upwd' => md5(md5($values['pwd']).$values['salt']),
            'usalt' => $values['salt'],
        ))) return FALSE;
        return TRUE;
    }
    function _update($values) {
        $data = array(
            'uname' => $values['name'],
            'umail' => $values['email'],
            'utel' => $values['tel'],
            'uaddress' => $values['address']
        );
        $this->db->where('uid', $values['id']);
        $this->db->update($this->table, $data);
        return ($this->db->affected_rows()) ? TRUE : FALSE;        
    }
    function _get($id) {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('uid', $id);
        $query = $this->db->get();
        $result = $query->first_row();
        $query->free_result();
	return ($result) ? $result : false;
    }
    function _reset($id, $salt, $tmp) {
        $data = array(
            'upwd' => md5(md5($tmp).$salt),
            'usalt' => $salt,
            'utmp' => $tmp,
            'uhkey' => NULL
        );
        $this->db->where('uid', $id);
        $this->db->update($this->table, $data);
        return ($this->db->affected_rows()) ? TRUE : FALSE;
    }
    function _countFriend($id) {
        $this->db->from($this->table);
        $this->db->where('uidparent', $id);
        return $this->db->count_all_results();
    }
    function _naptien($values) {
        $this->db->trans_start();
        //Cap nhat tien trong tai khoan
        $this->db->set('uwallet', 'uwallet + ' . $values['amount'], FALSE);
        $this->db->where('uid', $values['uid']);
        $this->db->update($this->table); 
        //Luu transaction giao dich
        $this->db->query("call _topupUser({$values['uid']},{$values['amount']})");
        //Log action
        $this->db->trans_complete();
        return TRUE;
    }
    function _select($search) {
    	if ($search != '') {
            $this->db->select('uid AS id, uname AS name, umail AS mail, utel AS tel');
            $this->db->from($this->table);
            $this->db->or_like('uname', $search);
            $this->db->or_like('umail', $search);
            $this->db->or_like('utel', $search);
            $query = $this->db->get();
            $result = $query->result_array();
            $query->free_result();
            if ($result && count($result) > 0) return $result;
    	}
    	return FALSE;
    }
    function _getRef($uid) {
        $this->db->select('uref, ulevel');
        $this->db->from($this->table);
        $this->db->where('uid', $uid);
        $query = $this->db->get();
        $result = $query->row_array();
        $query->free_result();
        return ($result) ? $result : FALSE;
    }
}
/* End of file */