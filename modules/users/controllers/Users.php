<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {
    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('ssAdminId')) redirect(site_url().'admin');
        $this->permarr = $this->permission->getPermissions($this->permissions[_MOD_USERS]);
        $this->load->model('mod_users');
    }
    function index() {
        if (!$this->permarr['read']) redirect(site_url().'admin/denied?w=read');
        $this->parser->parse('list');
    }
    function ajax_list() {
        $list = $this->mod_users->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $u) {
            $no++;
            $row = array();
            $row[] = "<a href='".site_url()."users/detail?id={$u->uid}'>{$u->uname}</a>";
            $row[] = $u->umail;
            $row[] = $u->utel;
            $row[] = date('d/m/Y', strtotime($u->ureg));
            $row[] = "<div class='tmp_{$u->uid}'>{$u->utmp}</div>";
            $row[] = '
                <div class="hidden-sm hidden-xs action-buttons">
                <a class="green" href="'.site_url().'users/edit?id='.$u->uid.'" title="Edit"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
                <a class="green" href="javascript:;" onclick="reset_pwd(\''.$u->uid.'\');"><i class="ace-icon fa fa-refresh bigger-130" title="Reset mật khẩu"></i></a>
                <a class="red" href="#"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
                </div>
            ';

            $data[] = $row;
        }

        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mod_users->count_all(),
            "recordsFiltered" => $this->mod_users->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    function add() {
        if (!$this->permarr['write']) redirect(site_url().'admin/denied?w=write');
        if ($this->input->post('isSent') == 'OK') {
            $params = $this->input->post();
            $params['salt'] = getRandom(15);
            $params['pwd'] = '111';
            
            $ref = $this->mod_users->_getRef($params['idparent']);
            $params['ref'] = ($ref and !is_null($ref['uref'])) ? $ref['uref'].$params['idparent'].'-' : $params['idparent'].'-';
            $params['level'] = ($ref) ? $ref['ulevel'] + 1 : 0;
            if ($this->mod_users->_create($params)) {
                redirect(site_url().'users');
            }
            redirect(site_url().'users/add');
        }
        $this->parser->assign('title', 'Thêm mới thành viên');
        $this->parser->parse('action');
    }
    function edit() {
        if (!$this->permarr['update']) redirect(site_url().'admin/denied?w=update');
        if ($this->input->post('isSent') == 'OK') {
            $params = $this->input->post();
            if ($this->mod_users->_update($params)) {
                redirect(site_url().'users');
            }
            redirect(site_url().'users/edit?id='.$params['id']);
        }
        $this->parser->assign('title', 'Cập nhật thành viên');
        $this->parser->assign('user', $this->mod_users->_get($this->input->get('id')));
        $this->parser->parse('action');        
    }
    function detail() {
        $id = $this->input->get('id');
        $profile = $this->mod_users->_get($id);
        $this->parser->assign('profile', $profile);
        $this->parser->assign('parent', $this->mod_users->_get($profile->uidparent));
        $this->parser->assign('friends', $this->mod_users->_countFriend($id));
        
        $this->parser->parse('detail');
    }
    function friends() {
        if (!$this->permarr['read']) {
            echo 'denied';
            exit;
        }
        $this->parser->assign('parent', $this->input->get('id'));
        $this->parser->parse('friends');
    }
    function friends_list() {
        $this->mod_users->parent = $this->input->get('id');
        $list = $this->mod_users->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $u) {
            $no++;
            $row = array();
            $row[] = "<a href='".site_url()."users/detail?id={$u->uid}'>{$u->uname}</a>";
            $row[] = $u->umail;
            $row[] = $u->utel;
            $row[] = date('d/m/Y', strtotime($u->ureg));

            $data[] = $row;
        }

        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mod_users->count_all(),
            "recordsFiltered" => $this->mod_users->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    //Reset mat khau
    function reset() {
        if (!$this->permarr['update']) {
            echo 'denied';
            exit;
        }
        $new_pwd = getRandom(6);
        $this->mod_users->_reset($this->input->get('id'), getRandom(15), $new_pwd);
        echo $new_pwd;
    }
    function naptien() {
        if (!$this->permarr['write']) {
            echo json_encode(array("status" => 'denied'));
            exit;
        }
        $items = $this->input->post();
        $items['amount'] = str_replace(",","",$items['amount']);
        $items['aid'] = $this->session->userdata('ssAdminId');
        $this->mod_users->_naptien($items);
        echo json_encode(array("status" => TRUE));
    }
    function saoke() {
        if (!$this->permarr['read']) {
            echo 'denied';
            exit;
        }
        $this->parser->assign('id', $this->input->get('id'));
        $this->parser->parse('saoke');
    }
    function saoke_list() {
        $this->load->model('mod_saoke');
        $this->mod_saoke->id = $this->input->get('id');
        $list = $this->mod_saoke->get_datatables();
        $data = array();
        
        $this->config->load('my_conf');
        $type = $this->config->item('trans_type');
        
        $no = @$_POST['start'];
        foreach ($list as $t) {
            $no++;
            $row = array();
            $row[] = date('d/m/Y H:i:s', strtotime($t->trandate));
            $row[] = number_format($t->tranvalue);
            $row[] = $type[$t->trantype];
            $row[] = $t->trannote;
            $row[] = number_format($t->tranbalance);
            $row[] = $t->user;

            $data[] = $row;
        }

        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mod_saoke->count_all(),
            "recordsFiltered" => $this->mod_saoke->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);        
    }
    function select() {
        $params = $this->input->post();
        if (isset($params['isSent']) && ($params['isSent'] == 'OK')) {
            $searchInfo = trim(@$params['searchInfo']);
            if ($searchInfo != '') {
                $list = $this->mod_users->_select($searchInfo);
                if ($list) {
                    $result = '{"error_code":200,"message":"Success","data":'.json_encode($list).'}';
                    echo $result;
                    exit;
                }
            }
            echo '{"error_code":101,"message":"Không tìm thấy thành viên nào"}';
            exit;
        }
    }
}
