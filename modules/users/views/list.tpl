{* Extend our master template *}
{extends file="master.tpl"}
{block name=script}
<script src="{$assets_path}plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{$assets_path}plugins/datatables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar_users").addClass("active");
    
    load_person();
});
function load_person() {
    table = $('#table').DataTable({ 

        "processing": true,
        "serverSide": true,
        //"paging": true,
        //"lengthChange": false,
        //"searching": false,
        //"ordering": true,
        //"info": true,
        //"autoWidth": true,
        "order": [],

        "ajax": {
            "url": "{site_url()}users/ajax_list",
            "type": "POST"
        },
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    });
}
function reload_table() {
    table.ajax.reload(null,false); //reload datatable ajax 
}
function reset_pwd(id) {
    $(".tmp_"+ id).html("<center><i class='ace-icon fa fa-spinner fa-spin orange bigger-125'></i></center>");
    $.get('{site_url()}users/reset?id='+ id, {}, 
    function(response) {
        if (response == 'denied') document.location.href = '{site_url()}admin/denied?w=update';
        else $(".tmp_"+ id).html(response);
    });
}</script>
{/block}
{block name=body}
<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li class="active">Danh sách thành viên</li>
</ul>
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="space-2"></div>
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<h3 class="header smaller lighter blue">Danh sách thành viên</h3>
<div class="clearfix">
<div class="pull-left tableTools-container"></div>
</div>
<div style="margin-bottom:5px;">
    <button class="btn btn-xs btn-primary" onclick="document.location.href='{site_url()}users/add'"><i class="ace-icon fa fa-bolt"></i> Create</button>
    <button class="btn btn-xs btn-warning" onclick="reload_table()"><i class="ace-icon fa fa-refresh"></i> Reload</button>
</div>
<table id="table" class="table table-bordered table-striped">
<thead>
<tr>
<th>Ten</th>
<th>Email</th>
<th>Dien thoai</th>
<th>Ngay dang ky</th>
<th>Mật khẩu tạm</th>
<th></th>
</tr>
</thead>
<tbody>
</tbody>
</table>
<!-- PAGE CONTENT ENDS -->
</div>
</div>
</div>
</div>
</div>
{/block}