<script type="text/javascript">
$(document).ready(function() {
    //datatables
    load_list('wallet', {$id});
});
</script>
<h3 class="header smaller lighter blue">Sao kê tài khoản</h3>
<div class="clearfix">
<div class="pull-left tableTools-container"></div>
</div>
<div style="margin-bottom:5px;">
    <button class="btn btn-xs btn-warning" onclick="reload_table()"><i class="ace-icon fa fa-refresh"></i> Reload</button>
</div>
<div>
<table id="table" class="table table-bordered table-striped">
<thead>
<tr>
<th>Ngày giao dịch</th>
<th>Giá trị</th>
<th>Loại giao dịch</th>
<th>Ghi chú</th>
<th>Tiền trong ví</th>
<th>Người giao dịch</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>