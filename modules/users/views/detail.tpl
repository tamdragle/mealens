{* Extend our master template *}
{extends file="master.tpl"}
{block name=script}
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <form id="form" method="post">
            <input type="hidden" id="isSent" name="isSent" value="OK" />
            <input type="hidden" id="uid" name="uid" value="{$profile->uid}" />
            <div class="modal-body form">
                <div class="form-group">
                    <label>Số tiền:</label>
                    <input class="form-control" type="text" id="amount" name="amount" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="naptien()" class="btn btn-xs btn-primary">Submit</button>
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<script src="{$assets_path}plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{$assets_path}plugins/datatables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar_users").addClass("active");
});
$('input#amount').keyup(function(event) {
  // skip for arrow keys
  if(event.which >= 37 && event.which <= 40) return;

  // format number
  $(this).val(function(index, value) {
    return value.replace(/\D/g, "").replace(/\B(?=(\d{ldelim}3{rdelim})+(?!\d))/g, ",");
  });
});
function reload_table() {
    table.ajax.reload(null,false); //reload datatable ajax 
}
function add_amount() {
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Nạp thêm tiền'); // Set Title to Bootstrap modal title
}
function naptien() {
    $.ajax({
        url : "{site_url()}users/naptien",
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data) {
           //if success close modal and reload ajax table
           $('#modal_form').modal('hide');
           if (data.status == 'denied') document.location.href = '{site_url()}admin/denied?w=write';
           else {
               var wallet = $('#wallet').text();
               var amount = $('#amount').val();
               var sum = parseInt(wallet.replace(/,/g, "")) + parseInt(amount.replace(/,/g, ""));
               $('#wallet').text(sum.toString().replace(/\B(?=(\d{ldelim}3{rdelim})+(?!\d))/g, ","));
               //reload_table();
               load('wallet', $('#uid').val());
           }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the nap tien');
        }
    });
}
function load(list, id) {
    $('#saoke').html("<center><i class='ace-icon fa fa-spinner fa-spin orange bigger-125'></i></center>");
    var url = '';
    switch (list) {
        case 'friend':
            url = '{site_url()}users/friends?id='+ id;
            break;
        case 'wallet':
            url = '{site_url()}users/saoke?id='+ id;
            break;
    }
    if (url != '') {
        $.get(url, {}, 
        function(response) {
            if (response == 'denied') document.location.href = site_url + 'denied?w=read';
            else $('#saoke').html(response);
        });
    }
    else alert('Chuc nang khong ton tai!');
}
function load_list(list, id) {
    var url;
    switch (list) {
        case 'friend':
            url = '{site_url()}users/friends_list?id='+ id;
            break;
        case 'wallet':
            url = '{site_url()}users/saoke_list?id='+ id;
            break;
    }
    if (url != '') {
        table = $('#table').DataTable({ 
            "processing": true,
            "serverSide": true,
            "order": [],

            "ajax": {
                "url": url,
                "type": "POST"
            },
            "columnDefs": [
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            ],
        });
    }
}
</script>
{/block}
{block name=body}
<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li><a href="{site_url()}users">Danh sách thành viên</a></li>
<li class="active">Chi tiết thành viên</li>
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<h3 class="header smaller lighter blue">{$profile->uname}</h3>
<div class="clearfix">
<div class="pull-left tableTools-container"></div>
</div>
<div class="col-xs-12 col-sm-3 center">
<span class="profile-picture"><img class="editable img-responsive" alt="{$profile->uname}'s Avatar" id="avatar2" src="{$assets_path}images/avatars/profile-pic.jpg" /></span>
<div class="space space-4"></div>
<a href="#" class="btn btn-xs btn-block btn-primary"><i class="ace-icon fa fa-envelope-o bigger-110"></i> Gởi tin nhắn</a>
<a href="#" class="btn btn-xs btn-block btn-success" onclick="add_amount()"><i class="ace-icon fa fa-dollar bigger-110"></i> Nạp thêm tiền</a>
</div>
<div class="col-xs-12 col-sm-9">
<div class="profile-user-info">
<div class="profile-info-row">
<div class="profile-info-name">Địa chỉ</div>
<div class="profile-info-value">
<i class="fa fa-map-marker light-orange bigger-110"></i>
<span>{$profile->uaddress}</span>
</div>
</div>
<div class="profile-info-row">
<div class="profile-info-name">Ngày tham gia</div>
<div class="profile-info-value">
<span>{date('d/m/Y', strtotime($profile->ureg))}</span>
</div>
</div>
<div class="profile-info-row">
<div class="profile-info-name">Email</div>
<div class="profile-info-value">
<i class="fa fa-envelope light-blue bigger-110"></i>
<span><a href="#" target="_blank">{$profile->umail}</a></span>
</div>
</div>
<div class="profile-info-row">
<div class="profile-info-name">Điện thoại</div>
<div class="profile-info-value">
<i class="fa fa-phone light-green bigger-110"></i>
<span>{$profile->utel}</span>
</div>
</div>
</div>
<div class="hr hr-8 dotted"></div>
<div class="profile-user-info">
{if $parent}
<div class="profile-info-row">
<div class="profile-info-name">Người giới thiệu</div>
<div class="profile-info-value">
<i class="fa fa-user-secret blue bigger-110"></i>
<span><a href="{site_url()}users/detail?id={$parent->uid}">{$parent->uname}</a></span>
</div>
</div>
{/if}
<div class="profile-info-row">
<div class="profile-info-name">Tiền khả dụng</div>
<div class="profile-info-value">
<i class="fa fa-dollar green bigger-110"></i>
<span><a href="javascript:;" onclick="load('wallet', '{$profile->uid}');" class="green" id="wallet">{number_format($profile->uwallet)}</a></span>
</div>
</div>
<div class="profile-info-row">
<div class="profile-info-name">Bạn bè</div>
<div class="profile-info-value">
<i class="fa fa-users light-orange bigger-110"></i>
<span><a href="javascript:;" onclick="load('friend', '{$profile->uid}');">{$friends}</a></span>
</div>
</div>
</div>
</div><!-- /.col -->
<!-- PAGE CONTENT ENDS -->
</div>
</div>
<div class="space-8"></div>
<div class="row">
<div id="saoke" class="col-xs-12"></div>
</div>
</div>
</div>
</div>
{/block}