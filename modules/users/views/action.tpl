{* Extend our master template *}
{extends file="master.tpl"}
{block name=script}
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <form id="form" method="post">
            <div class="modal-body form">
                <div class="input-group">
                <span class="input-group-addon">
                <i class="ace-icon fa fa-check"></i>
                </span>
                <input type="text" id="searchInfo" name="searchInfo" class="form-control search-query" placeholder="Nhập ID, Email, Điện thoại để tìm" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-purple btn-sm">
                <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                Search
                </button>
                </span>
                </div>
            </div>
            <div class="modal-footer">
                <table class="table table-bordered table-striped search_member">
                <tr class="tr_top" style="background-color:#3c8dbc; color:#fff;">
                <th>Tên</th>
                <th>Email</th>
                <th>Điện thoại</th>
                </tr>
                </table>                
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<!-- page specific plugin scripts -->
<script src="{$assets_path}js/wizard.min.js"></script>
<script src="{$assets_path}js/jquery.validate.min.js"></script>
<script src="{$assets_path}js/jquery-additional-methods.min.js"></script>
<script src="{$assets_path}js/bootbox.js"></script>
<script src="{$assets_path}js/jquery.maskedinput.min.js"></script>
<script src="{$assets_path}js/select2.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar_users").addClass("active");
    $('#picture').ace_file_input({
        style: 'well',
        btn_choose: 'Drop files here or click to choose',
        btn_change: null,
        no_icon: 'ace-icon fa fa-cloud-upload',
        allowExt: ["jpeg", "jpg", "png", "gif" , "bmp"],
	allowMime: ["image/jpg", "image/jpeg", "image/png", "image/gif", "image/bmp"],
        droppable: true,
        thumbnail: 'small'//large | fit
        //,icon_remove:null//set null, to hide remove/reset button
        /**,before_change:function(files, dropped) {
                //Check an example below
                //or examples/file-upload.html
                return true;
        }*/
        /**,before_remove : function() {
                return true;
        }*/
        ,
        preview_error : function(filename, error_code) {
            //name of the file that failed
            //error_code values
            //1 = 'FILE_LOAD_FAILED',
            //2 = 'IMAGE_LOAD_FAILED',
            //3 = 'THUMBNAIL_FAILED'
            //alert(error_code);
        }
    }).on('change', function(){
            //console.log($(this).data('ace_input_files'));
            //console.log($(this).data('ace_input_method'));
    });
    $('#validation-form').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        ignore: "",
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email:true
            },
            tel: {
                required: true
            }
        },
        messages: {
            email: {
                required: "Email is required",
                email: "Please provide a valid email."
            }
        },
        submitHandler: function (form) {
            $("button[type='submit']").attr("disabled", true).text("Please wait...");
            form.submit();
        },
        invalidHandler: function (form) {
        }
    });

    $( '.btn-sm' ).on("click",function() {
        $.ajax({
            type: "POST",
            url: '{site_url()}users/select',
            data: {
                isSent:'OK',
                searchInfo:$("#searchInfo").val()
            },
            success: function(response) {
                var dataJson = JSON.parse(response);
                if (dataJson.error_code == 200){
                    $(".search_member tr").not(".tr_top").remove();
                    for (var i = 0; i < dataJson.data.length; i++) {
                        $(".tr_top").after(''+
                            '<tr style="text-align:left;">'+
                            '<td><a href="javascript:addVal(\''+ dataJson.data[i].id +'\', \''+ dataJson.data[i].name +'\')">'+ dataJson.data[i].name +'</a></td>'+
                            '<td><a href="javascript:addVal(\''+ dataJson.data[i].id +'\', \''+ dataJson.data[i].mail +'\')">'+ dataJson.data[i].mail +'</a></td>'+
                            '<td><a href="javascript:addVal(\''+ dataJson.data[i].id +'\', \''+ dataJson.data[i].tel +'\')">'+ dataJson.data[i].tel +'</a></td>'+
                            '</tr>');
                    }
                } else {
                    $(".search_member tr").not(".tr_top").remove();
                    $(".tr_top").after('<tr valign="top"><td colspan="3" style="text-align:center; color:red;">'+ dataJson.message +'</td></tr>');
                }
            }
        });
        return false;
    });
});
function selectParent() {
    $('#form')[0].reset();
    $(".search_member tr").not(".tr_top").remove();
    $('#modal_form').modal('show');
    $('.modal-title').text('Chọn người giới thiệu');
}
function addVal(id, search, field) {
    $('#idparent').val(id);
    $('#parent').val($.trim(search));
    $('#modal_form').modal('hide');
}
</script>
{/block}
{block name=body}
<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li><a href="{site_url()}users">Danh sách thành viên</a></li>
<li class="active">{$title}</li>
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<h3 class="header smaller lighter blue">{$title}</h3>
<div class="clearfix">
<div class="pull-left tableTools-container"></div>
</div>
<form class="form-horizontal" role="form" id="validation-form" action="" method="post" enctype="multipart/form-data">
<input type="hidden" id="isSent" name="isSent" value="OK" />
<input type="hidden" id="id" name="id" value="{$user->uid}" />
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="name">Họ tên:</label>
<div class="col-sm-9">
<input type="text" id="name" name="name" value="{$user->uname}" class="input-xlarge" />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="address">Địa chỉ:</label>
<div class="col-sm-9">
<input type="text" id="address" name="address" value="{$user->uaddress}" class="form-control" />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="email">Email</label>
<div class="col-sm-9">
<span class="input-icon">
<input type="text" id="email" name="email" value="{$user->umail}" class="input-xlarge" />
<i class="ace-icon fa fa-envelope blue"></i>
</span>
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="tel">Phone</label>
<div class="col-sm-9">
<span class="input-icon">
<input type="text" id="tel" name="tel" value="{$user->utel}" />
<i class="ace-icon fa fa-phone blue"></i>
</span>
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="picture">Picture</label>
<div class="col-sm-9">
<input type="file" id="picture" name="picture" />
</div>
</div>
{if !$user->uid}
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="parent">Người giới thiệu</label>
<div class="col-sm-9" style="position:relative;">
<span class="input-icon">
<input type="text" id="parent" name="parent" class="input-xlarge" readonly="true" value="" />
<input type="hidden" id="idparent" name="idparent" />
<i class="ace-icon fa fa-user-secret blue"></i>
</span>
<div style="position:absolute; top:8px; left:292px;"><a href="javascript:void(-1)" onclick="selectParent()">Select</a></div>
</div>
</div>
{/if}
<div class="clearfix form-actions">
<div class="col-md-offset-3 col-md-9">
<button class="btn btn-xs btn-info" type="submit">
<i class="ace-icon fa fa-check bigger-110"></i>
Submit
</button>
<button class="btn btn-xs" type="reset">
<i class="ace-icon fa fa-undo bigger-110"></i>
Reset
</button>
</div>
</div>
</form>
<!-- PAGE CONTENT ENDS -->
</div>
</div>
</div>
</div>
</div>
{/block}