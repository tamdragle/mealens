<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
get_instance() -> load -> iface('DefaultInterface');
class Mod_meal extends MY_Model implements DefaultInterface {

	function _isExists($id, $type) {
		switch ($type) {
			case 'user' :
				$key = 'uid';
				$table = 'users';
				break;
			case 'menu' :
				$key = 'mid';
				$table = 'menu';
				break;
		}
		$this -> db -> select($key);
		$this -> db -> where($key, $id);
		$query = $this -> db -> get($table);
		return ($query -> num_rows() > 0) ? TRUE : FALSE;
	}

	function _isOrdered($menu) {
		$this -> db -> select('*');
		$this -> db -> where('mid', $menu['menu']);
		$this -> db -> where('uid', $menu['user']);
		$query = $this -> db -> get('meal_orders');
		return ($query -> num_rows() > 0) ? TRUE : FALSE;
	}

	function _isDishInMenu($menu, $dish) {
		$this -> db -> select('d.id');
		$this -> db -> from('menu AS m');
		$this -> db -> join('menu_detail AS d', 'm.mid = d.mid');
		$this -> db -> where('m.mid', $menu);
		$this -> db -> where('d.did', $dish);
		$query = $this -> db -> get();
		return ($query -> num_rows() > 0) ? TRUE : FALSE;
	}

	function _getMaxOrder() {
		$this -> db -> select_max('moid', 'maxid');
		$this -> db -> from('meal_orders');
		$query = $this -> db -> get();
		$result = $query -> row_array();
		$query -> free_result();
		return ($result) ? $result['maxid'] : false;
	}

	function _getOrderRef() {

	}

	function _createOrder($order, $detail) {
		$this -> db -> trans_start();
		$balance = $order['balance'];
		foreach ($detail as $key => $list) {
			$status = 0;
			$total = 0;
			$oid = $this -> _getMaxOrder();
			$oid++;
			$insDish = array();
			foreach ($list as $k => $d) {
				$insDish[] = array('moid' => $oid, 'moquantity' => $d['quantity'], 'did' => $d['did']);
				$price = $this -> _getDishPrice($d['did']);
				$total += $price * $d['quantity'];
			}

			//Tru tien neu thanh toan bang mealens
			if ($order['payment'] == 2) {
				//Log mealens
				if (!$this -> db -> insert('mealens_logs', array('lvalue' => $total, 'moid' => $oid, 'uid' => $order['user'])))
					return FALSE;
				//Cap nhat vi
				$this -> db -> set('uwallet', 'uwallet - ' . $total, FALSE);
				$this -> db -> where('uid', $order['user']);
				$this -> db -> update('users');
				//Luu transaction
				$balance = $balance - $total;
				if (!$this -> db -> insert('users_transaction', array('tranvalue' => $total, 'tranbalance' => $balance, 'trantype' => 2, 'uid' => $order['user'], 'aid' => $order['user'], 'moid' => $oid)))
					return FALSE;
				$status = 6;
			}
			if (!$this -> db -> insert('meal_orders', array('moid' => $oid, 'mostatus' => $status, 'motype' => $order['payment'], 'moname' => $order['name'], 'moaddress' => $order['address'], 'motel' => $order['tel'], 'monote' => $order['note'], 'mid' => $key, 'uid' => $order['user'])))
				return FALSE;
			if (count($insDish) > 0)
				$this -> db -> insert_batch('meal_orders_detail', $insDish);
		}
		$this -> db -> trans_complete();
		return TRUE;
	}

	function _getMenu($z, $year, $dish) {
		$this -> db -> select('*');
		$this -> db -> from('menu');
		$this -> db -> where('mz', $z);
		$this -> db -> where('myear', $year);
		$this -> db -> where('did', $dish);
		$query = $this -> db -> get();
		$result = $query -> row_array();
		$query -> free_result();
		return (count($result) > 0) ? $result : false;

	}

	function _getDishPrice($id) {
		$this -> db -> select('dprice');
		$this -> db -> from('dish');
		$this -> db -> where('did', $id);
		$query = $this -> db -> get();
		$result = $query -> row_array();
		$query -> free_result();
		return ($result) ? $result['dprice'] : false;
	}
}

/* End of file */
