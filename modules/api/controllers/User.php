<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends REST_Controller {

	public function __construct() {
		parent::__construct();
		$this -> load -> model('mod_user');
		$this -> config -> load('my_conf');
	}

	function login_post() {
		if ($this -> input -> post('isSent') == 'OK') {
			$response = array();
			$salt = $this -> mod_user -> _getSalt($this -> input -> post('username'));
			if ($salt) {
				$user = $this -> mod_user -> _login(array('uid' => $salt['uid'], 'password' => $this -> input -> post('password'), 'salt' => $salt['usalt']));
				if ($user) {
					if ($this -> input -> post('ftoken') != "") {
						$this -> mod_user -> _insertFirebaseToken(array('uid' => $salt['uid'], 'ftoken' => $this -> input -> post('ftoken'), 'os' => $this -> input -> post('os')));
					}
					$session = array('ssId' => $user['uid'], 'ssFullname' => $user['uname'], 'ssTel' => $user['utel'], 'ssAddress' => $user['uaddress']);
					//co the bo
					$this -> session -> set_userdata($session);
					$token = $this -> mod_user -> _createToken($session);
					$user["token"] = $token;
					$response["data"] = $user;
					$response["error_code"] = 200;
					$response["message"] = "Login thành công";
				} else {
					$response["error_code"] = 202;
					$response["message"] = "Mật khẩu không đúng";
				}
			} else {
				$response["error_code"] = 201;
				$response["message"] = "Không tìm thấy thông tin user";
			}
			$this -> response($response);
		}
		exit ;
	}

	function register_post() {
		if ($this -> input -> post('isSent') == 'OK') {
			$response = array();
			$params = $this -> input -> post();
			if ($params['name'] == '') {
				$response["error_code"] = 201;
				$response["message"] = "Vui lòng nhập tên";
				$this -> response($response);
				exit ;
			}
			//Email
			if ($params['mail'] == '') {
				$response["error_code"] = 201;
				$response["message"] = "Vui lòng nhập email";
				$this -> response($response);
				exit ;
			}
			if (!filter_var($params['mail'], FILTER_VALIDATE_EMAIL)) {
				$response["error_code"] = 201;
				$response["message"] = "Vui lòng nhập đúng định dạng email";
				$this -> response($response);
				exit ;
			}
			if ($this -> mod_user -> _isExists('email', $params['mail'])) {
				$response["error_code"] = 201;
				$response["message"] = "Email đã có người sử dụng";
				$this -> response($response);
				exit ;
			}
			//Tel
			if ($params['tel'] == '') {
				$response["error_code"] = 201;
				$response["message"] = "Vui lòng nhập số điện thoại";
				$this -> response($response);
				exit ;
			}
			if (!is_numeric($params['tel'])) {
				$response["error_code"] = 201;
				$response["message"] = "Vui lòng nhập đúng định dạng số điện thoại";
				$this -> response($response);
				exit ;
			}
			if ($this -> mod_user -> _isExists('tel', $params['tel'])) {
				$response["error_code"] = 201;
				$response["message"] = "Điện thoại đã có người sử dụng";
				$this -> response($response);
				exit ;
			}
			//Address
			if ($params['address'] == '') {
				$response["error_code"] = 201;
				$response["message"] = "Vui lòng nhập địa chỉ nhận hàng";
				$this -> response($response);
				exit ;
			}
			//Kiem tra thong tin nguoi gioi thieu
			$params['salt'] = getRandom(15);

			if ($params['pwd'] == '') {
				$params['pwd'] = 'mealens';
			}

			$ref = $this -> mod_user -> _getRef($params['ref']);
			if ($ref) {
				$params['parent'] = $ref['uid'];
				$params['ref'] = (!is_null($ref['uref'])) ? $ref['uref'] . $ref['uid'] . '-' : $ref['uid'] . '-';
				$params['level'] = $ref['ulevel'] + 1;
			} else {
				$params['parent'] = 0;
				$params['ref'] = '-';
				$params['level'] = 0;
			}
			if (PROMOTION_REG) {
				$params['wallet'] = PROMOTION_REG_VALUE;
			}
			if ($this -> mod_user -> _register($params)) {
				$salt = $this -> mod_user -> _getSalt($this -> input -> post('tel'));
				if ($salt) {
					$user = $this -> mod_user -> _login(array('uid' => $salt['uid'], 'password' => $this -> input -> post('pwd'), 'salt' => $salt['usalt']));
					if ($this -> input -> post('ftoken') != "") {
						$this -> mod_user -> _insertFirebaseToken(array('uid' => $salt['uid'], 'ftoken' => $this -> input -> post('ftoken'), 'os' => $this -> input -> post('os')));
					}
					$session = array('ssId' => $user['uid'], 'ssFullname' => $user['uname'], 'ssTel' => $user['utel'], 'ssAddress' => $user['uaddress']);
					//co the bo
					$this -> session -> set_userdata($session);
					$token = $this -> mod_user -> _createToken($session);
					$user["token"] = $token;
					$response["data"] = $user;
					$response["error_code"] = 200;
					$response["message"] = "Đăng ký thành công, vui lòng đăng nhập để dặt cơm hoặc mua hàng";
				} else {
					$response["error_code"] = 201;
					$response["message"] = "Không thể ghi dữ liệu";
				}
			} else {
				$response["error_code"] = 201;
				$response["message"] = "Không thể ghi dữ liệu";
			}
			$this -> response($response);
		}
		exit ;
	}

	function friend_post() {
		if ($this -> input -> post('isSent') == 'OK') {
			$response = array();
			$headers = apache_request_headers();
			$response = $this -> mod_user -> _checkToken($headers);
			if ($response["uid"] != NULL) {
				$uid = $response["uid"];
				$response["data"] = array();
				$friendList = $this -> mod_user -> _getFriend($uid);
				foreach ($friendList as $detail) {
					$tmp = array();
					$tmp['uid'] = $detail['uid'];
					$tmp['uname'] = $detail['uname'];
					$tmp['utel'] = $detail['utel'];
					$tmp['ureg'] = $detail['ureg'];
					array_push($response["data"], $tmp);
				}
				$response["error_code"] = 200;
				$response["message"] = "Thành công";
			}
			$this -> response($response);
		}
		exit ;
	}

	function transaction_post() {
		if ($this -> input -> post('isSent') == 'OK') {
			$response = array();
			$headers = apache_request_headers();
			$response = $this -> mod_user -> _checkToken($headers);
			if ($response["uid"] != NULL) {
				$uid = $response["uid"];
				$response["data"] = array();
				$tranList = $this -> mod_user -> _getTran($uid);
				$config = $this -> config -> item('trans_type');
				foreach ($tranList as $detail) {
					$tmp = array();
					$tmp['tranid'] = $detail['tranid'];
					$tmp['trandate'] = $detail['trandate'];
					$tmp['tranvalue'] = $detail['tranvalue'];
					$tmp['tranbalance'] = $detail['tranbalance'];
					$tmp['trantype'] = $config[$detail['trantype']];
					$tmp['trannote'] = $detail['trannote'];
					$tmp['transUser'] = $detail['uname'];
					array_push($response["data"], $tmp);
				}
				$response["error_code"] = 200;
				$response["message"] = "Thành công";
			}
			$this -> response($response);
		}
		exit ;
	}

	function logout_post() {
		if ($this -> input -> post('isSent') == 'OK') {
			$response = array();
			$headers = apache_request_headers();
			$response = $this -> mod_user -> _checkToken($headers);
			if ($response["uid"] != NULL) {
				$response["error_code"] = 200;
				$response["message"] = "Đăng xuất thành công";
				$this -> session -> sess_destroy();
			}
			$this -> response($response);
		}
		exit ;
	}
	
	function wallet_post() {
		if ($this -> input -> post('isSent') == 'OK') {
			$response = array();
			$headers = apache_request_headers();
			$response = $this -> mod_user -> _checkToken($headers);
			if ($response["uid"] != NULL) {
				$wallet = $this -> mod_user -> _getWallet($response["uid"]);
				$response["data"] = $wallet;
				$response["error_code"] = 200;
				$response["message"] = "Thành công";
			}
			$this -> response($response);
		}
		exit ;
	}

}
