<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meal extends API_Controller {

	function __construct() {
		parent::__construct();
		$this -> load -> model("mod_meal");
		$this -> load -> model("mod_user");
		$this -> load -> model("mod_lunch");
		$this -> load -> model("mod_dish");
		$this -> load -> library('cart');
		$this -> config -> load('my_conf');
		$this -> parser -> assign('path_upload', $this -> config -> item('path_upload'));
	}

	/*
	 * - user (int): Nguoi dat com
	 * - menu (int): Menu com
	 * - address (varchar): Dia chi giao com
	 * - detail (json): array(
	 *                      array(
	 *                          did,
	 *                          dquantity
	 *                      )
	 *                  )
	 */
	function order_post() {
		$error = FALSE;
		$orders = $this -> post();
		if ($this -> input -> post('isSent') == 'OK') {
			$response = array();
			$headers = apache_request_headers();
			$response = $this -> mod_user -> _checkToken($headers);
			if ($response["uid"] != NULL) {
				//Lay thong tin data
				//Lay thong tin mon an
				$detail = json_decode($orders['detail'], true);
				$dish = array();

				foreach ($detail as $key => $d) {
					//Lay thong tin menu
					$mid = $key;
					$this -> mod_lunch -> id = $mid;
					$menu = $this -> mod_lunch -> _get();
					if (!$menu) {
						$error = TRUE;
						$response["error_code"] = 201;
						$response["message"] = "Thực đơn không tồn tại";
						break;
						//Kiem tra mon an co ton tai trong menu khong
					} elseif ($this -> mod_meal -> _isDishInMenu($orders['menu'], $d['did'])) {
						$error = TRUE;
						$response["error_code"] = 201;
						$response["message"] = "Món ăn không tồn tại";
						break;
					} else {
						if ($menu['mz'] < date('z')) {//Qua ngay
							$error = TRUE;
							$response["error_code"] = 201;
							$response["message"] = "Thực đơn đã quá ngày, không thể hoàn tất đơn hàng";
							break;
						} elseif (($menu['mz'] == date('z')) AND date('H') > 10) {//Qua gio
							$error = TRUE;
							$response["error_code"] = 201;
							$response["message"] = "Thực đơn đã quá giờ, không thể hoàn tất đơn hàng";
							break;
						}
					}
				}

				if (!$error) {
					$total = 0;
					foreach ($detail as $key => $dishs) {
						foreach ($dishs as $k => $d) {
							$price = $this -> mod_dish -> _getPrice($d['did']);
							$total += $price * $d['quantity'];
							$insDish[] = array('moid' => $oid, 'moquantity' => $d['quantity'], 'did' => $d['did']);
						}
					}
					$orders['user'] = $response["uid"];
					if ($orders['payment'] == 2) {
						$user = $this -> mod_user -> _get($orders['user']);
						if ($total < SHIPPING_VALUE)
							$total += SHIPPING_FEE;
						if ($user -> uwallet < $total) {//Thieu tien
							$response["error_code"] = 201;
							$response["message"] = "Bạn không đủ tiền trong tài khoản.";
							$this -> response($response);
							exit ;
						}
						//Nhap mat khau thanh toan
						if ($orders['paypwd'] == '') {
							$response["error_code"] = 201;
							$response["message"] = "Vui lòng nhập mật khẩu thanh toán.";
							$this -> response($response);
							exit ;
						}
						//Mat khau thanh toan ko dung
						if (md5(md5($orders['paypwd']) . $user -> usalt) != $user -> upayment) {
							$response["error_code"] = 201;
							$response["message"] = "Mật khẩu thanh toán không đúng.";
							$this -> response($response);
							exit ;
						}
						$orders['balance'] = $user -> uwallet;
					}

					if (count($insDish) > 0) {
						if ($this -> mod_meal -> _createOrder($orders, $detail)) {
							$response["error_code"] = 200;
							$response["message"] = "Đặt hàng thành công";
						} else {
							$response["error_code"] = 500;
							$response["message"] = "Đặt hàng thất bại";
						}
					} else {
						$response["error_code"] = 202;
						$response["message"] = "Chưa chọn món ăn";
					}
				}

			}
			$this -> response($response);
		}
		exit ;
	}

	function fee_ship_post() {
		$response = array();
		$response["error_code"] = 200;
		$tmp = array();
		$tmp['shipValue'] = SHIPPING_VALUE;
		$tmp['shipFee'] = SHIPPING_FEE;
		$response["data"] = $tmp;
		$response["message"] = "Thành công";
		$this -> response($response);
		exit ;
	}

	function order_by_user_post() {
		if ($this -> input -> post('isSent') == 'OK') {
			$response = array();
			$headers = apache_request_headers();
			$response = $this -> mod_user -> _checkToken($headers);
			if ($response["uid"] != NULL) {
				$response["data"] = array();
				$orderList = $this -> mod_lunch -> _getOrders($response["uid"]);
				foreach ($orderList as $order) {
					$tmp = array();
					$tmp['moid'] = $order['moid'];
					$tmp['modate'] = $order['mdate'];
					$tmp['mostatus'] = $this -> config -> item('order_status')[$order['mostatus']];
					$tmp['moaddress'] = $order['moaddress'];
					$tmp['mosoluong'] = $order['soluong'];
					$tmp['motong'] = $order['tong'];
					$tmp['lst'] = array();
					$orderDetails = $this -> mod_lunch -> _getOrderDetail($order['moid']);
					foreach ($orderDetails as $detail) {
						$dmp = array();
						$dmp['gia'] = $detail['gia'];
						$dmp['soluong'] = $detail['soluong'];
						$dmp['tong'] = $detail['tong'];
						$dmp['dname'] = $detail['dname'];
						$dmp['dpic'] = $this -> config -> item('path_upload') . "meal/" . $detail['dpic'];
						array_push($tmp["lst"], $dmp);
					}
					array_push($response["data"], $tmp);
				}
				$response["error_code"] = 200;
				$response["message"] = "Thành công";
			}
			$this -> response($response);
		}
		exit ;
	}

}
