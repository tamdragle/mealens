<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Profile extends API_Controller {

	public function __construct() {
		parent::__construct();
		$this -> load -> model('mod_user');
	}

	function change_password_post() {
		if ($this -> input -> post('isSent') == 'OK') {
			$response = array();
			$headers = apache_request_headers();
			$response = $this -> mod_user -> _checkToken($headers);
			if ($response["uid"] != NULL) {
				$params = $this -> input -> post();
				if ($params['oldpwd'] == '') {
					$response["error_code"] = 201;
					$response["message"] = "Vui lòng nhập mật khẩu cũ";
					$this -> response($response);
					exit ;
				}
				//Email
				if ($params['newpwd'] == '') {
					$response["error_code"] = 201;
					$response["message"] = "Vui lòng nhập mật khẩu mới";
					$this -> response($response);
					exit ;
				}
				if ($params['newpwd'] != $params['newpwd_confirm']) {
					$response["error_code"] = 201;
					$response["message"] = "Xác nhận mật khẩu không chính xác";
					$this -> response($response);
					exit ;
				}
				$user = $this -> mod_user -> _get($response["uid"]);
				switch ($params['type']) {
					case 'login' :
						$pwd = $user -> upwd;
						break;
					case 'payment' :
						$pwd = $user -> upayment;
						break;
				}
				if (md5(md5($params['oldpwd']) . $user -> usalt) != $pwd) {
					$response["error_code"] = 201;
					$response["message"] = "Mật khẩu cũ không đúng";
					$this -> response($response);
					exit ;
				}
				if ($this -> mod_user -> _chgPwd(array('id' => $response["uid"], 'salt' => $user -> usalt, 'pwd' => $params['newpwd_confirm'], 'type' => $params['type']))) {
					$response["error_code"] = 200;
					$response["message"] = "Đổi mật khẩu thành công";
				} else {
					$response["error_code"] = 201;
					$response["message"] = "Không đổi được mật khẩu";

				}
				$this -> response($response);
			}

		}
		exit ;
	}

}
