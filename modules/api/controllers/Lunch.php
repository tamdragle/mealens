<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Lunch extends API_Controller {

	public function __construct() {
		parent::__construct();
		//load model
		$this -> load -> model('mod_lunch');
		$this -> load -> library('cart');
		$this -> config -> load('my_conf');
		$this -> load -> model('mod_user');
		$this -> parser -> assign('path_upload', $this -> config -> item('path_upload'));
	}

	function list_post() {
		if ($this -> input -> post('isSent') == 'OK') {
			$response = array();
			$headers = apache_request_headers();
			$response = $this -> mod_user -> _checkToken($headers);
			if ($response["uid"] != NULL) {
				$response["data"] = array();
				$z = ($this -> input -> post('z')) ? $this -> input -> post('z') : date('z');
				$year = date('Y');
				$this -> mod_lunch -> z = $z;
				$this -> mod_lunch -> year = $year;
				$menu = $this -> mod_lunch -> _get($z, $year);
				if ($menu) {
					$this -> mod_lunch -> menu = $menu['mid'];
					$DetailList = $this -> mod_lunch -> _getDetail();
					foreach ($DetailList as $detail) {
						$tmp = array();
						$tmp['id'] = $detail['id'];
						$tmp['mid'] = $detail['mid'];
						$tmp['mquantity'] = $detail['mquantity'];
						$tmp['mprice'] = $detail['mprice'];
						$tmp['did'] = $detail['did'];
						$tmp['dname'] = $detail['dname'];
						$tmp['mdate'] = $detail['mdate'];
						$tmp['dpic'] = $this -> config -> item('path_upload') . "meal/" . $detail['dpic'];
						$tmp['dtid'] = $detail['dtid'];
						$tmp['dfeature'] = $detail['dfeature'];
						$tmp['drecipe'] = $detail['drecipe'];
						array_push($response["data"], $tmp);
					}
				}
				$response["error_code"] = 200;
				$response["message"] = "Thành công";
			}
			$this -> response($response);
		}
		exit ;
	}

	function cart_post() {
		$cart = $this -> cart -> contents();
		$carts = array();
		foreach ($cart as $c) {
			$carts[$c['menu']][] = $c;
		}
		ksort($carts);
		$this -> parser -> assign('carts', $carts);
		$menu = array();
		foreach ($carts as $k => $cs) {
			$this -> mod_lunch -> id = $k;
			$menu[$k] = $this -> mod_lunch -> _get();
		}
		$this -> parser -> assign('menu', $menu);
		$this -> parser -> parse('lunch/cart');
	}

	function addCart_post() {
		if ($this -> input -> post('isSent') == 'OK') {
			$mid = $this -> input -> post('menuID');
			$this -> mod_lunch -> id = $mid;
			$detail = $this -> mod_lunch -> _getDetail();
			$this -> mod_lunch -> id = $detail['mid'];
			$menu = $this -> mod_lunch -> _get();
			if ($menu['mz'] < date('z')) {
				echo '{"error_code":201,"message":"Không thể thêm được vào giỏ hàng (Quá ngày)"}';
				exit ;
			} elseif (($menu['mz'] == date('z')) AND (date('H') > 10)) {
				echo '{"error_code":201,"message":"Không thể thêm được vào giỏ hàng (Quá giờ)"}';
				exit ;
			}
			$insert_data = array('id' => $detail['did'] . '_' . $detail['mid'], 'name' => $detail['dname'], 'price' => $detail['mprice'], 'qty' => 1, 'menu' => $detail['mid'], 'pic' => $detail['dpic']);
			// This function add items into cart.
			if ($this -> cart -> insert($insert_data)) {
				echo '{"error_code":200,"message":"Đã thêm vào giỏ hàng"}';
			} else {
				echo '{"error_code":201,"message":"Không thêm được vào giỏ hàng"}';
			}
			exit ;
		}
	}

	function updCart_post() {
		$data = array('rowid' => $this -> input -> post('cartID'), 'qty' => $this -> input -> post('cartVal'), );
		if ($this -> cart -> update($data)) {
			echo '{"error_code":200,"message":"Cập nhật giỏ hàng thành công"}';
		} else {
			echo '{"error_code":201,"message":"Không cập nhật được vào giỏ hàng"}';
		}
		exit ;
	}

	function delCart_post() {
		$opt = $this -> input -> post('cartOpt');
		$id = $this -> input -> post('cartID');
		// Check rowid value.
		if ($opt === "all") {
			// Destroy data which store in session.
			$this -> cart -> destroy();
		} elseif ($opt === "date") {
			$data = array();
			$cart = $this -> cart -> contents();
			foreach ($cart as $c) {
				if ($c['menu'] == $id) {
					$data[] = array('rowid' => $c['rowid'], 'qty' => 0);
				}
				$this -> cart -> update($data);
			}
		} else {
			// Destroy selected rowid in session.
			$data = array('rowid' => $id, 'qty' => 0);
			// Update cart data, after cancle.
			$this -> cart -> update($data);
		}
		echo '{"error_code":200,"message":"Xoá thành công"}';
		exit ;
	}

	function checkout_post() {
		$error = FALSE;
		if ($this -> input -> post('isSent') == 'OK') {
			$response = array();
			$headers = apache_request_headers();
			$response = $this -> mod_user -> _checkToken($headers);
			if ($response["uid"] != NULL) {
				//Lay thong tin data
				$data = $this -> input -> get('data');
				$dataArray = json_decode($data, true);

				foreach ($dataArray as $key => $value) {
					//Lay thong tin menu
					$mid = $value('menu');
					$this -> mod_lunch -> id = $mid;
					$menu = $this -> mod_lunch -> _get();
					if (!$menu) {
						$this -> parser -> assign('message', 'Thực đơn không tồn tại');
					} else {
						if ($menu['mz'] < date('z')) {//Qua ngay
							$error = TRUE;
							$response["error_code"] = 201;
							$response["message"] = "Thực đơn đã quá ngày, không thể hoàn tất đơn hàng";
							break;
						} elseif (($menu['mz'] == date('z')) AND date('H') > 10) {//Qua gio
							$error = TRUE;
							$response["error_code"] = 201;
							$response["message"] = "Thực đơn đã quá giờ, không thể hoàn tất đơn hàng";
							break;
						}
					}
				}

				if (!$error) {
					foreach ($dataArray as $key => $value) {
						//Lay thong tin menu
						$mid = $value('menu');
						$this -> mod_lunch -> id = $mid;
						$menu = $this -> mod_lunch -> _get();
						if (!$menu) {
							$this -> parser -> assign('message', 'Thực đơn không tồn tại');
						} else {
							if ($menu['mz'] < date('z')) {//Qua ngay
								$error = TRUE;
								$response["error_code"] = 201;
								$response["message"] = "Thực đơn đã quá ngày, không thể hoàn tất đơn hàng";
								break;
							} elseif (($menu['mz'] == date('z')) AND date('H') > 10) {//Qua gio
								$error = TRUE;
								$response["error_code"] = 201;
								$response["message"] = "Thực đơn đã quá giờ, không thể hoàn tất đơn hàng";
								break;
							}
						}
					}
				}

			}
			$this -> response($response);
		}
		exit ;
	}

	function orderCart() {
		$this -> load -> library('curl');
		$this -> curl -> create(site_url() . 'api/meal/order');

		$detail = array();
		$clear = array();
		$carts = $this -> cart -> contents();
		foreach ($carts as $cart) {
			if ($cart['menu'] == $this -> input -> post('menuID')) {
				$detail[] = array('did' => $cart['id'], 'dquantity' => $cart['qty']);
				$clear[] = array('rowid' => $cart['rowid'], 'qty' => 0);
			}
		}
		$this -> curl -> post(array('user' => $this -> session -> userdata('ssId'), 'menu' => $this -> input -> post('menuID'), 'name' => $this -> input -> post('tenkhachhang'), 'address' => $this -> input -> post('diachi'), 'tel' => $this -> input -> post('dienthoai'), 'note' => $this -> input -> post('ghichu'), 'detail' => json_encode($detail)));
		$order = json_decode($this -> curl -> execute());
		if ($order -> err_code == '200') {
			//clear cart
			$this -> cart -> update($clear);
		}
		echo '{"error_code":"' . $order -> err_code . '","message":"' . $order -> err_mess . '"}';
		exit ;
	}

	function orders() {
		$this -> parser -> parse('lunch/orders');
	}

}
