<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Images extends MY_Controller {
    private $id;
    private $tree;
    private $treeTable;
    private $optResult;
    var $path;
    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('ssAdminId')) redirect(site_url().'admin');
        $this->permarr = $this->permission->getPermissions($this->permissions[_MOD_IMAGES]);
        $this->load->model('mod_images');
    }
    function index() {
        if (!$this->permarr['read']) redirect(site_url().'admin/denied?w=read');
        $danhmuc = $this->mod_images->_load();
        foreach($danhmuc as $row) {
            $this->tree[$row['imgcat_idparent']][$row['imgcat_id']] = $row;
        }
        $this->creatTableTree();
        $this->parser->assign('treeTable', $this->treeTable);
        $this->parser->parse('danhmuc');
    }
    private function creatTableTree($cid = '0', $parrentstt = '', $level = 1) {
        if (!isset($this->tree[$cid])) return false;
        $stt = 0;
        foreach ($this->tree[$cid] as $key => $value) {
            $stt++;
            if ($parrentstt!='') $this->treeTable .= "<tr data-tt-id='$parrentstt-$stt' data-tt-parent-id='$parrentstt'>";
            else $this->treeTable .= "<tr data-tt-id='$stt'>";
            $delete = (!isset($this->tree[$key])) ? "<a class='red delete' id='{$value['imgcat_id']}' title='Xóa' href='#'><i class='ace-icon fa fa-trash-o bigger-130'></i></a>" : "";
            $this->treeTable .= "
                <td><a href='#' id='{$value['imgcat_id']}' class='detail'>{$value['imgcat_name']}</a></td>
                <td>{$value['num']}</td>
                <td nowrap='nowrap'>
                    <a class='green edit' href='#'id='{$value['imgcat_id']}' title='Cập nhật'><i class='ace-icon fa fa-pencil bigger-130'></i></a>
                    {$delete}
                </td>
            ";
            $this->treeTable .= "</tr>\n";
            if ($parrentstt != '') $this->creatTableTree($key, $parrentstt.'-'.$stt, $level+1);
            else $this->creatTableTree($key, $stt, $level+1);
        }
    }
    function insertDanhmuc() {
        if (!$this->permarr['write']) redirect(site_url().'admin/denied?w=write');
        if ($this->input->post('isSent') == 'OK') {
            $items = $this->input->post();
            $this->mod_images->_create($items);
            redirect(site_url().'images');
        }
        else {
            $danhmuc = $this->mod_images->_load();
            foreach ($danhmuc as $row) {
                $this->tree[$row['imgcat_idparent']][$row['imgcat_id']] = $row;
            }
            $this->creatOptTree();

            $this->parser->assign('title', 'Thêm danh mục');
            $this->parser->assign('options', $this->optResult);
            $this->parser->assign('val', array(
                    'parent' => '',
                    'name' => ''
                )
            );
            $this->parser->parse('actionDanhmuc');
        }
    }
    function updateDanhmuc() {
        if (!$this->permarr['update']) redirect(site_url().'admin/denied?w=update');
        if ($this->input->post('isSent') == 'OK') {
            $items = $this->input->post();
            $this->mod_images->id = $items['id'];
            $this->mod_images->_update($items);
            redirect(site_url().'images');
        }
        else {
            $danhmuc = $this->mod_images->_load();
            foreach ($danhmuc as $row) {
                $this->tree[$row['imgcat_idparent']][$row['imgcat_id']] = $row;
            }
            $this->id = $this->input->get('id');
            $this->creatOptTree();
            $this->mod_images->id = $this->input->get('id');
            $danhmuc = $this->mod_images->_load();

            $this->parser->assign('title', 'Cập nhật danh mục');
            $this->parser->assign('options', $this->optResult);
            $this->parser->assign('val', array(
                    'id' => $this->id,
                    'parent' => $danhmuc['imgcat_idparent'],
                    'name' => $danhmuc['imgcat_name']
                )
            );
            $this->parser->parse('actionDanhmuc');
        }
    }
    private function creatOptTree($cid = '0', $level = 1) {
        if (!isset($this->tree[$cid])) return false;
        foreach ($this->tree[$cid] as $key => $value) {
            $name = '';
            if($level > 1) {
                $name .= str_repeat('--- ', $level - 1);
            }
            $name .= $value['imgcat_name'];
            if ($value['imgcat_id'] != $this->id) {
                $this->optResult[$value['imgcat_id']] = $name;
                $this->creatOptTree($value['imgcat_id'], $level + 1);
            }
        }
    }
    function deleteDanhmuc(){
        if (!$this->permarr['delete']) redirect(site_url().'admin/denied?w=delete');
        $this->mod_images->danhmuc = $this->input->get('id');
        //Lay het file hinh anh cua sanpham
        $files = $this->mod_images->_getAllImages();
        $this->config->load('my_conf');
        foreach ($files as $file) {
            if ($file['img_src'] != '' && is_file(config_item('path_upload').'images/'.$file['img_src'])) unlink(config_item('path_upload').'images/'.$file['img_src']);
            if ($file['img_src'] != '' && is_file(config_item('path_upload').'images/thumb_'.$file['img_src'])) unlink(config_item('path_upload').'images/thumb_'.$file['img_src']);
        }
        $this->mod_images->id = $this->input->get('id');
        $this->mod_images->_delete();
        redirect(site_url().'images');
    }
    function view() {
        if ($this->input->post('isSent') == 'OK') {
            $this->config->load('my_conf');
            // set upload preferences
            $config['upload_path'] = config_item('path_upload').'images';
            $config['allowed_types'] = 'png|gif|jpg|jpeg';
            //$config['max_size']    = '150';

            //initialize upload class
            $this->load->library('upload', $config);
            $this->load->library('imgresize');

            $upload_error = array();

            for ($i=0; $i<count($_FILES['usr_files']['name']); $i++) {
                $_FILES['userfile']['name']= $_FILES['usr_files']['name'][$i];
                $_FILES['userfile']['type']= $_FILES['usr_files']['type'][$i];
                $_FILES['userfile']['tmp_name']= $_FILES['usr_files']['tmp_name'][$i];
                $_FILES['userfile']['error']= $_FILES['usr_files']['error'][$i];
                $_FILES['userfile']['size']= $_FILES['usr_files']['size'][$i];

                if ($this->upload->do_upload()) {
                    $files = $this->upload->data();
                    //$params['pic'] = $files['file_name'];
                    //Resize
                    if((intval($files['image_width']) > 150) OR (intval($files['image_height']) > 150)) {
                        $thumb_name = 'thumb_'.$files['raw_name'].$files['file_ext'];
                        $objResize = $this->imgresize->getInstanceOf($files['full_path'], $files['file_path'].$thumb_name, 150, 150);
                        $objResize->getResizedImage();
                        unset($objResize);
                    }
                }
                else {
                    echo json_encode(array("status" => 'false'));
                    break;                    
                }
            }
        }
        else {
            $this->mod_images->id = $this->input->get('dm');
            $this->getPath($this->mod_images->id);
            $this->parser->assign('path', array_reverse($this->path));
            $this->parser->assign('dm', $this->mod_images->_load());
            $this->parser->parse('list');            
        }
    }
    function view_ajax() {
        $this->config->load('my_conf');
        $this->parser->assign('path_upload', config_item('path_upload'));
        $this->mod_images->danhmuc = $this->input->get('dm');
        //$this->parser->assign('images', $this->mod_images->_getImages());
        $this->parser->parse('images');
    }
    private function getPath($id) {
        $this->mod_images->id = $id;
        $category = $this->mod_images->_getCategory();
        $this->path[] = $category;
        if ($category['imgcat_idparent'] != '0') $this->getPath($category['imgcat_idparent']);
    }
}
