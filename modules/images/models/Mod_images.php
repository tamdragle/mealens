<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
get_instance()->load->iface('DefaultInterface');
class Mod_images extends MY_Model implements DefaultInterface {
    var $id = FALSE;
    var $danhmuc = FALSE;
    public function _load() {
        $this->db->select("
            DM.*,
            (SELECT COUNT(IMG.img_id) FROM images IMG WHERE DM.imgcat_id = IMG.imgcat) AS num
        ");
        $this->db->from('images_category DM');
        if ($this->id) $this->db->where('DM.imgcat_id', $this->id);
        $this->db->order_by('DM.imgcat_name', 'ASC');
        $query = $this->db->get();
        $result = ($this->id) ? $query->first_row('array') : $query->result_array();
        $query->free_result();
        return $result;
    }
    public function _create($values) {
        $data = array(
            'imgcat_idparent' => $values['parent'],
            'imgcat_name' => $values['name']
        );
        $this->db->insert('images_category', $data);
    }
    public function _update($values) {
        $data = array(
            'imgcat_idparent' => $values['parent'],
            'imgcat_name' => $values['name']
        );
        $this->db->where('imgcat_id', $this->id);
        $this->db->update('images_category', $data);
    }
    public function _delete() {
        $this->db->delete('images_category', array('id' => $this->id)); 
    }
    public function _getAllImages() {
        $this->db->select("img_src");
        $this->db->from('images');
        $this->db->where('imgcat', $this->danhmuc); 
        $query = $this->db->get();
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }
    function _getCategory() {
        $this->db->select("*");
        $this->db->from('images_category');
        if ($this->id) $this->db->where('imgcat_id', $this->id);
        $query = $this->db->get();
        $result = ($this->id) ? $query->first_row('array') : $query->result_array();
        $query->free_result();
        return $result;
    }
    public function _getImages() {
        $this->db->select("*");
        $this->db->from('images');
        if ($this->id) $this->db->where('imgcat_id', $this->id);
        else $this->db->where('imgcat', $this->danhmuc); 
        $this->db->order_by('imgcat_id', 'DESC');
        $query = $this->db->get();
        $result = ($this->id) ? $query->first_row('array') : $query->result_array();
        $query->free_result();
        return $result;
    }
}
/* End of file */