<script type="text/javascript">
jQuery(function($) {
    var $overflow = '';
    var colorbox_params = {
        rel: 'colorbox',
        reposition:true,
        scalePhotos:true,
        scrolling:false,
        previous:'<i class="ace-icon fa fa-arrow-left"></i>',
        next:'<i class="ace-icon fa fa-arrow-right"></i>',
        close:'&times;',
        current:'{ldelim}current{rdelim} of {ldelim}total{rdelim}',
        maxWidth:'100%',
        maxHeight:'100%',
        onOpen:function(){
            $overflow = document.body.style.overflow;
            document.body.style.overflow = 'hidden';
        },
        onClosed:function(){
            document.body.style.overflow = $overflow;
        },
        onComplete:function(){
            $.colorbox.resize();
        }
    };
    $('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
    $("#cboxLoadingGraphic").html("<i class='ace-icon fa fa-spinner orange fa-spin'></i>");//let's add a custom loading icon
    $(document).one('ajaxloadstart.page', function(e) {
        $('#colorbox, #cboxOverlay').remove();
    });
})
</script>
<ul class="ace-thumbnails clearfix">
    <!--
<li>
<div>
<img width="150" height="150" alt="150x150" src="assets/images/gallery/thumb-5.jpg" />
<div class="text">
<div class="inner">
<a href="assets/images/gallery/image-5.jpg" data-rel="colorbox"><i class="ace-icon fa fa-search-plus"></i></a>
<a href="#"><i class="ace-icon fa fa-times red"></i></a>
</div>
</div>
</div>
</li>
    -->
</ul>   