{* Extend our master template *}
{extends file="master.tpl"}
{block name=css}
<!-- page specific plugin styles -->
<link rel="stylesheet" href="{$assets_path}plugins/treetable/jquery.treetable.css" />
<link rel="stylesheet" href="{$assets_path}plugins/treetable/jquery.treetable.theme.default.css" />
{/block}
{block name=script}
<script src="{$assets_path}plugins/treetable/jquery.treetable.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
$("#example-advanced").treetable({ expandable: true });
// Highlight selected row
$("#example-advanced tbody").on("mousedown", "tr", function() {
    $(".selected").not(this).removeClass("selected");
    $(this).toggleClass("selected");
});
/* ]]> */
$(document).ready(function() {
    $("#sidebar_image").addClass("active");
    $(".detail").click(function () {
        id = $(this).attr('id');
        location.href = "{site_url()}images/view?dm="+ id;
    });
    $(".delete").click(function () {
        id = $(this).attr('id');
        if (confirm('Bạn có chắc xóa ?')) {
            location.href = "{site_url()}images/deleteDanhmuc?id="+ id;
        }
    });
    $(".edit").click(function () {
        id = $(this).attr('id');
        location.href = "{site_url()}images/updateDanhmuc?id="+ id;
    });
});
</script>
{/block}
{block name=body}
<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li class="active">Danh muc hinh anh</li>
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<h3 class="header smaller lighter blue">Danh muc hinh anh</h3>
<div class="clearfix">
<div class="pull-left tableTools-container"></div>
</div>
    <div>
        <span><a href="{site_url()}images/insertDanhmuc">Thêm danh mục</a></span>
        <span style="margin:0px 5px;">|</span>
        <span><a href="javascript:void(-1);" onClick="jQuery('#example-advanced').treetable('expandAll'); return false;">Mở rộng</a></span>
        <span style="margin:0px 5px;">|</span>
        <span><a href="javascript:void(-1);" onClick="jQuery('#example-advanced').treetable('collapseAll'); return false;">Thu lại</a></span>
    </div>
    <table id="example-advanced" class="table table-bordered table-striped table-condensed">
    <thead>
    <tr valign="top">
    <th>Tên danh mục</th>
    <th>Hình ảnh</th>
    <th></th>
    </tr>
    </thead>
    <tbody>
    {$treeTable}
    </tbody>
    </table>
<!-- PAGE CONTENT ENDS -->
</div>
</div>
</div>
</div>
</div>
{/block}