{* Extend our master template *}
{extends file="master.tpl"}
{block name=css}
<link rel="stylesheet" href="{$assets_path}css/colorbox.min.css" />
{/block}
{block name=script}
<script src="{$assets_path}js/jquery.colorbox.min.js"></script>
<script src="{$assets_path}js/jquery.form.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar_image").addClass("active");
    $("#danhsachhinh").load("{site_url()}images/view_ajax?dm={$dm.imgcat_id}", function(){});
    // adding and removing file input box
    //elements
    var progressbox = $('#progressbox'); //progress bar wrapper
    var progressbar = $('#progressbar'); //progress bar element
    var statustxt = $('#statustxt'); //status text element
    var submitbutton = $("#SubmitButton"); //submit button
    var myform = $("#UploadForm"); //upload form
    var completed = '0%'; //initial progressbar value
    var FileInputsHolder = $('#AddFileInputBox'); //Element where additional file inputs are appended
    var MaxFileInputs = 6; //Maximum number of file input boxs
    var i = $('#AddFileInputBox div').size() + 1;
    $('#AddMoreFileBox').on('click', function() {
        if (i < MaxFileInputs) {
            /*
            $('<label class="ace-file-input">\n\
                <input type="file" id="hinh[]" name="hinh[]" />\n\
                <span class="ace-file-container" data-title="Choose">\n\
                    <span class="ace-file-name" data-title="No File ..."><i class=" ace-icon fa fa-upload"></i></span>\n\
                </span>\n\
            </label>').appendTo(FileInputsHolder);
            */
            $('<div style="margin-top:3px;" class="fileupload fileupload-new" data-provides="fileupload"><span class="btn btn-theme02 btn-file"><input class="default" type="file" id="hinh[]" name="hinh[]" /></span> <span><a href="javascript:removeFileBox(\'removeFileBox'+ i +'\')" class="small2" id="removeFileBox'+ i +'"><img src="{$assets_path}icon/close_icon.gif" border="0" /></a></span></div>').appendTo(FileInputsHolder);
            i++;
        }
        return false;
    });
    $('#removeFileBox').click(function () {
        alert('mia');
        if ( i > 1 ) {
            $(this).parent().parent().remove();
            console.log('xxx' + $(this).html());
            i--;
        }
        else alert('Chan qua');
        return false;
    });
    $("#ShowForm").click(function () {
        $("#uploaderform").slideToggle();
    });
    $(myform).ajaxForm({
        beforeSend: function() { //brfore sending form
            submitbutton.attr('disabled', 'disabled'); // disable upload button
            statustxt.empty();
            progressbox.show(); //show progressbar
            progressbar.width(completed); //initial value 0% of progressbar
            statustxt.html(completed); //set status text
            statustxt.css('color','#000'); //initial color of status text
        },
        uploadProgress: function(event, position, total, percentComplete) { //on progress
            progressbar.width(percentComplete + '%') //update progressbar percent complete
            statustxt.html(percentComplete + '%'); //update status text
            if(percentComplete>50) statustxt.css('color','#fff'); //change status text to white after 50%
            else statustxt.css('color','#000');
        },
        complete: function(response) { // on complete
            alert('Finish!');
            myform.resetForm();  // reset form
            submitbutton.removeAttr('disabled'); //enable submit button
            progressbox.hide(); // hide progressbar
            $("#uploaderform").slideUp(); // hide form after upload
            //window.location.replace(pathname);
            //window.location.reload();
            $("#danhsachhinh").load("{site_url()}images/view_ajax?dm={$dm.imgcat_id}", function(){});
        }
    });
});
function removeFileBox(xxx) {
    $(xxx).parent().parent().remove()
    console.log('xxx' + $(xxx).parent().parent().val());
}
</script>
{/block}
{block name=body}
<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li><a href="{site_url()}images">Danh muc hinh anh</a></li>
{assign var="stt" value=0}
{foreach from=$path item=p}
    {assign var="stt" value={$stt} + 1}
    {if count($path) > $stt}
        <li><a href="{site_url()}images/view?dm={$p.imgcat_id}">{$p.imgcat_name}</a></li>
    {else}
        <li class="active">{$p.imgcat_name}</li>
    {/if}
{/foreach}
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<center>
    <div id="uploaderform" style="display:none;">
    <form action="{site_url()}images/view?dm={$dm.imgcat_id}" method="post" enctype="multipart/form-data" name="UploadForm" id="UploadForm">
    <input type="hidden" id="isSent" name="isSent" value="OK" />
    <div id="AddFileInputBox" style="width:250px;"></div>
    <div style="margin-top:5px;"><a href="#" id="AddMoreFileBox">Thêm hình để upload</a></div>
    <div style="margin-top:5px;"><button type="submit" id="SubmitButton" class="btn btn-theme02">Submit</button></div>
    <div style="margin-top:5px;" id="progressbox"><div id="progressbar"></div><div id="statustxt">0%</div></div>
    </form>
    </div>
    <div id="uploadResults">
        <div align="center" style="margin:20px;"><a href="javascript:void(-1)" id="ShowForm">Thêm hình</a></div>
    </div>
</center>

<h3 class="header smaller lighter blue">Danh sach hinh anh</h3>
<div class="clearfix">
<div class="pull-left tableTools-container"></div>
</div>
<div id="danhsachhinh"></div>
<!-- PAGE CONTENT ENDS -->
</div>
</div>
</div>
</div>
</div>
{/block}