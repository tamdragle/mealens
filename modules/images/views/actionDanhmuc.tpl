{* Extend our master template *}
{extends file="master.tpl"}
{block name=css}{/block}
{block name=script}
<script src="{$assets_path}js/jquery.validate.min.js"></script>
<script src="{$assets_path}js/jquery-additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar_image").addClass("active");
    $('#validation-form').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        ignore: "",
        rules: {
            name: {
                required: true
            }
        },
        messages: {

        },
        submitHandler: function (form) {
            $("button[type='submit']").attr("disabled", true).text("Please wait...");
            form.submit();
        },
        invalidHandler: function (form) {
        }
    });
});
</script>
{/block}
{block name=body}
<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li><a href="{site_url()}images">Danh muc hinh anh</a></li>
<li class="active">{$title}</li>
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<h3 class="header smaller lighter blue">{$title}</h3>
<div class="clearfix">
<div class="pull-left tableTools-container"></div>
</div>

<form class="form-horizontal" role="form" id="validation-form" action="" method="post" enctype="multipart/form-data">
<input type="hidden" id="isSent" name="isSent" value="OK" />
<input type="hidden" id="id" name="id" value="{$val.id}" />
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="name">Tên:</label>
<div class="col-sm-9">
<input type="text" id="name" name="name" value="{$val.name}" class="input-xlarge" />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="address">Parent:</label>
<div class="col-sm-9">
<select class="form-control" id='parent' name='parent'>
<option value='0'>Root</option>
{html_options options=$options selected=$val.parent}
</select>
</div>
</div>
<div class="clearfix form-actions">
<div class="col-md-offset-3 col-md-9">
<button class="btn btn-xs btn-info" type="submit">
<i class="ace-icon fa fa-check bigger-110"></i>
Submit
</button>
<button class="btn btn-xs" type="reset">
<i class="ace-icon fa fa-undo bigger-110"></i>
Reset
</button>
</div>
</div>
</form>
<!-- PAGE CONTENT ENDS -->
</div>
</div>
</div>
</div>
</div>
{/block}