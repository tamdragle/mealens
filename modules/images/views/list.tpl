{* Extend our master template *}
{extends file="master.tpl"}
{block name=css}
<link rel="stylesheet" href="{$assets_path}css/colorbox.min.css" />
{/block}
{block name=script}
<script src="{$assets_path}js/jquery.colorbox.min.js"></script>
<script src="{$assets_path}js/jquery.form.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar_image").addClass("active");
    $("#danhsachhinh").load("{site_url()}images/view_ajax?dm={$dm.imgcat_id}", function(){});
    // adding and removing file input box
    //elements
    var progressbox = $('#progressbox'); //progress bar wrapper
    var progressbar = $('#progressbar'); //progress bar element
    var statustxt = $('#statustxt'); //status text element
    var submitbutton = $("#SubmitButton"); //submit button
    var myform = $("#UploadForm"); //upload form
    var completed = '0%'; //initial progressbar value
    $(myform).ajaxForm({
        beforeSend: function() { //brfore sending form
            submitbutton.attr('disabled', 'disabled'); // disable upload button
            statustxt.empty();
            progressbox.show(); //show progressbar
            progressbar.width(completed); //initial value 0% of progressbar
            statustxt.html(completed); //set status text
            statustxt.css('color','#000'); //initial color of status text
        },
        uploadProgress: function(event, position, total, percentComplete) { //on progress
            progressbar.width(percentComplete + '%') //update progressbar percent complete
            statustxt.html(percentComplete + '%'); //update status text
            if(percentComplete>50) statustxt.css('color','#fff'); //change status text to white after 50%
            else statustxt.css('color','#000');
        },
        complete: function(response) { // on complete
            if (response.status == 'false') {
                alert('Co loi xay ra');
            }
            else {
                myform.resetForm();  // reset form
                submitbutton.removeAttr('disabled'); //enable submit button
                progressbox.hide(); // hide progressbar
                //$("#danhsachhinh").load("{site_url()}images/view_ajax?dm={$dm.imgcat_id}", function(){});
            }
        }
    });
});
</script>
{/block}
{block name=body}
<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li><a href="{site_url()}images">Danh muc hinh anh</a></li>
{assign var="stt" value=0}
{foreach from=$path item=p}
    {assign var="stt" value={$stt} + 1}
    {if count($path) > $stt}
        <li><a href="{site_url()}images/view?dm={$p.imgcat_id}">{$p.imgcat_name}</a></li>
    {else}
        <li class="active">{$p.imgcat_name}</li>
    {/if}
{/foreach}
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<div class="row">
    <div class="col-xs-12 col-sm-4"></div>
    <div class="col-xs-12 col-sm-4">
    <div class="widget-box">
    <div class="widget-header">
    <h4 class="widget-title">Select Files to Upload:</h4>
    <span class="widget-toolbar"><a href="#" data-action="collapse"><i class="ace-icon fa fa-chevron-up"></i></a></span>
    </div>
    <div class="widget-body" style="display: block;">
    <div class="widget-main">
        <form action="{site_url()}images/view?dm={$dm.imgcat_id}" method="post" enctype="multipart/form-data" name="UploadForm" id="UploadForm">
            <input type="hidden" id="isSent" name="isSent" value="OK" />
            <div><input name="usr_files[]" type="file" multiple="" /></div>
            <div class="space-4"></div>
            <div><input type="submit" id="SubmitButton" value="Upload" class="btn btn-primary btn-block"/></div>
            <div class="space-4"></div>
            <center><div id="progressbox"><div id="progressbar"></div><div id="statustxt">0%</div></div></center>
        </form>
    </div>
    </div>
    </div>
    </div>
    <div class="col-xs-12 col-sm-4"></div>
</div>

<h3 class="header smaller lighter blue">Danh sach hinh anh</h3>
<div class="clearfix">
<div class="pull-left tableTools-container"></div>
</div>
<div id="danhsachhinh"></div>
<!-- PAGE CONTENT ENDS -->
</div>
</div>
</div>
</div>
</div>
{/block}