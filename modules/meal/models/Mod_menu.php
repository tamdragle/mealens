<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
get_instance()->load->iface('DefaultInterface');
class Mod_menu extends MY_Model implements DefaultInterface {
    var $id = FALSE;
    var $z = FALSE;
    var $year = FALSE;
    function _get() {
        $this->db->select('*');
        $this->db->from('menu');
        if ($this->id) $this->db->where('mid', $this->id);
        else {
            $this->db->where('mz', $this->z);
            $this->db->where('myear', $this->year);
        }
        $query = $this->db->get();
        $result = $query->row_array();
        $query->free_result();
	return ($result) ? $result : false;                
    }
    function _getMenu($mid) {
        $sql = "
            SELECT
                menu_detail.id, 
                menu_detail.mid, 
                dish.did, 
                dish.dname, 
                dish.dpic, 
                menu_detail.mprice, 
                menu_detail.mquantity, 
                SUM(orders_meal.soluong) AS soluong
            FROM menu_detail
            INNER JOIN dish ON menu_detail.did = dish.did
            LEFT JOIN orders_meal ON menu_detail.mid = orders_meal.id_menu AND menu_detail.did = orders_meal.id_monan
            WHERE menu_detail.mid = {$mid}
            GROUP BY dish.did
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->free_result();
	return ($result) ? $result : false;
    }
    function _register($reg, $detail) {
        $this->db->trans_start();
        if (count($reg) > 0) {
            if (!$this->db->insert('menu', array(
                'mid' => $reg['mid'],
                'mz' => $reg['mz'],
                'myear' => $reg['myear'],
                'mdate' => $reg['mdate']
            ))) return FALSE;
        }
        if (count($detail) > 0) $this->db->insert_batch('menu_detail', $detail);
        $this->db->trans_complete();
        return TRUE;
    }
    function _quantity($values) {
        $data = array(
            'mquantity' => $values['quantity']
        );
        $this->db->where('id', $values['id']);
        $this->db->update('menu_detail', $data);
        return ($this->db->affected_rows()) ? TRUE : FALSE;                
    }
    function _getMax() {
        $this->db->select_max('mid', 'maxid');
        $this->db->from('menu');
        $query = $this->db->get();
        $result = $query->row_array();
        $query->free_result();
	return ($result) ? $result['maxid'] : false;        
    }
    function _getOrders($mid) {
        $sql = "
            SELECT meal_orders.moid, 
                meal_orders.modate, 
                meal_orders.mostatus, 
                meal_orders.moname, 
                meal_orders.motel, 
                meal_orders.moaddress, 
                users.uid, 
                users.uname, 
                SUM(orders_meal.soluong) AS soluong,
                SUM(orders_meal.tong) AS tong
            FROM meal_orders INNER JOIN users ON meal_orders.uid = users.uid
                INNER JOIN orders_meal ON meal_orders.moid = orders_meal.id_donhang
            WHERE meal_orders.mid = {$mid}
            GROUP BY meal_orders.moid
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->free_result();
	return ($result) ? $result : false;
    }
    function _getOrderDetail($id) {
        $this->db->select('*');
        $this->db->from('meal_orders');
        $this->db->where('moid', $id);
        $query = $this->db->get();
        $result = $query->row_array();
        $query->free_result();
	return ($result) ? $result : FALSE;        
    }
    function _getOrderDetails($id) {
        $this->db->select('o.gia, o.soluong, o.tong, d.dname, d.dpic');
        $this->db->from('orders_meal AS o');
        $this->db->join('dish AS d', 'o.id_monan = d.did');
        $this->db->where('o.id_donhang', $id);
        $query = $this->db->get();
        $result = $query->result_array();
        $query->free_result();
	return ($result) ? $result : FALSE;        
    }
    function _status($values) {
        $data = array(
            'mostatus' => $values['val']
        );
        $this->db->where('moid', $values['id']);
        $this->db->update('meal_orders', $data);
        return ($this->db->affected_rows()) ? TRUE : FALSE;        
    }
}
/* End of file */