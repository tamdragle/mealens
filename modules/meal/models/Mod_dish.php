<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
get_instance()->load->iface('DefaultInterface');
class Mod_dish extends MY_Model implements DefaultInterface {
    var $type = FALSE;
    var $table = 'dish';
    var $column = array('dname','dpic'); //set column field database for order and search
    var $order = array('did' => 'desc'); // default order 

    //Datatable
    private function _get_datatables_query() {
        $this->db->from($this->table);
        if ($this->type) $this->db->where('dtid', $this->type);
        $i = 0;
        foreach ($this->column as $item) {//loop column 
            if (@$_POST['search']['value']) {//if datatable send POST for search
                if ($i === 0 ) {//first loop
                    $this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND. 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                //last loop
                if (count($this->column) - 1 == $i) $this->db->group_end(); //close bracket
            }
            $column[$i] = $item; //set column array variable to order processing
            $i++;
        }
        if (isset($_POST['order'])) {//here order processing
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        elseif (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables() {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all() {
        $this->db->from($this->table);
        if ($this->type) $this->db->where('dtid', $this->type);
        return $this->db->count_all_results();
    }
    function _create($values) {
        if (!$this->db->insert($this->table, array(
            'dname' => $values['name'],
            'dpic' => $values['pic'],
            'dprice' => $values['price'],
            'dfeature' => $values['feature'],
            'dintro' => $values['intro'],
            'dtid' => $values['type']
        ))) return FALSE;
        return TRUE;
    }
    function _gets() {
        $this->db->select('d.did, d.dname, dt.dtname');
        $this->db->from($this->table.' AS d');
        $this->db->join('dish_type AS dt', 'd.dtid = dt.dtid');
        $query = $this->db->get();
        $result = $query->result_array();
        $query->free_result();
	return ($result) ? $result : FALSE;        
    }
    function _getType($type = FALSE) {
        $this->db->select('*');
        $this->db->from('dish_type');
        if ($type) $this->db->where('dtid', $type);
        $query = $this->db->get();
        $result = ($type) ? $query->row_array() : $query->result_array();
        $query->free_result();
	return ($result) ? $result : false;                
    }
    function _getPrice($id) {
        $this->db->select('dprice');
        $this->db->from($this->table);
        $this->db->where('did', $id);
        $query = $this->db->get();
        $result = $query->row_array();
        $query->free_result();
	return ($result) ? $result['dprice'] : false;
    }
}
/* End of file */