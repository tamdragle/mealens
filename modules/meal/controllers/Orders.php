<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends MY_Controller {
    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('ssAdminId')) redirect(site_url().'admin');
        $this->permarr = $this->permission->getPermissions($this->permissions[_MOD_MEAL_MENU]);
        $this->load->model('mod_menu');
    }
    function index() {
        if (!$this->permarr['read']) redirect(site_url().'admin/denied?w=read');

        $monthNames = Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
        $cMonth = ($this->input->get('month')) ? $this->input->get('month') : date("n");
        $cYear = ($this->input->get('year')) ? $this->input->get('year') : date("Y");

        $prev_year = $cYear;
        $next_year = $cYear;
        $prev_month = $cMonth-1;
        $next_month = $cMonth+1;

        if ($prev_month == 0) {
            $prev_month = 12;
            $prev_year = $cYear - 1;
        }
        if ($next_month == 13) {
            $next_month = 1;
            $next_year = $cYear + 1;
        }
        
        $calendar = '';
        $timestamp = mktime(0,0,0,$cMonth,1,$cYear);
        $maxday = date("t",$timestamp);
        $thismonth = getdate($timestamp);
        $startday = $thismonth['wday'];
        $thu = date('w',mktime(0,0,0,$cMonth,1,$cYear));//Lay thu cua ngay dau tien trong thang
        for ($i=0; $i<($maxday+$startday); $i++) {
            if (($i % 7) == 0) $calendar .= "<tr style='text-align:center;'>";
            if ($i < $startday) $calendar .= "<td></td>";
            else {
                $cDate= ($i - $startday + 1);
                if ($thu == 0 or $thu == 6) $calendar .= "<td style='height:80px; vertical-align:middle;'><span class='pink'>{$cDate}</span></td>";
                else {
                    $calendar .= "<td style='vertical-align:middle;'>"; 
                    $dateOfYear = date('z', strtotime("{$cYear}-{$cMonth}-{$cDate}"));
                    $calendar .= '
                        <a href="'.site_url().'meal/orders/view?z='.$dateOfYear.'&year='.$cYear.'" title="Xem danh sách đơn hàng">
                        '.$cDate.'
                        <i class="ace-icon fa fa-search"></i>
                        </a>
                    ';
                    $calendar .= "</td>"; 
                }
                $thu++;
            }
            if (($i % 7) == 6) {
                $calendar .= "</tr>";
                $thu = 0;
            }
        }
        $this->parser->assign('info', array(
            'pMonth' => $prev_month,
            'cMonth' => $cMonth,
            'nMonth' => $next_month,
            'pYear' => $prev_year,
            'cYear' => $cYear,
            'nYear' => $next_year
        ));
        $this->parser->assign('monthNames', $monthNames);
        $this->parser->assign('calendar', $calendar);
        $this->parser->parse('orders/calendar');
    }
    function view() {
        $z = $this->input->get('z');
        $year = $this->input->get('year');
        $this->mod_menu->z = $z;
        $this->mod_menu->year = $year;
        $menu = $this->mod_menu->_get($z, $year);
        if ($menu) {
            $this->parser->assign('menu', $this->mod_menu->_getOrders($menu['mid']));
        }
        else {
            $this->parser->assign('menu', FALSE);
        }
        $this->config->load('my_conf');
        $this->parser->assign('status', $this->config->item('order_status'));
        
        $this->parser->assign('date', dayofyear2date($z, $year, 'd-m-Y'));
        $this->parser->assign('month', date("m", strtotime(dayofyear2date($z, $year))));
        $this->parser->assign('z', $z);
        $this->parser->assign('year', $year);
        $this->parser->parse('orders/list');
    }
    function detail() {
        $this->config->load('my_conf');
        $this->parser->assign('path_upload', $this->config->item('path_upload'));
        $this->parser->assign('shipping_value', SHIPPING_VALUE);
        $this->parser->assign('shipping_fee', SHIPPING_FEE);
        $this->parser->assign('detail', $this->mod_menu->_getOrderDetail($this->input->get('id')));
        $this->parser->assign('details', $this->mod_menu->_getOrderDetails($this->input->get('id')));
        $this->parser->parse('orders/detail');        
    }
    function status() {
        $params = $this->input->post();
        $detail = $this->mod_menu->_getOrderDetail($params['id']);
        if (in_array($detail['mostatus'], array(0,1,2))) {
            if ($this->mod_menu->_status($params)) {
                echo '{"error_code":200,"message":"Cập nhật trạng thái thành công"}';
            }
            else {
                echo '{"error_code":201,"message":"Đã giao hàng, không thể cập nhật"}';
            }
        }
        else {
            echo '{"error_code":202,"message":"'.$detail['mostatus'].' Không thể thay đổi trạng thái"}';
        }
        exit;            
    }
}
