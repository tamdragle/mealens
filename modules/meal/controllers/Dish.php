<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dish extends MY_Controller {
    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('ssAdminId')) redirect(site_url().'admin');
        $this->permarr = $this->permission->getPermissions($this->permissions[_MOD_MEAL_DISH]);
        $this->load->model('mod_dish');
    }
    function index() {
        if (!$this->permarr['read']) redirect(site_url().'admin/denied?w=read');
        $types = array();
        $typeList = $this->mod_dish->_getType();
        foreach ($typeList as $type) {
            $types[$type['dtid']] = $type['dtname'];
        }
        $this->parser->assign('type', $this->input->get('type'));
        $this->parser->assign('types', $types);
        $this->parser->parse('dish/list');
    }
    function ajax_list() {
        $this->config->load('my_conf');
        $this->mod_dish->type = $this->input->get('type');
        $list = $this->mod_dish->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $d) {
            $no++;
            $row = array();
            $row[] = $d->dname;
            if (is_file("{$this->config->item('path_upload')}meal/thumb_{$d->dpic}")) $row[] = "<img src='/{$this->config->item('path_upload')}meal/thumb_{$d->dpic}' />";
            elseif (is_file("{$this->config->item('path_upload')}meal/{$d->dpic}")) $row[] = "<img src='/{$this->config->item('path_upload')}meal/{$d->dpic}' style='max-width:100px;' />";
            else $row[] = '';
            $row[] = $d->dfeature;
            $row[] = number_format($d->dprice);
            $data[] = $row;
        }

        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mod_dish->count_all(),
            "recordsFiltered" => $this->mod_dish->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    function add() {
        if (!$this->permarr['write']) redirect(site_url().'admin/denied?w=write');
        if ($this->input->post('isSent') == 'OK') {
            $params = $this->input->post();
            
            $this->config->load('my_conf');
            $config = array();
            $config['upload_path'] = $this->config->item('path_upload').'meal';
            $config['allowed_types'] = '*';
            $config['file_name'] = date('dmYHis');
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('picture')) {
                $files = $this->upload->data();
                $params['pic'] = $files['file_name'];
                //Resize
                /*
                $this->load->library('imgresize');
                if((intval($files['image_width']) > 500) OR (intval($files['image_height']) > 500)) {
                    $thumb_name = 'thumb_'.$files['raw_name'].$files['file_ext'];
                    $objResize = $this->imgresize->getInstanceOf($files['full_path'], $files['file_path'].$thumb_name, 500, 500);
                    $objResize->getResizedImage();
                    unset($objResize);
                }
                */
            }
            else $params['pic'] = '';
            if ($this->mod_dish->_create($params)) {
                redirect(site_url().'meal/dish?type='.$params['type']);
            }
            redirect(site_url().'meal/dish/add');
        }
        $this->parser->assign('type', $this->input->get('type'));
        $this->parser->parse('dish/insert');
    }
}
