<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MY_Controller {
    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('ssAdminId')) redirect(site_url().'admin');
        $this->permarr = $this->permission->getPermissions($this->permissions[_MOD_MEAL_MENU]);
        $this->load->model('mod_menu');
    }
    function index() {
        if (!$this->permarr['read']) redirect(site_url().'admin/denied?w=read');

        $monthNames = Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
        $cMonth = ($this->input->get('month')) ? $this->input->get('month') : date("n");
        $cYear = ($this->input->get('year')) ? $this->input->get('year') : date("Y");

        $prev_year = $cYear;
        $next_year = $cYear;
        $prev_month = $cMonth-1;
        $next_month = $cMonth+1;

        if ($prev_month == 0) {
            $prev_month = 12;
            $prev_year = $cYear - 1;
        }
        if ($next_month == 13) {
            $next_month = 1;
            $next_year = $cYear + 1;
        }
        
        $calendar = '';
        $timestamp = mktime(0,0,0,$cMonth,1,$cYear);
        $maxday = date("t",$timestamp);
        $thismonth = getdate($timestamp);
        $startday = $thismonth['wday'];
        $thu = date('w',mktime(0,0,0,$cMonth,1,$cYear));//Lay thu cua ngay dau tien trong thang
        for ($i=0; $i<($maxday+$startday); $i++) {
            if (($i % 7) == 0) $calendar .= "<tr style='text-align:center;'>";
            if ($i < $startday) $calendar .= "<td></td>";
            else {
                $cDate= ($i - $startday + 1);
                if ($thu == 0 or $thu == 6) $calendar .= "<td style='height:80px; vertical-align:middle;'><span class='pink'>{$cDate}</span></td>";
                else {
                    $calendar .= "<td style='vertical-align:middle;'>"; 
                    $dateOfYear = date('z', strtotime("{$cYear}-{$cMonth}-{$cDate}"));
                    $calendar .= '
                        <a href="'.site_url().'meal/menu/view?z='.$dateOfYear.'&year='.$cYear.'" title="Xem thực đơn">
                        '.$cDate.'
                        <i class="ace-icon fa fa-search"></i>
                        </a>
                    ';
                    $calendar .= "</td>"; 
                }
                $thu++;
            }
            if (($i % 7) == 6) {
                $calendar .= "</tr>";
                $thu = 0;
            }
        }
        $this->parser->assign('info', array(
            'pMonth' => $prev_month,
            'cMonth' => $cMonth,
            'nMonth' => $next_month,
            'pYear' => $prev_year,
            'cYear' => $cYear,
            'nYear' => $next_year
        ));
        $this->parser->assign('monthNames', $monthNames);
        $this->parser->assign('calendar', $calendar);
        $this->parser->parse('menu/calendar');
    }
    function view() {
        $z = $this->input->get('z');
        $year = $this->input->get('year');
        $this->mod_menu->z = $z;
        $this->mod_menu->year = $year;
        $menu = $this->mod_menu->_get($z, $year);
        if ($menu) {
            $this->parser->assign('menu', $this->mod_menu->_getMenu($menu['mid']));
        }
        else {
            $this->parser->assign('menu', FALSE);
        }
        $this->parser->assign('date', dayofyear2date($z, $year, 'd-m-Y'));
        $this->parser->assign('month', date("m", strtotime(dayofyear2date($z, $year))));
        $this->parser->assign('z', $z);
        $this->parser->assign('year', $year);
        $this->parser->parse('menu/menu');
    }
    function register() {
        $this->load->model('mod_dish');
        if ($this->input->post('isSent') == 'OK') {
            $params = $this->input->post();
            $reg = array();
            $detail = array();
            if ($params['function'] == 'insert') {
                $mid = $this->mod_menu->_getMax();
                $mid++;
                $reg['mid'] = $mid;
                $reg['mz'] = $params['z'];
                $reg['myear'] = $params['year'];
                $reg['mdate'] = dayofyear2date($params['z'], $params['year'], 'Y-m-d');
            }
            else {
                $mid = $params['mid'];
            }
            foreach ($params['dualdish'] as $dish) {
                $detail[] = array(
                    'mid' => $mid,
                    'mprice' => $this->mod_dish->_getPrice($dish),
                    'did' => $dish
                );
            }
            $this->mod_menu->_register($reg, $detail);
            redirect(site_url()."meal/menu/view?z={$params['z']}&year={$params['year']}");
        }
        else {
            $z = $this->input->get('z');
            $year = $this->input->get('year');
            $this->mod_menu->z = $z;
            $this->mod_menu->year = $year;
            $menu = $this->mod_menu->_get($z, $year);
            
            $option_selected = array();
            if ($menu) {
                $menu_detail = $this->mod_menu->_getMenu($menu['mid']);
                foreach ($menu_detail as $m) {
                    $option_selected[] = $m['did'];
                }
                $this->parser->assign('function', 'update');
                $this->parser->assign('mid', $menu['mid']);
            }
            else $this->parser->assign('function', 'insert');

            $option_dish = array();
            $dishs = $this->mod_dish->_gets();
            foreach ($dishs as $dish) {
                if (!in_array($dish['did'], $option_selected)) $option_dish[$dish['dtname']][$dish['did']] = $dish['dname'];
            }
            $this->parser->assign('option_dish', $option_dish);
            $this->parser->assign('date', dayofyear2date($z, $year, 'd-m-Y'));
            $this->parser->assign('month', date("m", strtotime(dayofyear2date($z, $year))));
            $this->parser->assign('z', $z);
            $this->parser->assign('year', $year);
            $this->parser->parse('menu/register');
        }
    }
    function quantity() {
        if (!$this->permarr['update']) {
            echo json_encode(array("status" => 'denied'));
            exit;
        }
        if ($this->input->post('isSent') == 'OK') {
            $items = $this->input->post();
            if ($this->mod_menu->_quantity($items)) {
                echo json_encode(array("status" => TRUE));
            }
        }
    }
}
