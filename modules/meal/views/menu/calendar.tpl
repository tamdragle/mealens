{* Extend our master template *}
{extends file="master.tpl"}
{block name=css}{/block}
{block name=script}
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar_meal").addClass("active open");
    $("#sidebar_meal_menu").addClass("active");
});
</script>
{/block}
{block name=body}
<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li><a href="#">Cơm văn phòng</a></li>
<li class="active">Lịch thực đơn</li>
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="space-2"></div>
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<h3 class="header smaller lighter blue">Lịch thực đơn</h3>
<div class="clearfix">
<div class="pull-left tableTools-container"></div>
</div>
<ul class="pager">
<li class="previous"><a href="{site_url()}meal/menu?month={$info['pMonth']}&year={$info['pYear']}">← Tháng trước</a></li>
<li class="next"><a href="{site_url()}meal/menu?month={$info['nMonth']}&year={$info['nYear']}">Tháng sau →</a></li>
</ul>
<table id="table" class="table table-bordered table-striped">
<thead>
<tr>
    <th colspan="7" style="text-align:center;">{$monthNames[$info['cMonth']-1]} - {$info['cYear']}</th>
</tr>        
<tr>
<th style="text-align:center;"><span class="pink">Chủ nhật</span></th>
<th style="text-align:center;">Thứ hai</th>
<th style="text-align:center;">Thứ ba</th>
<th style="text-align:center;">Thứ tư</th>
<th style="text-align:center;">Thứ năm</th>
<th style="text-align:center;">Thứ sáu</th>
<th style="text-align:center;"><span class="pink">Thứ bảy</span></th>
</tr>
</thead>
<tbody>
{$calendar}
</tbody>
</table>
<!-- PAGE CONTENT ENDS -->
</div>
</div>
</div>
</div>
</div>
{/block}