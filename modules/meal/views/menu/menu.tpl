{* Extend our master template *}
{extends file="master.tpl"}
{block name=css}{/block}
{block name=script}
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <form id="form" method="post">
            <input type="hidden" id="isSent" name="isSent" value="OK" />
            <input type="hidden" id="id" name="id" />
            <div class="modal-body form">
                <div class="form-group">
                    <label>Số lượng:</label>
                    <input class="form-control" type="text" id="quantity" name="quantity" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="quantity_change()" class="btn btn-xs btn-primary">Submit</button>
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar_meal").addClass("active open");
    $("#sidebar_meal_menu").addClass("active");
});
function addDish() {
    document.location.href = "{site_url()}meal/menu/register?z={$z}&year={$year}";
}
function change_quantity(id, quantity) {
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('#id').val(id);
    $('#quantity').val(quantity);
    $('.modal-title').text('Thay đổi số lượng'); // Set Title to Bootstrap modal title
}
function quantity_change() {
    $.ajax({
        url : "{site_url()}meal/menu/quantity",
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data) {
           //if success close modal and reload ajax table
           $('#modal_form').modal('hide');
           if (data.status == 'denied') document.location.href = '{site_url()}admin/denied?w=write';
           else {
               var qua = $('#id').val().toString();
               $('#dish_'+ qua).text($('#quantity').val());
           }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the cap nhat');
        }
    });
}
</script>
{/block}
{block name=body}
<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li><a href="#">Cơm văn phòng</a></li>
<li><a href="{site_url()}meal/menu?month={$month}&year={$year}">Lịch thực đơn</a></li>
<li class="active">Chi tiết thực đơn</li>
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="space-2"></div>
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<h3 class="header smaller lighter blue">Chi tiết thực đơn</h3>
<div class="row">
<div class="col-xs-12">
<div class="pull-left">
    <button class="btn btn-xs btn-primary" onclick="addDish()"><i class="ace-icon fa fa-plus"></i> Thêm món ăn</button>
</div>
</div>
</div>
<div class="space-2"></div>
<table id="table" class="table table-bordered table-striped">
<thead>
<tr>
<th colspan="4" class="center blue">{$date}</th>
</tr>        
<tr>
<th>Tên món ăn</th>
<th>Đơn giá</th>
<th>Số lượng</th>
<th>Đặt hàng</th>
</tr>
</thead>
<tbody>
{if $menu}
    {foreach from=$menu item=m}
    <tr>
    <td>{$m['dname']}</td>
    <td>{number_format($m['mprice'])}</td>
    <td><span id="dish_{$m['id']}">{$m['mquantity']}</span> <a class="green" href="javascript:void(-1);" title="Edit" onclick="change_quantity({$m['id']}, $('#dish_{$m['id']}').text())"><i class="ace-icon fa fa-pencil smaller"></i></a></td>
    <td>{$m['soluong']}</td>
    </tr>
    {/foreach}
{else}
    <tr>
        <td colspan="4" class="center red">Chưa có dữ liệu</td>  
    </tr>
{/if}
</tbody>
</table>
<!-- PAGE CONTENT ENDS -->
</div>
</div>
</div>
</div>
</div>
{/block}