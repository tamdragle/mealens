{* Extend our master template *}
{extends file="master.tpl"}
{block name=script}
<script src="{$assets_path}plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{$assets_path}plugins/datatables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar_meal").addClass("active open");
    $("#sidebar_meal_dish").addClass("active");
    
    load_person({$type});
});
function load_person(typeID) {
    table = $('#table').DataTable({ 

        "processing": true,
        "serverSide": true,
        //"paging": true,
        //"lengthChange": false,
        //"searching": false,
        //"ordering": true,
        //"info": true,
        //"autoWidth": true,
        "order": [],

        "ajax": {
            "url": (typeID) ? "{site_url()}meal/dish/ajax_list?type="+ typeID : "{site_url()}meal/dish/ajax_list",
            "type": "POST"
        },
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    });
}
function reload_table() {
    table.ajax.reload(null,false); //reload datatable ajax 
}
function gotoType(id) {
    document.location.href='{site_url()}meal/dish?type='+ id;
}
function addDish(type) {
    document.location.href='{site_url()}meal/dish/add?type='+ type;
}
</script>
{/block}
{block name=body}
<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li><a href="#">Cơm văn phòng</a></li>
<li class="active">Danh sách món ăn</li>
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="space-2"></div>
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<h3 class="header smaller lighter blue">Danh sách món ăn</h3>
<div class="row">
<div class="col-xs-12">
<div class="pull-left">
    {if $type}<button class="btn btn-xs btn-primary" onclick="addDish($('#type').val())"><i class="ace-icon fa fa-bolt"></i> Create</button>{/if}
    <button class="btn btn-xs btn-warning" onclick="reload_table()"><i class="ace-icon fa fa-refresh"></i> Reload</button>
</div>
<div class="pull-right">
    <select id="type" name="type" aria-invalid="false" onchange="gotoType($('#type').val())">
    <option value="">All</option>
    {html_options options=$types selected=$type}
    </select>
</div>
</div>
</div>
<div class="space-2"></div>
<table id="table" class="table table-bordered table-striped">
<thead>
<tr>
<th>Tên món</th>
<th>Hình</th>
<th>Thành phần</th>
<th>Giá</th>
</tr>
</thead>
<tbody>
</tbody>
</table>

<!-- PAGE CONTENT ENDS -->
</div>
</div>
</div>
</div>
</div>
{/block}