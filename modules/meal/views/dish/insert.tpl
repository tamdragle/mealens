{* Extend our master template *}
{extends file="master.tpl"}
{block name=script}
<!-- page specific plugin scripts -->
<script src="{$assets_path}js/wizard.min.js"></script>
<script src="{$assets_path}js/jquery.validate.min.js"></script>
<script src="{$assets_path}js/jquery-additional-methods.min.js"></script>
<script src="{$assets_path}js/bootbox.js"></script>
<script src="{$assets_path}js/jquery.maskedinput.min.js"></script>
<script src="{$assets_path}js/select2.min.js"></script>
<script src="{$assets_path}plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar_meal").addClass("active open");
    $("#sidebar_meal_dish").addClass("active");
    $('#picture').ace_file_input({
        style: 'well',
        btn_choose: 'Drop files here or click to choose',
        btn_change: null,
        no_icon: 'ace-icon fa fa-cloud-upload',
        allowExt: ["jpeg", "jpg", "png", "gif" , "bmp"],
	allowMime: ["image/jpg", "image/jpeg", "image/png", "image/gif", "image/bmp"],
        droppable: true,
        thumbnail: 'small'//large | fit
        //,icon_remove:null//set null, to hide remove/reset button
        /**,before_change:function(files, dropped) {
                //Check an example below
                //or examples/file-upload.html
                return true;
        }*/
        /**,before_remove : function() {
                return true;
        }*/
        ,
        preview_error : function(filename, error_code) {
            //name of the file that failed
            //error_code values
            //1 = 'FILE_LOAD_FAILED',
            //2 = 'IMAGE_LOAD_FAILED',
            //3 = 'THUMBNAIL_FAILED'
            //alert(error_code);
        }
    }).on('change', function(){
            //console.log($(this).data('ace_input_files'));
            //console.log($(this).data('ace_input_method'));
    });
    $('#validation-form').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        ignore: "",
        rules: {
            name: {
                required: true
            }
        },
        messages: {
        },
        submitHandler: function (form) {
            $("button[type='submit']").attr("disabled", true).text("Please wait...");
            form.submit();
        },
        invalidHandler: function (form) {
        }
    });
    CKEDITOR.replace('intro');
});
</script>
{/block}
{block name=body}
<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li><a href="#">Cơm văn phòng</a></li>
<li><a href="{site_url()}meal/dish">Danh sách món ăn</a></li>
<li class="active">Thêm món ăn</li>
</ul>
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<h3 class="header smaller lighter blue">Them mon an</h3>
<div class="clearfix">
<div class="pull-left tableTools-container"></div>
</div>
<form class="form-horizontal" role="form" id="validation-form" action="" method="post" enctype="multipart/form-data">
<input type="hidden" id="isSent" name="isSent" value="OK" />
<input type="hidden" id="type" name="type" value="{$type}" />
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="name">Tên:</label>
<div class="col-sm-9">
<input type="text" id="name" name="name" class="input-xlarge" />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="picture">Picture</label>
<div class="col-sm-9">
<input type="file" id="picture" name="picture" />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="price">Thành phần:</label>
<div class="col-sm-9">
<input type="text" id="feature" name="feature" class="input-xlarge" />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="price">Giá:</label>
<div class="col-sm-9">
<input type="text" id="price" name="price" />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="price">Giới thiệu:</label>
<div class="col-sm-9">
<textarea id="intro" name="intro" rows="10" cols="80"></textarea>
</div>
</div>
<div class="clearfix form-actions">
<div class="col-md-offset-3 col-md-9">
<button class="btn btn-xs btn-info" type="submit">
<i class="ace-icon fa fa-check bigger-110"></i>
Submit
</button>
<button class="btn btn-xs" type="reset">
<i class="ace-icon fa fa-undo bigger-110"></i>
Reset
</button>
</div>
</div>
</form>
<!-- PAGE CONTENT ENDS -->
</div>
</div>
</div>
</div>
</div>
{/block}