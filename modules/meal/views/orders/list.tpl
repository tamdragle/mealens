{* Extend our master template *}
{extends file="master.tpl"}
{block name=css}
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
{/block}
{block name=script}
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar_meal").addClass("active open");
    $("#sidebar_meal_orders").addClass("active");
});
function chgStatus(id, sta) {
    $.ajax({
        url : "{site_url()}meal/orders/status",
        type: "POST",
        data: {
            'id': id,
            'val': $(sta).val()
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the cap nhat');
        }
    });
}
function viewDetail(id) {
    $.get('{site_url()}meal/orders/detail?id='+ id, {},
    function(response) {
        if (response.error_code == '201') alert(response.message);
        else $('#dialogbox').html(response);
    });
    $('#dialogbox').dialog({
        autoOpen:true,
        modal:true,
        title: "Đơn hàng của ngày {$date}",
        width: "60%",
        maxHeight: 600,
        position: { my: 'top', at: 'top+10' },
        open: function( event, ui ) {
            //alert('hello');
        }
    });    
}
</script>
{/block}
{block name=body}
<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li><a href="#">Cơm văn phòng</a></li>
<li><a href="{site_url()}meal/orders?month={$month}&year={$year}">Đơn hàng</a></li>
<li class="active">Danh sách đơn hàng</li>
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="space-2"></div>
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<h3 class="header smaller lighter blue">Danh sách đơn hàng</h3>
<table id="table" class="table table-bordered table-striped">
<thead>
<tr>
<th colspan="8" class="center blue">{$date}</th>
</tr>        
<tr>
<th>Ngày đặt</th>
<th>Người đặt</th>
<th>Số lượng</th>
<th>Số tiền</th>
<th>Người nhận</th>
<th>Địa chỉ nhận</th>
<th>ĐT người nhận</th>
<th>Trạng thái</th>
</tr>
</thead>
<tbody>
{if $menu}
    {assign var="soluong" value=0}
    {assign var="tong" value=0}
    {foreach from=$menu item=m}
    <tr>
    <td><a href='javascript:void(-1);' onclick='viewDetail({$m['moid']})'>{date("d-m-Y H:i:s", strtotime($m['modate']))}</a></td>
    <td><a href="{site_url()}users/detail?id={$m['uid']}">{$m['uname']}</a></td>
    <td>{$m['soluong']}</td>
    <td>{number_format($m['tong'])}</td>
    <td>{$m['moname']}</td>
    <td>{$m['moaddress']}</td>
    <td>{$m['motel']}</td>
    <td><select id="status[]" name="status[]" aria-invalid="false" {if in_array($m['mostatus'], array(3,4,5))}disabled="true"{/if} onchange="chgStatus({$m['moid']}, this)">
    {html_options options=$status selected={$m['mostatus']}}
    </select></td>
    </tr>
    {assign var="soluong" value={$soluong} + $m['soluong']}
    {assign var="tong" value={$tong} + $m['tong']}
    {/foreach}
{else}
    <tr>
        <td colspan="8" class="center red">Chưa có dữ liệu</td>  
    </tr>
{/if}
</tbody>
<thead>
<tr>
<th></th>
<th></th>
<th class="red">{number_format($soluong)}</th>
<th class="red">{number_format($tong)}</th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
</table>
<!-- PAGE CONTENT ENDS -->
</div>
</div>
</div>
</div>
</div>
<div id="dialogbox"></div>
{/block}