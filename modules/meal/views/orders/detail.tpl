<div class="tabbable">
<ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
<li><a data-toggle="tab" href="#dish">Thông tin món ăn</a></li>
<li class="active"><a data-toggle="tab" href="#info">Thông tin đơn hàng</a></li>
</ul>
<div class="tab-content">
<div id="dish" class="tab-pane">
    <table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
    <th>Tên món ăn</th>
    <th>Đơn giá</th>
    <th>Số lượng</th>
    <th>Thành tiền</th>
    </tr>
    </thead>
    <tbody>
    {assign var="soluong" value=0}
    {assign var="giatien" value=0}
    {foreach from=$details item=d}
        <tr>
        <td style="vertical-align:middle;">
            {php}
                if (is_file("{$path_upload}meal/thumb_{$d['dpic']}")) echo "<span><img src='/{$path_upload}meal/thumb_{$d['dpic']}' /></span>";
                elseif (is_file("{$path_upload}meal/{$d['dpic']}")) echo "<span><img src='/{$path_upload}meal/{$d['dpic']}' /></span>";
                else echo '';
            {/php}
            <span>{$d['dname']}</span>
        </td>
        <td style="vertical-align:middle;">{number_format($d['gia'])}</td>
        <td style="vertical-align:middle;">{$d['soluong']}</td>
        <td style="vertical-align:middle;">{number_format($d['tong'])}</td>
        </tr>
        {assign var="soluong" value={$soluong} + $d['soluong']}
        {assign var="giatien" value={$giatien} + $d['tong']}
    {/foreach}    
    </tbody>
    </table>
</div>
{if $giatien < $shipping_value}
    {assign var="shipping" value=$shipping_fee}
{else}
    {assign var="shipping" value=0}
{/if}
<div id="info" class="tab-pane in active">
    <table class="table table-striped table-bordered table-hover">
        <tr>
        <td>Ngày đăng ký</td>
        <td>{date("d/m/Y H:i:s", strtotime($detail['modate']))}</td>
        </tr>
        <tr>
        <td>Người nhận hàng</td>
        <td>{$detail['moname']}</td>
        </tr>
        <tr>
        <td>Địa chỉ nhận hàng <i class="fa fa-map-marker light-orange bigger-110"></i></td>
        <td>{$detail['moaddress']}</td>
        </tr>
        <tr>
        <td>Điện thoại người nhận <i class="fa fa-phone light-green bigger-110"></i></td>
        <td>{$detail['motel']}</td>
        </tr>
        <tr>
        <td>Số lượng</td>
        <td>{$soluong}</td>
        </tr>
        <tr>
        <td>Số tiền <i class="fa fa-dollar green bigger-110"></i></td>
        <td>{number_format($giatien)}</td>
        </tr>
        <tr>
        <td>Shipping <i class="fa fa-dollar green bigger-110"></i></td>
        <td>{number_format($shipping)}</td>
        </tr>
        <tr>
        <td>Thành tiền <i class="fa fa-dollar green bigger-110"></i></td>
        <td>{number_format($giatien + $shipping)}</span>
        </tr>
        <tr>
    </table>
    
    
</div>
</div>
</div>