<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
get_instance()->load->iface('DefaultInterface');
class Mod_user extends MY_Model implements DefaultInterface {
    var $table = 'admin';
    var $column = array('alogin','aname','amail','atel','aid'); //set column field database for order and search
    var $order = array('aid' => 'desc'); // default order 

    //Datatable
    private function _get_datatables_query() {
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column as $item) {//loop column 
            if (@$_POST['search']['value']) {//if datatable send POST for search
                if ($i === 0 ) {//first loop
                    $this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND. 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                //last loop
                if (count($this->column) - 1 == $i) $this->db->group_end(); //close bracket
            }
            $column[$i] = $item; //set column array variable to order processing
            $i++;
        }
        if (isset($_POST['order'])) {//here order processing
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        elseif (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables() {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    function _create($values) {
        if (!$this->db->insert($this->table, array(
            'alogin' => $values['userlogin'],
            'apwd' => md5(md5($values['pwd']).$values['salt']),
            'asalt' => $values['salt'],
            'aname' => $values['fullname'],
            'amail' => $values['email'],
            'atel' => $values['phone']
        ))) return FALSE;
        return TRUE;
    }
    function _get($id) {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('aid', $id);
        $query = $this->db->get();
        $result = $query->first_row('array');
        $query->free_result();
	return ($result) ? $result : false;
    }
    function _getSalt($id) {
        $this->db->select('aid, asalt');
        $this->db->from('admin');
        $this->db->or_where('alogin', $id);
        $this->db->or_where('amail', $id);
        $this->db->or_where('atel', $id);
        $query = $this->db->get();
        return $query->row_array();
    }
    function _login($login) {
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('aid', $login['aid']);
        $this->db->where('apwd', md5(md5($login['password']).$login['salt']));
        $query = $this->db->get();
        return $query->row_array();
    }
    function _doimatkhau($values) {
        $data = array(
            'apwd' => $values['password_new']
        );
        $this->db->where('aid', $values['aid']);
        $this->db->update('admin', $data);
        return ($this->db->affected_rows()) ? TRUE : FALSE;
    }
}
/* End of file */