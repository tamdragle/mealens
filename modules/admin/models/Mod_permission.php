<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
get_instance()->load->iface('DefaultInterface');
class Mod_permission extends MY_Model implements DefaultInterface {
    function _getPermission($mod, $user) {
        $this->db->select('value');
        $this->db->from('admin_permission');
        $this->db->where('amid', $mod);
        $this->db->where('aid', $user);
        $query = $this->db->get();
        $result = $query->first_row('array');
        $query->free_result();
        return $result['value'];
    }
    function _getPermissions($mod = array(), $user = FALSE) {
        $this->db->select("amid, value");
        $this->db->from('admin_permission');
        $this->db->where('aid', $user);
        $this->db->where_in('amid', $mod);
        $query = $this->db->get();
        $result = $query->result_array();
        $query->free_result();
        return ($result) ? $result : FALSE;
    }
    function _setPermission($value = array()) {
        $this->db->trans_start();
        $this->db->where('aid', $value['aid']);
        $this->db->delete('admin_permission');
        foreach($value['value'] as $key => $val) {
            $data[] = array(
                'aid' => $value['aid'],
                'amid' => $val['idMod'],
                'value' => $val['priv']
            );
        }
        $this->db->insert_batch('admin_permission', $data);
        $this->db->trans_complete();
    }
}
/* End of file */