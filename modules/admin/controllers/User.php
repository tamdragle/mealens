<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {
    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('ssAdminId')) redirect(site_url().'admin');
        $this->permarr = $this->permission->getPermissions($this->permissions[_MOD_ADMIN]);
        $this->load->model('mod_user');
    }
    function index() {
        if (!$this->permarr['read']) redirect(site_url().'admin/denied?w=read');
        $this->parser->parse('user/list');
    }
    function ajax_list() {
        $list = $this->mod_user->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $acc) {
            $no++;
            $row = array();
            $row[] = "<a href='".site_url()."admin/user/detail?id={$acc->aid}'>{$acc->alogin}</a>";
            $row[] = $acc->aname;
            $row[] = $acc->amail;
            $row[] = $acc->atel;

            $data[] = $row;
        }

        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mod_user->count_all(),
            "recordsFiltered" => $this->mod_user->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    function ajax_add() {
        if (!$this->permarr['write']) {
            echo json_encode(array("status" => 'denied'));
            exit;
        }
        $items = $this->input->post();
        $items['salt'] = getRandom(15);
        $items['pwd'] = '111';
        $this->mod_user->_create($items);
        echo json_encode(array("status" => TRUE));
    }
    function detail() {
        if (!$this->permarr['read']) redirect(site_url().'admin/denied?w=read');
        if ($this->input->post('isSent') == 'OK') {
            if (!$this->permarr['update']) redirect(site_url().'admin/denied?w=update');
            $value = array();
            $items = $this->input->post();
            $this->load->helper('array');
            $idMod = $items['idMod'];
            for ($msm = 0; $msm < sizeof($idMod); $msm++) {
                $this->permission->permissions['read'] = (isset($items['checkbox_read']) && element($msm, $items['checkbox_read'], '') != '') ? TRUE : 0;
                $this->permission->permissions['write'] = (isset($items['checkbox_write']) && element($msm, $items['checkbox_write'], '') != '') ? TRUE : 0;
                $this->permission->permissions['delete'] = (isset($items['checkbox_delete']) && element($msm, $items['checkbox_delete'], '') != '') ? TRUE : 0;
                $this->permission->permissions['update'] = (isset($items['checkbox_update']) && element($msm, $items['checkbox_update'], '') != '') ? TRUE : 0;
                $this->permission->permissions['master'] = (isset($items['checkbox_master']) && element($msm, $items['checkbox_master'], '') != '') ? TRUE : 0;
                $bitmask = $this->permission->toBitmask();
                if ($bitmask > 32) $bitmask = 32;
                $value[$msm]['idMod'] = $idMod[$msm];
                $value[$msm]['priv'] = $bitmask;
            }
            $items['value'] = $value;
            $this->mod_permission->_setPermission($items);
            redirect(site_url().'admin/user/detail?id='.$items['aid']);
        }
        else {
            $id = $this->input->get('id');
            $account = $this->mod_user->_get($id);
            $modules = $this->permissionPerMod($id);
            $items = array();
            $stt = 0;
            foreach ($modules as $module) {
                $permarr = $this->permission->getPermissions($module['value']);
                $read = ($permarr['master'] OR $permarr['read']) ? ' checked' : '';
                $write = ($permarr['master'] OR $permarr['write']) ? ' checked' : '';
                $delete = ($permarr['master'] OR $permarr['delete']) ? ' checked' : '';
                $update = ($permarr['master'] OR $permarr['update']) ? ' checked' : '';
                $master = ($permarr['master']) ? ' checked' : '';

                $items[$module['mid']] = array(
                    'read' => $read,
                    'write' => $write,
                    'delete' => $delete,
                    'update' => $update,
                    'master' => $master,
                    'module' => $module['mname'],
                    'stt' => $stt
                );
                $stt++;
            }
            $this->parser->assign('items', $items);

            $data = array(
                'account' => $account,
                'modules' => $items,
            );
            $this->parser->assign('data', $data);
            $this->parser->parse('user/detail');
        }
    }
    function changePWD() {
        if ($this->input->post('isSent') == 'OK') {
            $items = $this->input->post();
            if ($items['password_new'] != $items['password_new_confirm']) redirect(site_url().'admin/warning?w=confirm');
            else {
                $values = array();
                $account = $this->mod_user->_get($this->session->userdata('ssAdminId'));
                if (md5(md5($items['password_old']).$account['asalt']) == $account['apwd']) {
                    $values['password_new'] = md5(md5($items['password_new_confirm']).$account['asalt']);
                    $values['aid'] = $account['aid'];
                    if ($this->mod_user->_doimatkhau($values)) {
                        redirect(site_url().'admin/warning');
                    }
                }
                else redirect(site_url().'admin/warning?w=wrong');
            }
        }
    }
    private function permissionPerMod($user) {
        $this->load->model('mod_module');
        $modules = $this->mod_module->_gets();
        $data = array();
        $i = 0;
        foreach ($modules as $mod) {
            $data[$i]['mid'] = $mod['amid'];
            $data[$i]['mname'] = $mod['amname'];
            $data[$i]['value'] = $this->mod_permission->_getPermission($mod['amid'], $user);
            $i++;
        }
        return $data;
    }
}
