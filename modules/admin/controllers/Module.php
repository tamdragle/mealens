<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Module extends MY_Controller {
    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('ssAdminId')) redirect(site_url().'admin');
        $this->permarr = $this->permission->getPermissions($this->permissions[_MOD_ADMIN]);
        $this->load->model('mod_module');
    }
    function index() {
        if (!$this->permarr['read']) redirect(site_url().'admin/denied?w=read');
        $this->parser->parse('module/list');
    }
    function ajax_list() {
        $list = $this->mod_module->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $mod) {
            $no++;
            $row = array();
            $row[] = $mod->amname;
            $row[] = $mod->amdefine;
            $row[] = $mod->amid;
            $row[] = '';

            $data[] = $row;
        }

        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mod_module->count_all(),
            "recordsFiltered" => $this->mod_module->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    function ajax_add() {
        if (!$this->permarr['write']) {
            echo json_encode(array("status" => 'denied'));
            exit;
        }
        $items = $this->input->post();
        $this->mod_module->_create($items);
        echo json_encode(array("status" => TRUE));
    }
}
