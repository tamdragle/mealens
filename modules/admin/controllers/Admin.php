<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {
    function __construct() {
        parent::__construct();
        if ($this->session->userdata('ssAdminId')) redirect(site_url().'admin/module');
        $this->load->model('mod_user');
    }
    function index() {
        redirect(site_url().'admin/login');
    }
    function login() {
        if ($this->input->post('isSent') == 'OK') {
            $params = $this->input->post();
            $salt = $this->mod_user->_getSalt($params['username']);
            if ($salt) {
                $user = $this->mod_user->_login(array(
                    'aid' => $salt['aid'],
                    'password' => $params['password'],
                    'salt' => $salt['asalt']
                ));
                if ($user) {
                    $this->session->set_userdata(array(
                        'ssAdminId' => $user['aid'],
                        'ssAdminLogin' => $user['alogin'],
                        'ssAdminFullname' => $user['aname']
                    ));
                    redirect(site_url().'admin/module');
                }
            }
            redirect(site_url().'admin');
        }
        $this->parser->parse('admin/login');
    }
    function logout() {
        $this->session->unset_userdata('ssAdminId');
        $this->session->unset_userdata('ssAdminLogin');
        $this->session->unset_userdata('ssAdminFullname');
        $this->session->unset_userdata('ssAdminLevel');
        redirect(site_url().'admin');
    }
    function denied() {
        switch ($this->input->get('w')) {
            case 'read':
                $pri = 'đọc';
                break;
            case 'write':
                $pri = 'ghi';
                break;
            case 'delete':
                $pri = 'xóa';
                break;
            case 'update':
                $pri = 'cập nhật';
                break;
            default :
                $pri = '';
                break;
        }
        $this->parser->assign('pri', $pri);
        $this->parser->parse('admin/denied');
    }
    function warning() {
        switch ($this->input->get('w')) {
            case 'confirm':
                $code = 500;
                $warning = 'Xác nhận mật khẩu không đúng';
                break;
            case 'wrong':
                $this->session->unset_userdata('ssAdminId');
                $this->session->unset_userdata('ssAdminLogin');
                $this->session->unset_userdata('ssAdminFullname');
                $this->session->unset_userdata('ssAdminLevel');
                $code = 500;
                $warning = 'Mật khẩu cũ không đúng';
                break;
            default :
                $this->session->unset_userdata('ssAdminId');
                $this->session->unset_userdata('ssAdminLogin');
                $this->session->unset_userdata('ssAdminFullname');
                $this->session->unset_userdata('ssAdminLevel');
                $code = 600;
                $warning = 'Đổi mật khẩu thành công';
                break;
        }
        $this->parser->assign('code', $code);
        $this->parser->assign('warning', $warning);
        $this->parser->parse('admin/warning');
    }
}
