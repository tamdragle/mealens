{* Extend our master template *}
{extends file="master.tpl"}
{block name=script}
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar_quantri").addClass("active open");
    $("#sidebar_quantri_user").addClass("active");

    $("#setpriv").click(function() {
        $("#frmAdmin").submit();
    });
});
</script>
{/block}
{block name=body}
<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li><a href="#">Other Pages</a></li>
<li class="active">Blank Page</li>
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<h3 class="header smaller lighter blue">Chi tiet account</h3>
<div class="clearfix">
<div class="pull-left tableTools-container"></div>
</div>
    <div style="margin-bottom:5px;">
        <button class="btn btn-xs btn-success" id="setpriv"><i class="ace-icon fa fa-check"></i> Cấp quyền</button>
    </div>
    <div>
    <form id='frmAdmin' name='frmAdmin' method='post' action=''>
    <input type='hidden' id='isSent' name='isSent' value='OK'>
    <input type='hidden' id='aid' name='aid' value='{$data.account.aid}'>
    <table id="table" class="table table-bordered table-striped">
    <thead>
    <tr>
    <th>Tên module</th>
    <th>Đọc</th>
    <th>Ghi</th>
    <th>Xóa</th>
    <th>Cập nhật</th>
    </tr>
    </thead>
    <tbody>
    {foreach from=$data.modules key=myId item=i}
        <tr>
        <td>{$i.module}</td>
        <td><input type='checkbox' id='checkbox_read[{$i.stt}]' name='checkbox_read[{$i.stt}]' value='{$myId}'{$i.read} /></td>
        <td><input type='checkbox' id='checkbox_write[{$i.stt}]' name='checkbox_write[{$i.stt}]' value='{$myId}'{$i.write} /></td>
        <td><input type='checkbox' id='checkbox_delete[{$i.stt}]' name='checkbox_delete[{$i.stt}]' value='{$myId}'{$i.delete} /></td>
        <td><input type='checkbox' id='checkbox_update[{$i.stt}]' name='checkbox_update[{$i.stt}]' value='{$myId}'{$i.update} /></td>
        </tr>
        <input type='hidden' id='idMod[{$i.stt}]' name='idMod[{$i.stt}]' value='{$myId}'>
    {/foreach}
    </tbody>
    </table>
    </form>
    </div>
<!-- PAGE CONTENT ENDS -->
</div>
</div>
</div>
</div>
</div>
{/block}