# <center>**Mealean API Specifications**</center>
##### <center>Version 0.4.8 - Feb 24 2017 </center>
___
### <center>**History**</center>
#### 27/08/2017 - TamTT
- Khởi tạo
___


## **API List**
1. [Register](#register)
2. [Log In](#log-in)
3. [Change Password](#change-password)
4. [Friend](#friend)
5. [Transaction](#transaction)
6. [Wallet](#wallet)
7. [Dish List](#dish-list)
___
## **Internal Functions**

___
## **API Specifications**
### Register
- Đăng ký tài khoản mới
- Method: POST
- URL: `/user/register`
- Parameters:
    + `isSent (string)*` : OK
    + `name (string)*` : Tên đăng nhập
    + `mail (string)*` : Email
    + `pwd (string)*` : Mật khẩu
    + `address (string)*` : Địa chỉ
    + `ftoken (string)*` : Firebase token
    + `ref (string)*` : Email hoặc số điện thoại người giới thiệu
    + `os (int)*` : Hệ điều hành, 1:android, 2:ios
- Response Data:
    + `token (string)` : Token dùng để thực hiện các thao tác khác
    + `uid (String)` : User id
    + `uidparent (string)` : User parent id
    + `uref (string)` : Uref
    + `ulevel (string)` : Level của ngưởi dùng
    + `uname (string)` : Tên họ vd: Trần, Nguyễn, Lê
    + `uavatar (string)` : Link avatar
    + `umail (string)` : Email
    + `utel (string)` : Điện thoại
    + `uwallet (string)` : Tiền trong ví
    + `ureg (string)` : Ngày đăng kí

### Log In
- Đăng nhập vào hệ thống
- Method: POST
- URL: `/user/log-in`
- Parameters:
    + `isSent (string)*` : OK
    + `username (string)*` : Tên đăng nhập
    + `password (string)*` : Mật khẩu
    + `ftoken (string)*` : Firebase token
    + `os (int)*` : Hệ điều hành, 1:android, 2:ios
- Response Data:
    + `token (string)` : Token dùng để thực hiện các thao tác khác
    + `uid (String)` : User id
    + `uidparent (string)` : User parent id
    + `uref (string)` : Uref
    + `ulevel (string)` : Level của ngưởi dùng
    + `uname (string)` : Tên họ vd: Trần, Nguyễn, Lê
    + `uavatar (string)` : Link avatar
    + `umail (string)` : Email
    + `utel (string)` : Điện thoại
	  + `uwallet (string)` : Tiền trong ví
    + `ureg (string)` : Ngày đăng kí

### Change Password
- Đổi password
- Method: POST
- URL: `/profile/change_password`
- Parameters:
    + `isSent (string)*` : OK
    + `oldpwd (string)*` : Password cũ
    + `newpwd (string)*` : Password mới
    + `newpwd_confirm (string)*` : Confirm password
    + `type (string)*` : Kiểu password muốn đổi, 'login' and other
- Response Data: NULL

### Friend
- Lấy danh sách các bạn bè
- Method: POST
- URL: `/user/friend`
- Parameters:
    + `isSent (string)*` : OK
- Response Data:
    + `uid (String)` : user id
    + `uname (string)` : Tên họ vd: Trần, Nguyễn, Lê
    + `avatar (string)` : link avatar
    + `utel (string)` : điện thoại
    + `ureg (string)` : ngày đăng kí

### Transaction
- Lấy danh sách lịch sử giao dịch
- Method: POST
- URL: `/user/transaction`
- Parameters:
    + `isSent (string)*` : OK
- Response Data:
    + `tranid (String)` : Id của giao dịch
    + `tranvalue (string)` : Giá trị giao dịch
    + `tranbalance (string)` : Số tiền hiện tại
    + `trantype (string)` : Loại giao dịch
    + `transUser (string)` : Người giao dịch
    + `trandate (string)` : Ngày giao dịch
    + `trannote (string)` : Ghi chú

### Wallet
- Lấy thông tin ví tiền
- Method: POST
- URL: `/user/wallet`
- Parameters:
    + `isSent (string)*` : OK
- Response Data:
    + `uwallet (String)` : Số tiền trong ví

### Dish List
- Lấy danh sách các category món ăn
- Method: POST
- URL: `/dish/list`
- Parameters:
    + `isSent (string)*` : OK
- Response Data:
    + `id (String)` : Id của category
    + `name (string)` : Tên của category
