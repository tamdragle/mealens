<?php /* Smarty version Smarty-3.1.13, compiled from "/Users/dinhhoaibao/Sites/mealens/views/general/contact.tpl" */ ?>
<?php /*%%SmartyHeaderCode:150555396859509cd9d8c4c4-43008781%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '10fe5dcf7c57bcdd0027367a22f70773f71fcd48' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/general/contact.tpl',
      1 => 1499691993,
      2 => 'file',
    ),
    'af0fda9ea677ca3f1c9dc6db7c00ab6b823810f7' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/layouts/home/master.tpl',
      1 => 1499090107,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '150555396859509cd9d8c4c4-43008781',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_59509cd9e1fbc4_28220654',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59509cd9e1fbc4_28220654')) {function content_59509cd9e1fbc4_28220654($_smarty_tpl) {?><!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=-100%, user-scalable=yes" />
<meta name="format-detection" content="telephone=no">
<title> MEAL - Enjoying and Sharing</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="stylesheet" href="/assets/css/styles.css">
<link rel="stylesheet" href="/assets/css/responsive.css">
<link rel="stylesheet" href="/assets/css/slick.css">

</head>
<body id="index" class="under">
<div id="wrapper">
    <?php echo $_smarty_tpl->getSubTemplate ('home/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    
<div id="mainvisual">
<h2>
    <div class="gMap gMapDisableScrollwheel gMapMinifyInfoWindow gMapZoom15">
        <div class="gMapCenter">
            <p class="gMapLatLng">10.791128829984036,106.68861055396724</p>
        </div>
        <div class="gMapMarker">
            <div class="gMapInfoWindow"><span class="bold">MealENS</span></div>
            <p class="gMapLatLng">10.7902857,106.68873929999995</p>
        </div>
    </div>    	
</h2>
</div>
<div id="main">
    <div class="container">
        <div class="section">
            <ul class="lh-contact">
                <li>
                    <dl>
                        <dt><img src="/assets/images/ico_map.jpg" alt=""></dt>
                        <dd>21/4 Đường số 2, Thạnh Mỹ Lợi, Q2, TP.HCM</dd>
                    </dl>
                </li>
                <li>
                    <dl>
                        <dt><img src="/assets/images/ico_call.jpg" alt=""></dt>
                        <dd>0932627357 - 0903334221</dd>
                    </dl>
                </li>
                <li>
                    <dl>
                        <dt><img src="/assets/images/ico_mail.jpg" alt=""></dt>
                        <dd><a href="mailto:contact@yourdomain.com">contact@mealens.com</a></dd>
                    </dl>
                </li>
            </ul>
            <p class="lh-ttl">“Sức khỏe của khách hàng là lợi ích của chúng tôi”</p>
        </div>
    </div>
</div>

    <?php echo $_smarty_tpl->getSubTemplate ('home/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</div>
<script src="/assets/js/jquery.js" type="text/javascript"></script>
<script src="/assets/js/common.js"></script>
<script src="/assets/js/slick.js"></script>
<script type="text/javascript">
function login() {
    $.ajax({
        url : "<?php echo site_url();?>
login",
        type: "POST",
        data: {
            'isSent': 'OK',
            'username': $('#username').val(),
            'password': $('#password').val()
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
            if (data.error_code == '200') location.reload();
            /*
            if (data.error_code == '200') {
                var res =   '<ul class="login account">'+
                            '<li><a href="<?php echo site_url();?>
profile">Tài khoản</a></li>'+
                            '<li><a href="<?php echo site_url();?>
lunch/cart">Giỏ hàng</a></li>'+
                            '<li><a href="<?php echo site_url();?>
logout">Thoát</a></li>'+
                            '</ul>';
                $('#profile').html(res);
            }
            */
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the login');
        }
    });
}
</script>

<script type="text/javascript" src="/assets/js/gmaps.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=&callback=gmaps.renderAll"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('body').addClass("lienhe");
    $("#menu_contact").addClass("active");
});
</script>

</body>
</html><?php }} ?>