<?php /* Smarty version Smarty-3.1.13, compiled from "/Users/dinhhoaibao/Sites/mealens/views/lunch/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1250687555594c046af062b1-03033860%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b65950ff5d420b83a13f11ee03ab7fafc86b4bba' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/lunch/index.tpl',
      1 => 1499723838,
      2 => 'file',
    ),
    'af0fda9ea677ca3f1c9dc6db7c00ab6b823810f7' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/layouts/home/master.tpl',
      1 => 1499090107,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1250687555594c046af062b1-03033860',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_594c046b1bddb1_17576361',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_594c046b1bddb1_17576361')) {function content_594c046b1bddb1_17576361($_smarty_tpl) {?><!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=-100%, user-scalable=yes" />
<meta name="format-detection" content="telephone=no">
<title> MEAL - Enjoying and Sharing</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="stylesheet" href="/assets/css/styles.css">
<link rel="stylesheet" href="/assets/css/responsive.css">
<link rel="stylesheet" href="/assets/css/slick.css">

</head>
<body id="index" class="under">
<div id="wrapper">
    <?php echo $_smarty_tpl->getSubTemplate ('home/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    
<div id="mainvisual">
    <ul>
        <li><img src="/assets/images/main_img01.jpg" alt=""></li>
        <li><img src="/assets/images/main_img01.jpg" alt=""></li>
        <li><img src="/assets/images/main_img01.jpg" alt=""></li>
    </ul>
    <div class="container">
        <p>CƠM NGON<br>MỖI NGÀY</p>
        <p></p>
    </div>
</div>
<div id="main">
    <div class="container">
        <p class="menu">THỰC ĐƠN THEO TUẦN</p>
        <ul class="menu-day">
            <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['week']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value){
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
                <li><a href="<?php echo site_url();?>
lunch?z=<?php echo date("z",$_smarty_tpl->tpl_vars['v']->value);?>
" <?php if (date("z",$_smarty_tpl->tpl_vars['v']->value)==$_smarty_tpl->tpl_vars['z']->value){?>class="active"<?php }?>><?php echo $_smarty_tpl->tpl_vars['k']->value;?>
</a></li>
            <?php } ?>
        </ul>
        <p class="center"><img src="/assets/images/ico_choice.jpg" alt=""></p>
        <ul class="menu-choice">
            <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['types']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value){
$_smarty_tpl->tpl_vars['t']->_loop = true;
?>
                <li><a href="<?php echo site_url();?>
lunch?z=<?php echo $_smarty_tpl->tpl_vars['z']->value;?>
&type=<?php echo $_smarty_tpl->tpl_vars['t']->value['dtid'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['dtid']==$_smarty_tpl->tpl_vars['type']->value){?>class="active"<?php }?>><?php echo $_smarty_tpl->tpl_vars['t']->value['dtname'];?>
</a></li>
            <?php } ?>
        </ul>
        <?php if ($_smarty_tpl->tpl_vars['menu']->value){?>
        <ul class="menu-list">
            <?php if (isset($_smarty_tpl->tpl_vars["c"])) {$_smarty_tpl->tpl_vars["c"] = clone $_smarty_tpl->tpl_vars["c"];
$_smarty_tpl->tpl_vars["c"]->value = 0; $_smarty_tpl->tpl_vars["c"]->nocache = null; $_smarty_tpl->tpl_vars["c"]->scope = 0;
} else $_smarty_tpl->tpl_vars["c"] = new Smarty_variable(0, null, 0);?>
            <?php  $_smarty_tpl->tpl_vars['m'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['m']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['menu']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['m']->key => $_smarty_tpl->tpl_vars['m']->value){
$_smarty_tpl->tpl_vars['m']->_loop = true;
?>
                <?php if ((($_smarty_tpl->tpl_vars['c']->value+1)-floor(($_smarty_tpl->tpl_vars['c']->value+1)/2))%2==0){?>
                    <li class="menu-list-content">
                        <a href="javascript:void(-1);" onclick="addcart(<?php echo $_smarty_tpl->tpl_vars['m']->value['id'];?>
)">
                            <p><?php echo $_smarty_tpl->tpl_vars['m']->value['dname'];?>
</p>
                            <p><?php echo $_smarty_tpl->tpl_vars['m']->value['dfeature'];?>
</p>
                            <p><?php echo number_format($_smarty_tpl->tpl_vars['m']->value['mprice']);?>
</p>
                            <span>CHỌN</span>
                        </a>
                    </li>
                    <li class="menu-list-img">
                        <a href="<?php echo site_url();?>
lunch/detail?id=<?php echo $_smarty_tpl->tpl_vars['m']->value['did'];?>
">
                        <?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                            if (is_file("<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/thumb_<?php echo $_smarty_tpl->tpl_vars['m']->value['dpic'];?>
")) echo "<img src='/<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/thumb_<?php echo $_smarty_tpl->tpl_vars['m']->value['dpic'];?>
' />";
                            elseif (is_file("<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/<?php echo $_smarty_tpl->tpl_vars['m']->value['dpic'];?>
")) echo "<img src='/<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/<?php echo $_smarty_tpl->tpl_vars['m']->value['dpic'];?>
' />";
                            else echo '';
                        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                        </a>
                    </li>
                <?php }else{ ?>
                    <li class="menu-list-img">
                        <a href="<?php echo site_url();?>
lunch/detail?id=<?php echo $_smarty_tpl->tpl_vars['m']->value['did'];?>
">
                        <?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                            if (is_file("<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/thumb_<?php echo $_smarty_tpl->tpl_vars['m']->value['dpic'];?>
")) echo "<img src='/<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/thumb_<?php echo $_smarty_tpl->tpl_vars['m']->value['dpic'];?>
' />";
                            elseif (is_file("<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/<?php echo $_smarty_tpl->tpl_vars['m']->value['dpic'];?>
")) echo "<img src='/<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/<?php echo $_smarty_tpl->tpl_vars['m']->value['dpic'];?>
' />";
                            else echo '';
                        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                        </a>
                    </li>
                    <li class="menu-list-content">
                        <a href="javascript:void(-1);" onclick="addcart(<?php echo $_smarty_tpl->tpl_vars['m']->value['id'];?>
)">
                            <p><?php echo $_smarty_tpl->tpl_vars['m']->value['dname'];?>
</p>
                            <p><?php echo $_smarty_tpl->tpl_vars['m']->value['dfeature'];?>
</p>
                            <p><?php echo number_format($_smarty_tpl->tpl_vars['m']->value['mprice']);?>
</p>
                            <span>CHỌN</span>
                        </a>
                    </li>
                <?php }?>
                <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['c']->value;?>
<?php $_tmp1=ob_get_clean();?><?php if (isset($_smarty_tpl->tpl_vars["c"])) {$_smarty_tpl->tpl_vars["c"] = clone $_smarty_tpl->tpl_vars["c"];
$_smarty_tpl->tpl_vars["c"]->value = $_tmp1+1; $_smarty_tpl->tpl_vars["c"]->nocache = null; $_smarty_tpl->tpl_vars["c"]->scope = 0;
} else $_smarty_tpl->tpl_vars["c"] = new Smarty_variable($_tmp1+1, null, 0);?>
            <?php } ?>
        </ul>
        <?php }else{ ?>
        <p class="menu">Chưa có thực đơn</p>
        <?php }?>
    </div>
    <div class="box01">
        <div class="container">
            <p class="image-r"><img src="/assets/images/idx_img01.jpg" alt=""></p>
            <p>Chúng tôi cung cấp</p>
            <p>Cơm văn phòng - giao tận nơi</p>
            <p>Đặt cơm online hoặc qua mobi app</p>
            <p class="mb10">Giao cơm bằng <span class="bold">bộ hộp cơm nhựa cao cấp</span> hoặc khay nhựa PP (không cần trả khay) tuyệt đối an toàn cho thực phẩm nóng.</p>
            <p class="mb10"><span class="bold">Giao hàng miễn phí</span> các quận: 1, 3, một phần các quận 2, 4, 5, 10, Tân Bình, Phú Nhuận, Bình Thạnh (liên hệ chúng tôi để biết chi tiết)</p>
            <p class="mb30">Vui lòng đặt cơm <span class="bold">trước 9h sáng mỗi ngày</span> để được phục vụ tốt nhất.</p>
            <span><a href="">Xem thêm để biết chi tiết</a></span>
        </div>
    </div>
    <div class="box02">
        <div class="container">
            <p>Tại sao bạn nên chọn cơm tại đây?</p>
            <p>VÌ CƠM NGON - GIÁ LẠI TỐT</p>
            <ul>
                <li>
                    <p><img src="/assets/images/idx_img03.png" alt=""></p>
                    <p>Giá hợp lý, chỉ từ 35.000 VNĐ<br>lại còn có nhiều khuyến mãi</p>
                </li>
                <li>
                    <p><img src="/assets/images/idx_img04.png" alt=""></p>
                    <p>Thực đơn phong phú,<br>đa dạng, luôn thay đổi</p>
                </li>
                <li>
                    <p><img src="/assets/images/idx_img05.png" alt=""></p>
                    <p>Nguồn gốc rõ ràng, nguyên liệu<br>tươi ngon và dinh dưỡng</p>
                </li>
            </ul>
            <span><a href="">Cung cấp suất ăn cho công ty, trường học - hỏi tại đây</a></span>
        </div>
    </div>
</div>

    <?php echo $_smarty_tpl->getSubTemplate ('home/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</div>
<script src="/assets/js/jquery.js" type="text/javascript"></script>
<script src="/assets/js/common.js"></script>
<script src="/assets/js/slick.js"></script>
<script type="text/javascript">
function login() {
    $.ajax({
        url : "<?php echo site_url();?>
login",
        type: "POST",
        data: {
            'isSent': 'OK',
            'username': $('#username').val(),
            'password': $('#password').val()
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
            if (data.error_code == '200') location.reload();
            /*
            if (data.error_code == '200') {
                var res =   '<ul class="login account">'+
                            '<li><a href="<?php echo site_url();?>
profile">Tài khoản</a></li>'+
                            '<li><a href="<?php echo site_url();?>
lunch/cart">Giỏ hàng</a></li>'+
                            '<li><a href="<?php echo site_url();?>
logout">Thoát</a></li>'+
                            '</ul>';
                $('#profile').html(res);
            }
            */
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the login');
        }
    });
}
</script>

<script type="text/javascript">
$(document).ready(function() {
    $("#menu_lunch").addClass("active");
});
function loadCart() {
    $('#cart').html("<center><i class='ace-icon fa fa-spinner fa-spin orange bigger-125'></i></center>");
    $.get('<?php echo site_url();?>
lunch/viewCart', {},
    function(response) {
        if (response.error_code == '201') alert(response.message);
        else $('#cart').html(response);
    });
}
function addcart(menuID) {
    <?php if ($_smarty_tpl->tpl_vars['login']->value!='YES'){?>
        alert('Vui lòng đăng nhập');
    <?php }elseif($_smarty_tpl->tpl_vars['z']->value<date('z')){?>
        alert('Đã quá ngày, Bạn không thể thêm vào giỏ hàng');
    <?php }elseif(($_smarty_tpl->tpl_vars['z']->value==date('z'))&&date('H')>10){?>
        alert('Đã quá giờ, Bạn không thể thêm vào giỏ hàng');
    <?php }else{ ?>
        $.ajax({
            url : "<?php echo site_url();?>
lunch/addCart",
            type: "POST",
            data: {
                'menuID': menuID
            },
            dataType: "JSON",
            success: function(data) {
                alert(data.message);
                //loadCart();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Khong the cap nhat');
            }
        });
    <?php }?>
}</script>

</body>
</html><?php }} ?>