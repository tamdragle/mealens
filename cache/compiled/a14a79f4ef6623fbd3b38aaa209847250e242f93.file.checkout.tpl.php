<?php /* Smarty version Smarty-3.1.13, compiled from "/Users/dinhhoaibao/Sites/mealens/views/lunch/checkout.tpl" */ ?>
<?php /*%%SmartyHeaderCode:558804854594c1b22d290b7-46613214%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a14a79f4ef6623fbd3b38aaa209847250e242f93' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/lunch/checkout.tpl',
      1 => 1499225103,
      2 => 'file',
    ),
    'af0fda9ea677ca3f1c9dc6db7c00ab6b823810f7' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/layouts/home/master.tpl',
      1 => 1499090107,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '558804854594c1b22d290b7-46613214',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_594c1b22eaad51_70379695',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_594c1b22eaad51_70379695')) {function content_594c1b22eaad51_70379695($_smarty_tpl) {?><!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=-100%, user-scalable=yes" />
<meta name="format-detection" content="telephone=no">
<title> MEAL - Enjoying and Sharing</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="stylesheet" href="/assets/css/styles.css">
<link rel="stylesheet" href="/assets/css/responsive.css">
<link rel="stylesheet" href="/assets/css/slick.css">

</head>
<body id="index" class="under">
<div id="wrapper">
    <?php echo $_smarty_tpl->getSubTemplate ('home/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    
<div id="mainvisual">
<h2>&nbsp;</h2>
</div>
<div id="main">
<div class="container">
<?php if ($_smarty_tpl->tpl_vars['message']->value){?>
    <ul class="topic-path">
    <li><a href="<?php echo site_url();?>
lunch">Cơm trưa</a></li>
    <li><a href="<?php echo site_url();?>
lunch">Giỏ hàng</a></li>
    <li><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</li>
    </ul>
<?php }else{ ?>
    <ul class="topic-path">
    <li><a href="<?php echo site_url();?>
lunch">Cơm trưa</a></li>
    <li><a href="<?php echo site_url();?>
lunch">Giỏ hàng</a></li>
    <li>Hoàn tất đơn hàng của ngày <?php echo date("d-m-Y",strtotime($_smarty_tpl->tpl_vars['menu']->value['mdate']));?>
</li>
    </ul>
    <div class="payment clearfix">
    <dl class="payment-danhmuc">
    <dt>DANH MỤC ĐẶT HÀNG</dt>
    <?php if (isset($_smarty_tpl->tpl_vars["total"])) {$_smarty_tpl->tpl_vars["total"] = clone $_smarty_tpl->tpl_vars["total"];
$_smarty_tpl->tpl_vars["total"]->value = 0; $_smarty_tpl->tpl_vars["total"]->nocache = null; $_smarty_tpl->tpl_vars["total"]->scope = 0;
} else $_smarty_tpl->tpl_vars["total"] = new Smarty_variable(0, null, 0);?>
    <?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cart']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value){
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
        <dd>
        <ul>
        <li>
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                if (is_file("<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/thumb_<?php echo $_smarty_tpl->tpl_vars['c']->value['pic'];?>
")) echo "<span><img src='/<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/thumb_<?php echo $_smarty_tpl->tpl_vars['c']->value['pic'];?>
' /></span>";
                elseif (is_file("<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/<?php echo $_smarty_tpl->tpl_vars['c']->value['pic'];?>
")) echo "<span><img src='/<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/<?php echo $_smarty_tpl->tpl_vars['c']->value['pic'];?>
' /></span>";
                else echo '';
            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        </li>
        <li><?php echo $_smarty_tpl->tpl_vars['c']->value['name'];?>
</li>
        <li>x<?php echo $_smarty_tpl->tpl_vars['c']->value['qty'];?>
</li>
        </ul>
        </dd>
        <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['total']->value;?>
<?php $_tmp1=ob_get_clean();?><?php if (isset($_smarty_tpl->tpl_vars["total"])) {$_smarty_tpl->tpl_vars["total"] = clone $_smarty_tpl->tpl_vars["total"];
$_smarty_tpl->tpl_vars["total"]->value = $_tmp1+$_smarty_tpl->tpl_vars['c']->value['subtotal']; $_smarty_tpl->tpl_vars["total"]->nocache = null; $_smarty_tpl->tpl_vars["total"]->scope = 0;
} else $_smarty_tpl->tpl_vars["total"] = new Smarty_variable($_tmp1+$_smarty_tpl->tpl_vars['c']->value['subtotal'], null, 0);?>
    <?php } ?>
    </dl>
    <div class="payment-cnt clearfix">
    <p>THÔNG TIN ĐẶT HÀNG</p>
    <div class="payment-cnt-l">
    <p class="mb10">TÊN KHÁCH HÀNG</p>
    <p><input type="text" id="tenkhachhang" name="tenkhachhang" placeholder="Tên của bạn" value="<?php echo $_smarty_tpl->tpl_vars['ssUser']->value['ssFullname'];?>
"></p>
    <p class="mb10">ĐỊA CHỈ GIAO HÀNG*</p>
    <p><input type="text" id="diachi" name="diachi" placeholder="Địa chỉ" value="<?php echo $_smarty_tpl->tpl_vars['ssUser']->value['ssAddress'];?>
"></p>
    <p class="mb10">SỐ ĐIỆN THOẠI *</p>
    <p><input type="text" id="dienthoai" name="dienthoai" placeholder="Điện thoại" value="<?php echo $_smarty_tpl->tpl_vars['ssUser']->value['ssTel'];?>
"></p>
    <p class="mb10">GHI CHÚ THÊM</p>
    <p><textarea id="ghichu" name="ghichu" placeholder="Gọi điện khi đem hàng tới"></textarea></p>
    </div>
    <div class="payment-cnt-r">
    <p class="mb10">HÌNH THỨC THANH TOÁN *</p>
    <p><select id="thanhtoan" name="thanhtoan">
            <option value="1">Thanh toán khi nhận hàng</option>
            <option value="2">Thanh toán bằng mealens</option>
    </select></p>
    <?php if ($_smarty_tpl->tpl_vars['total']->value<$_smarty_tpl->tpl_vars['shipping_value']->value){?>
        <?php if (isset($_smarty_tpl->tpl_vars["shipping"])) {$_smarty_tpl->tpl_vars["shipping"] = clone $_smarty_tpl->tpl_vars["shipping"];
$_smarty_tpl->tpl_vars["shipping"]->value = $_smarty_tpl->tpl_vars['shipping_fee']->value; $_smarty_tpl->tpl_vars["shipping"]->nocache = null; $_smarty_tpl->tpl_vars["shipping"]->scope = 0;
} else $_smarty_tpl->tpl_vars["shipping"] = new Smarty_variable($_smarty_tpl->tpl_vars['shipping_fee']->value, null, 0);?>
    <?php }else{ ?>
        <?php if (isset($_smarty_tpl->tpl_vars["shipping"])) {$_smarty_tpl->tpl_vars["shipping"] = clone $_smarty_tpl->tpl_vars["shipping"];
$_smarty_tpl->tpl_vars["shipping"]->value = 0; $_smarty_tpl->tpl_vars["shipping"]->nocache = null; $_smarty_tpl->tpl_vars["shipping"]->scope = 0;
} else $_smarty_tpl->tpl_vars["shipping"] = new Smarty_variable(0, null, 0);?>
    <?php }?>
    <ul class="payment-total">
    <li>GIÁ TIỀN:</li>
    <li><?php echo number_format($_smarty_tpl->tpl_vars['total']->value);?>
VNĐ</li>
    <li>PHÍ SHIPING:</li>
    <li><?php echo number_format($_smarty_tpl->tpl_vars['shipping']->value);?>
VNĐ</li>
    <li>GIẢM GIÁ:</li>
    <li>0VNĐ</li>
    <li>TỔNG CỘNG:</li>
    <li><?php echo number_format($_smarty_tpl->tpl_vars['total']->value+$_smarty_tpl->tpl_vars['shipping']->value);?>
</li>
    </ul>
    </div>
    </div>
    </div>
    <p class="center"><a href="javascript:void(-1);" onclick="orderCart(<?php echo $_smarty_tpl->tpl_vars['menu']->value['mid'];?>
)" class="btn-link">HOÀN TẤT THÔNG TIN</a></p>
<?php }?>
</div>
</div>

    <?php echo $_smarty_tpl->getSubTemplate ('home/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</div>
<script src="/assets/js/jquery.js" type="text/javascript"></script>
<script src="/assets/js/common.js"></script>
<script src="/assets/js/slick.js"></script>
<script type="text/javascript">
function login() {
    $.ajax({
        url : "<?php echo site_url();?>
login",
        type: "POST",
        data: {
            'isSent': 'OK',
            'username': $('#username').val(),
            'password': $('#password').val()
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
            if (data.error_code == '200') location.reload();
            /*
            if (data.error_code == '200') {
                var res =   '<ul class="login account">'+
                            '<li><a href="<?php echo site_url();?>
profile">Tài khoản</a></li>'+
                            '<li><a href="<?php echo site_url();?>
lunch/cart">Giỏ hàng</a></li>'+
                            '<li><a href="<?php echo site_url();?>
logout">Thoát</a></li>'+
                            '</ul>';
                $('#profile').html(res);
            }
            */
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the login');
        }
    });
}
</script>

<script type="text/javascript">
$(document).ready(function() {
    $('body').addClass("checkout-payment");
    $("#menu_lunch").addClass("active");
});
function orderCart(menuID) {
    $.ajax({
        url : "<?php echo site_url();?>
lunch/orderCart",
        type: "POST",
        data: {
            'menuID': menuID,
            'tenkhachhang': $('#tenkhachhang').val(),
            'diachi': $('#diachi').val(),
            'dienthoai': $('#dienthoai').val(),
            'ghichu': $('#ghichu').val(),
            'thanhtoan': $('#thanhtoan').val()
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
            if (data.error_code == '200') window.location.href = "<?php echo site_url();?>
lunch/orders";
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Không thể hoàn tất');
        }
    });
}
</script>

</body>
</html><?php }} ?>