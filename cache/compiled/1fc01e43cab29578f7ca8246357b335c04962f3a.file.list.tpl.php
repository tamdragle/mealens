<?php /* Smarty version Smarty-3.1.13, compiled from "/Users/dinhhoaibao/Sites/mealens/modules/meal/views/orders/list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2019190759594c912abb24f2-98739338%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1fc01e43cab29578f7ca8246357b335c04962f3a' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/modules/meal/views/orders/list.tpl',
      1 => 1499169397,
      2 => 'file',
    ),
    'e562e6fefacff9e7ea47ea7394447121d702aa45' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/layouts/master.tpl',
      1 => 1499090107,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2019190759594c912abb24f2-98739338',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_594c912ada96c8_60638035',
  'variables' => 
  array (
    'assets_path' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_594c912ada96c8_60638035')) {function content_594c912ada96c8_60638035($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include '/Users/dinhhoaibao/Sites/ci3x/libraries/Smarty/plugins/function.html_options.php';
?><!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<title>MEAL - Enjoying and Sharing</title>
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<!-- bootstrap & fontawesome -->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/bootstrap.min.css" />
<link rel="stylesheet" href="/assets/fonts/font-awesome.css" />
<!-- text fonts -->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/fonts.googleapis.com.css" />
<!-- ace styles -->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<!--[if lte IE 9]>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-part2.min.css" class="ace-main-stylesheet" />
<![endif]-->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-skins.min.css" />
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-rtl.min.css" />
<!--[if lte IE 9]>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-ie.min.css" />
<![endif]-->
<!-- inline styles related to this page -->

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

<!-- ace settings handler -->
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/ace-extra.min.js"></script>
<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
<!--[if lte IE 8]>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/html5shiv.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/respond.min.js"></script>
<![endif]-->
</head>
<body class="no-skin">
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_changepwd" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <form action="<?php echo site_url();?>
admin/user/changePWD" id="formChangePWD" method="post">
            <input type="hidden" value="OK" id="isSent" name="isSent" />
            <div class="modal-body form">
                <div class="form-group">
                    <label>Mật khẩu cũ:</label>
                    <input class="form-control" type="password" id="password_old" name="password_old" />
                </div>
                <div class="form-group">
                    <label>Mật khẩu mới:</label>
                    <input class="form-control" type="password" id="password_new" name="password_new" />
                </div>
                <div class="form-group">
                    <label>Xác nhận mật khẩu mới:</label>
                    <input class="form-control" type="password" id="password_new_confirm" name="password_new_confirm" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-xs btn-primary">Submit</button>
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="main-container ace-save-state" id="main-container">
<?php echo $_smarty_tpl->getSubTemplate ('sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li><a href="#">Cơm văn phòng</a></li>
<li><a href="<?php echo site_url();?>
meal/orders?month=<?php echo $_smarty_tpl->tpl_vars['month']->value;?>
&year=<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
">Đơn hàng</a></li>
<li class="active">Danh sách đơn hàng</li>
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="space-2"></div>
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<h3 class="header smaller lighter blue">Danh sách đơn hàng</h3>
<table id="table" class="table table-bordered table-striped">
<thead>
<tr>
<th colspan="8" class="center blue"><?php echo $_smarty_tpl->tpl_vars['date']->value;?>
</th>
</tr>        
<tr>
<th>Ngày đặt</th>
<th>Người đặt</th>
<th>Số lượng</th>
<th>Số tiền</th>
<th>Người nhận</th>
<th>Địa chỉ nhận</th>
<th>ĐT người nhận</th>
<th>Trạng thái</th>
</tr>
</thead>
<tbody>
<?php if ($_smarty_tpl->tpl_vars['menu']->value){?>
    <?php if (isset($_smarty_tpl->tpl_vars["soluong"])) {$_smarty_tpl->tpl_vars["soluong"] = clone $_smarty_tpl->tpl_vars["soluong"];
$_smarty_tpl->tpl_vars["soluong"]->value = 0; $_smarty_tpl->tpl_vars["soluong"]->nocache = null; $_smarty_tpl->tpl_vars["soluong"]->scope = 0;
} else $_smarty_tpl->tpl_vars["soluong"] = new Smarty_variable(0, null, 0);?>
    <?php if (isset($_smarty_tpl->tpl_vars["tong"])) {$_smarty_tpl->tpl_vars["tong"] = clone $_smarty_tpl->tpl_vars["tong"];
$_smarty_tpl->tpl_vars["tong"]->value = 0; $_smarty_tpl->tpl_vars["tong"]->nocache = null; $_smarty_tpl->tpl_vars["tong"]->scope = 0;
} else $_smarty_tpl->tpl_vars["tong"] = new Smarty_variable(0, null, 0);?>
    <?php  $_smarty_tpl->tpl_vars['m'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['m']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['menu']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['m']->key => $_smarty_tpl->tpl_vars['m']->value){
$_smarty_tpl->tpl_vars['m']->_loop = true;
?>
    <tr>
    <td><a href='javascript:void(-1);' onclick='viewDetail(<?php echo $_smarty_tpl->tpl_vars['m']->value['moid'];?>
)'><?php echo date("d-m-Y H:i:s",strtotime($_smarty_tpl->tpl_vars['m']->value['modate']));?>
</a></td>
    <td><a href="<?php echo site_url();?>
users/detail?id=<?php echo $_smarty_tpl->tpl_vars['m']->value['uid'];?>
"><?php echo $_smarty_tpl->tpl_vars['m']->value['uname'];?>
</a></td>
    <td><?php echo $_smarty_tpl->tpl_vars['m']->value['soluong'];?>
</td>
    <td><?php echo number_format($_smarty_tpl->tpl_vars['m']->value['tong']);?>
</td>
    <td><?php echo $_smarty_tpl->tpl_vars['m']->value['moname'];?>
</td>
    <td><?php echo $_smarty_tpl->tpl_vars['m']->value['moaddress'];?>
</td>
    <td><?php echo $_smarty_tpl->tpl_vars['m']->value['motel'];?>
</td>
    <td><select id="status[]" name="status[]" aria-invalid="false" <?php if (in_array($_smarty_tpl->tpl_vars['m']->value['mostatus'],array(3,4,5))){?>disabled="true"<?php }?> onchange="chgStatus(<?php echo $_smarty_tpl->tpl_vars['m']->value['moid'];?>
, this)">
    <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['m']->value['mostatus'];?>
<?php $_tmp1=ob_get_clean();?><?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['status']->value,'selected'=>$_tmp1),$_smarty_tpl);?>

    </select></td>
    </tr>
    <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['soluong']->value;?>
<?php $_tmp2=ob_get_clean();?><?php if (isset($_smarty_tpl->tpl_vars["soluong"])) {$_smarty_tpl->tpl_vars["soluong"] = clone $_smarty_tpl->tpl_vars["soluong"];
$_smarty_tpl->tpl_vars["soluong"]->value = $_tmp2+$_smarty_tpl->tpl_vars['m']->value['soluong']; $_smarty_tpl->tpl_vars["soluong"]->nocache = null; $_smarty_tpl->tpl_vars["soluong"]->scope = 0;
} else $_smarty_tpl->tpl_vars["soluong"] = new Smarty_variable($_tmp2+$_smarty_tpl->tpl_vars['m']->value['soluong'], null, 0);?>
    <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['tong']->value;?>
<?php $_tmp3=ob_get_clean();?><?php if (isset($_smarty_tpl->tpl_vars["tong"])) {$_smarty_tpl->tpl_vars["tong"] = clone $_smarty_tpl->tpl_vars["tong"];
$_smarty_tpl->tpl_vars["tong"]->value = $_tmp3+$_smarty_tpl->tpl_vars['m']->value['tong']; $_smarty_tpl->tpl_vars["tong"]->nocache = null; $_smarty_tpl->tpl_vars["tong"]->scope = 0;
} else $_smarty_tpl->tpl_vars["tong"] = new Smarty_variable($_tmp3+$_smarty_tpl->tpl_vars['m']->value['tong'], null, 0);?>
    <?php } ?>
<?php }else{ ?>
    <tr>
        <td colspan="8" class="center red">Chưa có dữ liệu</td>  
    </tr>
<?php }?>
</tbody>
<thead>
<tr>
<th></th>
<th></th>
<th class="red"><?php echo number_format($_smarty_tpl->tpl_vars['soluong']->value);?>
</th>
<th class="red"><?php echo number_format($_smarty_tpl->tpl_vars['tong']->value);?>
</th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
</table>
<!-- PAGE CONTENT ENDS -->
</div>
</div>
</div>
</div>
</div>
<div id="dialogbox"></div>

<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</div>

<!-- basic scripts -->
<!--[if !IE]> -->
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery-2.1.4.min.js"></script>
<!-- <![endif]-->
<!--[if IE]>
    <script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/bootstrap.min.js"></script>
<!-- ace scripts -->
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/ace-elements.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/ace.min.js"></script>
<!-- inline scripts related to this page -->
<script type="text/javascript">
function changePWD() {
    $('#formChangePWD')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_changepwd').modal('show'); // show bootstrap modal
    $('.modal-title').text('Đổi mật khẩu'); // Set Title to Bootstrap modal title
}
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar_meal").addClass("active open");
    $("#sidebar_meal_orders").addClass("active");
});
function chgStatus(id, sta) {
    $.ajax({
        url : "<?php echo site_url();?>
meal/orders/status",
        type: "POST",
        data: {
            'id': id,
            'val': $(sta).val()
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the cap nhat');
        }
    });
}
function viewDetail(id) {
    $.get('<?php echo site_url();?>
meal/orders/detail?id='+ id, {},
    function(response) {
        if (response.error_code == '201') alert(response.message);
        else $('#dialogbox').html(response);
    });
    $('#dialogbox').dialog({
        autoOpen:true,
        modal:true,
        title: "Đơn hàng của ngày <?php echo $_smarty_tpl->tpl_vars['date']->value;?>
",
        width: "60%",
        maxHeight: 600,
        position: { my: 'top', at: 'top+10' },
        open: function( event, ui ) {
            //alert('hello');
        }
    });    
}
</script>

</body>
</html><?php }} ?>