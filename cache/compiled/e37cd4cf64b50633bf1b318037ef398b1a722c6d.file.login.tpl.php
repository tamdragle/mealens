<?php /* Smarty version Smarty-3.1.13, compiled from "/Users/dinhhoaibao/Sites/mealens/modules/admin/views/admin/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:326445312594c045525c124-42135869%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e37cd4cf64b50633bf1b318037ef398b1a722c6d' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/modules/admin/views/admin/login.tpl',
      1 => 1499449184,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '326445312594c045525c124-42135869',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_594c04552ce9e4_17701449',
  'variables' => 
  array (
    'assets_path' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_594c04552ce9e4_17701449')) {function content_594c04552ce9e4_17701449($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<title>Login</title>
<meta name="description" content="User login page" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<!-- bootstrap & fontawesome -->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/bootstrap.min.css" />
<link rel="stylesheet" href="/assets/fonts/font-awesome.css" />
<!-- text fonts -->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/fonts.googleapis.com.css" />
<!-- ace styles -->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace.min.css" />
<!--[if lte IE 9]>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-part2.min.css" />
<![endif]-->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-rtl.min.css" />
<!--[if lte IE 9]>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-ie.min.css" />
<![endif]-->
<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
<!--[if lte IE 8]>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/html5shiv.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/respond.min.js"></script>
<![endif]-->
</head>
<body class="login-layout">
<div class="main-container">
<div class="main-content">
<div class="row">
<div class="col-sm-10 col-sm-offset-1">
<div class="login-container">
<div class="center">
<h1>
<i class="ace-icon fa fa-leaf green"></i>
<span class="red">Meal</span>
<span class="white" id="id-text2">ENS</span>
</h1>
</div>
<div class="space-6"></div>
<div class="position-relative">
<div id="login-box" class="login-box visible widget-box no-border">
<div class="widget-body">
<div class="widget-main">
<h4 class="header blue lighter bigger"><i class="ace-icon fa fa-coffee green"></i>Please Enter Your Information</h4>
<div class="space-6"></div>
<form action="" method="post">
<input type="hidden" value="OK" id=""isSent name="isSent" />
<fieldset>
<label class="block clearfix">
<span class="block input-icon input-icon-right">
<input type="text" id="username" name="username" class="form-control" placeholder="Username, Phone, Email">
<i class="ace-icon fa fa-user"></i>
</span>
</label>
<label class="block clearfix">
<span class="block input-icon input-icon-right">
<input type="password" id="password" name="password" class="form-control" placeholder="Password">
<i class="ace-icon fa fa-lock"></i>
</span>
</label>
<div class="space"></div>
<div class="clearfix">
<label class="inline">
<input type="checkbox" class="ace" />
<span class="lbl"> Remember Me</span>
</label>
<button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
<i class="ace-icon fa fa-key"></i>
<span class="bigger-110">Login</span>
</button>
</div>
<div class="space-4"></div>
</fieldset>
</form>
<div class="space-6"></div>
</div><!-- /.widget-main -->
<div class="toolbar clearfix">
<div><a href="#" data-target="#forgot-box" class="forgot-password-link"><i class="ace-icon fa fa-arrow-left"></i> I forgot my password</a></div>
</div>
</div><!-- /.widget-body -->
</div><!-- /.login-box -->
<div id="forgot-box" class="forgot-box widget-box no-border">
<div class="widget-body">
<div class="widget-main">
<h4 class="header red lighter bigger"><i class="ace-icon fa fa-key"></i> Retrieve Password</h4>
<div class="space-6"></div>
<p>Enter your email and to receive instructions</p>
<form>
<fieldset>
<label class="block clearfix">
<span class="block input-icon input-icon-right">
<input type="email" class="form-control" placeholder="Email" />
<i class="ace-icon fa fa-envelope"></i>
</span>
</label>
<div class="clearfix">
<button type="button" class="width-35 pull-right btn btn-sm btn-danger">
<i class="ace-icon fa fa-lightbulb-o"></i>
<span class="bigger-110">Send Me!</span>
</button>
</div>
</fieldset>
</form>
</div><!-- /.widget-main -->
<div class="toolbar center">
<a href="#" data-target="#login-box" class="back-to-login-link">
Back to login
<i class="ace-icon fa fa-arrow-right"></i>
</a>
</div>
</div><!-- /.widget-body -->
</div><!-- /.forgot-box -->
</div><!-- /.position-relative -->
</div>
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.main-content -->
</div><!-- /.main-container -->
<!-- basic scripts -->
<!--[if !IE]> -->
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery-2.1.4.min.js"></script>
<!-- <![endif]-->
<!--[if IE]>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
if('ontouchstart' in document.documentElement) document.write("<script src='<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
jQuery(function($) {
    $(document).on('click', '.toolbar a[data-target]', function(e) {
        e.preventDefault();
        var target = $(this).data('target');
        $('.widget-box.visible').removeClass('visible');//hide others
        $(target).addClass('visible');//show target
    });
});
</script>
</body>
</html>
<?php }} ?>