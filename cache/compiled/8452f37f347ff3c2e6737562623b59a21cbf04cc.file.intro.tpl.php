<?php /* Smarty version Smarty-3.1.13, compiled from "/Users/dinhhoaibao/Sites/mealens/views/general/intro.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1223115101594c19668f4656-87711811%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8452f37f347ff3c2e6737562623b59a21cbf04cc' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/general/intro.tpl',
      1 => 1499090107,
      2 => 'file',
    ),
    'af0fda9ea677ca3f1c9dc6db7c00ab6b823810f7' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/layouts/home/master.tpl',
      1 => 1499090107,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1223115101594c19668f4656-87711811',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_594c196698a162_56747201',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_594c196698a162_56747201')) {function content_594c196698a162_56747201($_smarty_tpl) {?><!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=-100%, user-scalable=yes" />
<meta name="format-detection" content="telephone=no">
<title> MEAL - Enjoying and Sharing</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="stylesheet" href="/assets/css/styles.css">
<link rel="stylesheet" href="/assets/css/responsive.css">
<link rel="stylesheet" href="/assets/css/slick.css">

</head>
<body id="index" class="under">
<div id="wrapper">
    <?php echo $_smarty_tpl->getSubTemplate ('home/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    
<div id="mainvisual">
<h2>&nbsp;</h2>
</div>
<div id="main">
    <div class="container">
        <div class="gioithieu-box01">
            <p><img src="/assets/images/gioithieu_img02.jpg" alt=""></p>
            <h3>GIỚI THIỆU</h3>
            <div>
                <p>Bạn là nhân viên văn phòng ? Bạn bận rộn không kịp chuẩn bị bữa trưa cho mình? Mỗi ngày bạn luôn đánh vật với câu hỏi “Trưa nay ăn gì?” Bạn ngại phải ra ngoài ăn với thời tiết lúc nắng lúc mưa của Sài Gòn? Bạn e dè khi phải dùng thực phẩm bẩn và tin tức về chúng cứ tràn lan trên mạng?...Có rất nhiều những trăn trở xoay quanh bữa trưa văn phòng hiện nay.</p>
                <p>Chúng tôi đã từng, đồng cảm, thấu hiểu và mong muốn mang đến một giải pháp hoàn hảo cho bạn. Sau quá trình cân nhắc và tìm hiểu, chúng tôi quyết định mang đến những dịch vụ tối ưu nhất với phương châm AN TOÀN – TIỆN LỢI – CHUYÊN NGHIỆP.</p>
                <p>Với dịch vụ CƠM TRƯA VĂN PHÒNG, chúng tôi mang bữa trưa gia đình đến tận công ty bạn với mức giá hợp lý nhất và nhiều ưu đãi. Được chế biến bằng nguồn nguyên liệu sạch cùng đội ngũ đầu bếp tận tâm, nhiều kinh nghiệm, đảm bảo an toàn và cam kết vệ sinh tuyệt đối, bạn hoàn toàn yên tâm khi dùng bữa.</p>
                <p>Đặc biệt hơn, với tiêu chí ăn chung càng vui, với mỗi người bạn giới thiệu bạn sẽ được tích điểm bằng 10% giá trị đơn hàng phát sinh. Số điểm tích lũy sẽ được sử dụng thanh toán đơn hàng cho chính bạn, càng đông – càng vui, bạn đã sẵn sàng thưởng thức và chia sẻ???</p>
            </div>
        </div>
    </div>
</div>

    <?php echo $_smarty_tpl->getSubTemplate ('home/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</div>
<script src="/assets/js/jquery.js" type="text/javascript"></script>
<script src="/assets/js/common.js"></script>
<script src="/assets/js/slick.js"></script>
<script type="text/javascript">
function login() {
    $.ajax({
        url : "<?php echo site_url();?>
login",
        type: "POST",
        data: {
            'isSent': 'OK',
            'username': $('#username').val(),
            'password': $('#password').val()
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
            if (data.error_code == '200') location.reload();
            /*
            if (data.error_code == '200') {
                var res =   '<ul class="login account">'+
                            '<li><a href="<?php echo site_url();?>
profile">Tài khoản</a></li>'+
                            '<li><a href="<?php echo site_url();?>
lunch/cart">Giỏ hàng</a></li>'+
                            '<li><a href="<?php echo site_url();?>
logout">Thoát</a></li>'+
                            '</ul>';
                $('#profile').html(res);
            }
            */
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the login');
        }
    });
}
</script>

<script type="text/javascript">
$(document).ready(function() {
    $('body').addClass("gioithieu");
    $("#menu_intro").addClass("active");
});
</script>

</body>
</html><?php }} ?>