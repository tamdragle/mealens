<?php /* Smarty version Smarty-3.1.13, compiled from "/Users/dinhhoaibao/Sites/mealens/views/lunch/orders.tpl" */ ?>
<?php /*%%SmartyHeaderCode:831142763594bda6da8bfa7-50120690%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b143fa70cc7f3bde4543e4a7c7bbc1c2fb6551fe' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/lunch/orders.tpl',
      1 => 1499090107,
      2 => 'file',
    ),
    'af0fda9ea677ca3f1c9dc6db7c00ab6b823810f7' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/layouts/home/master.tpl',
      1 => 1499090107,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '831142763594bda6da8bfa7-50120690',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_594bda6db1c5b9_28765685',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_594bda6db1c5b9_28765685')) {function content_594bda6db1c5b9_28765685($_smarty_tpl) {?><!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=-100%, user-scalable=yes" />
<meta name="format-detection" content="telephone=no">
<title> MEAL - Enjoying and Sharing</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="stylesheet" href="/assets/css/styles.css">
<link rel="stylesheet" href="/assets/css/responsive.css">
<link rel="stylesheet" href="/assets/css/slick.css">

<link rel="stylesheet" href="/assets/datatables/jquery.dataTables.css" />
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

</head>
<body id="index" class="under">
<div id="wrapper">
    <?php echo $_smarty_tpl->getSubTemplate ('home/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    
<div id="dialogbox"></div>
<div id="mainvisual">
<h2>&nbsp;</h2>
</div>
<div id="main">
<div class="container">
    <ul class="topic-path">
        <li><a class="ajax" href="<?php echo site_url();?>
lunch">Cơm trưa</a></li>
        <li>Danh sách đơn hàng</li>
    </ul>
    <div class="payment clearfix">
        <p class="note-table sp">※ Di chuyển trái/phải để xem thêm</p>
        <table id="table" class="table table-bordered table-striped">
        <thead>
        <tr>
        <th>Ngày đặt</th>
        <th>Số lượng</th>
        <th>Giá tiền</th>
        <th>Menu ngày</th>
        <th>Trạng thái</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
        </table>
    </div>
</div>
</div>

    <?php echo $_smarty_tpl->getSubTemplate ('home/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</div>
<script src="/assets/js/jquery.js" type="text/javascript"></script>
<script src="/assets/js/common.js"></script>
<script src="/assets/js/slick.js"></script>
<script type="text/javascript">
function login() {
    $.ajax({
        url : "<?php echo site_url();?>
login",
        type: "POST",
        data: {
            'isSent': 'OK',
            'username': $('#username').val(),
            'password': $('#password').val()
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
            if (data.error_code == '200') location.reload();
            /*
            if (data.error_code == '200') {
                var res =   '<ul class="login account">'+
                            '<li><a href="<?php echo site_url();?>
profile">Tài khoản</a></li>'+
                            '<li><a href="<?php echo site_url();?>
lunch/cart">Giỏ hàng</a></li>'+
                            '<li><a href="<?php echo site_url();?>
logout">Thoát</a></li>'+
                            '</ul>';
                $('#profile').html(res);
            }
            */
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the login');
        }
    });
}
</script>

<script src="/assets/datatables/jquery.dataTables.min.js"></script>
<script src="/assets/datatables/dataTables.bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#menu_lunch").addClass("active");
    load_list();
});
/*
$("#dialogbox").dialog({
    autoOpen:false,
    modal:true,
    title: "Chi tiết đơn hàng",
    width: "60%",
    open: function( event, ui ) {
        //alert('hello');
    }
});
*/
function viewDetail(id, donhang) {
    $.get('<?php echo site_url();?>
profile/lunch_detail?id='+ id, {},
    function(response) {
        if (response.error_code == '201') alert(response.message);
        else $('#dialogbox').html(response);
    });
    $('#dialogbox').dialog({
        autoOpen:true,
        modal:true,
        title: "Đơn hàng của ngày "+ donhang,
        width: "60%",
        maxHeight: 600,
        position: { my: 'top', at: 'top+10' },
        open: function( event, ui ) {
            //alert('hello');
        }
    });    
}
function load_list() {
    var url = '<?php echo site_url();?>
profile/lunch_list';
    if (url != '') {
        table = $('#table').DataTable({ 
            "processing": true,
            "serverSide": true,
            "order": [],

            "ajax": {
                "url": url,
                "type": "POST"
            },
            "columnDefs": [
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            ],
        });
    }
}
</script>

</body>
</html><?php }} ?>