<?php /* Smarty version Smarty-3.1.13, compiled from "/Users/dinhhoaibao/Sites/mealens/modules/meal/views/orders/detail.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12899640605950bdfc2f98b7-48276332%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '449df166f35dd0ec6cc2d87bed0b2777f65f81bd' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/modules/meal/views/orders/detail.tpl',
      1 => 1499405381,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12899640605950bdfc2f98b7-48276332',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5950bdfc41b596_24901351',
  'variables' => 
  array (
    'details' => 0,
    'path_upload' => 0,
    'd' => 0,
    'soluong' => 0,
    'giatien' => 0,
    'shipping_value' => 0,
    'shipping_fee' => 0,
    'detail' => 0,
    'shipping' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5950bdfc41b596_24901351')) {function content_5950bdfc41b596_24901351($_smarty_tpl) {?><div class="tabbable">
<ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
<li><a data-toggle="tab" href="#dish">Thông tin món ăn</a></li>
<li class="active"><a data-toggle="tab" href="#info">Thông tin đơn hàng</a></li>
</ul>
<div class="tab-content">
<div id="dish" class="tab-pane">
    <table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
    <th>Tên món ăn</th>
    <th>Đơn giá</th>
    <th>Số lượng</th>
    <th>Thành tiền</th>
    </tr>
    </thead>
    <tbody>
    <?php if (isset($_smarty_tpl->tpl_vars["soluong"])) {$_smarty_tpl->tpl_vars["soluong"] = clone $_smarty_tpl->tpl_vars["soluong"];
$_smarty_tpl->tpl_vars["soluong"]->value = 0; $_smarty_tpl->tpl_vars["soluong"]->nocache = null; $_smarty_tpl->tpl_vars["soluong"]->scope = 0;
} else $_smarty_tpl->tpl_vars["soluong"] = new Smarty_variable(0, null, 0);?>
    <?php if (isset($_smarty_tpl->tpl_vars["giatien"])) {$_smarty_tpl->tpl_vars["giatien"] = clone $_smarty_tpl->tpl_vars["giatien"];
$_smarty_tpl->tpl_vars["giatien"]->value = 0; $_smarty_tpl->tpl_vars["giatien"]->nocache = null; $_smarty_tpl->tpl_vars["giatien"]->scope = 0;
} else $_smarty_tpl->tpl_vars["giatien"] = new Smarty_variable(0, null, 0);?>
    <?php  $_smarty_tpl->tpl_vars['d'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['d']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['details']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['d']->key => $_smarty_tpl->tpl_vars['d']->value){
$_smarty_tpl->tpl_vars['d']->_loop = true;
?>
        <tr>
        <td style="vertical-align:middle;">
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                if (is_file("<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/thumb_<?php echo $_smarty_tpl->tpl_vars['d']->value['dpic'];?>
")) echo "<span><img src='/<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/thumb_<?php echo $_smarty_tpl->tpl_vars['d']->value['dpic'];?>
' /></span>";
                elseif (is_file("<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/<?php echo $_smarty_tpl->tpl_vars['d']->value['dpic'];?>
")) echo "<span><img src='/<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/<?php echo $_smarty_tpl->tpl_vars['d']->value['dpic'];?>
' /></span>";
                else echo '';
            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            <span><?php echo $_smarty_tpl->tpl_vars['d']->value['dname'];?>
</span>
        </td>
        <td style="vertical-align:middle;"><?php echo number_format($_smarty_tpl->tpl_vars['d']->value['gia']);?>
</td>
        <td style="vertical-align:middle;"><?php echo $_smarty_tpl->tpl_vars['d']->value['soluong'];?>
</td>
        <td style="vertical-align:middle;"><?php echo number_format($_smarty_tpl->tpl_vars['d']->value['tong']);?>
</td>
        </tr>
        <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['soluong']->value;?>
<?php $_tmp1=ob_get_clean();?><?php if (isset($_smarty_tpl->tpl_vars["soluong"])) {$_smarty_tpl->tpl_vars["soluong"] = clone $_smarty_tpl->tpl_vars["soluong"];
$_smarty_tpl->tpl_vars["soluong"]->value = $_tmp1+$_smarty_tpl->tpl_vars['d']->value['soluong']; $_smarty_tpl->tpl_vars["soluong"]->nocache = null; $_smarty_tpl->tpl_vars["soluong"]->scope = 0;
} else $_smarty_tpl->tpl_vars["soluong"] = new Smarty_variable($_tmp1+$_smarty_tpl->tpl_vars['d']->value['soluong'], null, 0);?>
        <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['giatien']->value;?>
<?php $_tmp2=ob_get_clean();?><?php if (isset($_smarty_tpl->tpl_vars["giatien"])) {$_smarty_tpl->tpl_vars["giatien"] = clone $_smarty_tpl->tpl_vars["giatien"];
$_smarty_tpl->tpl_vars["giatien"]->value = $_tmp2+$_smarty_tpl->tpl_vars['d']->value['tong']; $_smarty_tpl->tpl_vars["giatien"]->nocache = null; $_smarty_tpl->tpl_vars["giatien"]->scope = 0;
} else $_smarty_tpl->tpl_vars["giatien"] = new Smarty_variable($_tmp2+$_smarty_tpl->tpl_vars['d']->value['tong'], null, 0);?>
    <?php } ?>    
    </tbody>
    </table>
</div>
<?php if ($_smarty_tpl->tpl_vars['giatien']->value<$_smarty_tpl->tpl_vars['shipping_value']->value){?>
    <?php if (isset($_smarty_tpl->tpl_vars["shipping"])) {$_smarty_tpl->tpl_vars["shipping"] = clone $_smarty_tpl->tpl_vars["shipping"];
$_smarty_tpl->tpl_vars["shipping"]->value = $_smarty_tpl->tpl_vars['shipping_fee']->value; $_smarty_tpl->tpl_vars["shipping"]->nocache = null; $_smarty_tpl->tpl_vars["shipping"]->scope = 0;
} else $_smarty_tpl->tpl_vars["shipping"] = new Smarty_variable($_smarty_tpl->tpl_vars['shipping_fee']->value, null, 0);?>
<?php }else{ ?>
    <?php if (isset($_smarty_tpl->tpl_vars["shipping"])) {$_smarty_tpl->tpl_vars["shipping"] = clone $_smarty_tpl->tpl_vars["shipping"];
$_smarty_tpl->tpl_vars["shipping"]->value = 0; $_smarty_tpl->tpl_vars["shipping"]->nocache = null; $_smarty_tpl->tpl_vars["shipping"]->scope = 0;
} else $_smarty_tpl->tpl_vars["shipping"] = new Smarty_variable(0, null, 0);?>
<?php }?>
<div id="info" class="tab-pane in active">
    <table class="table table-striped table-bordered table-hover">
        <tr>
        <td>Ngày đăng ký</td>
        <td><?php echo date("d/m/Y H:i:s",strtotime($_smarty_tpl->tpl_vars['detail']->value['modate']));?>
</td>
        </tr>
        <tr>
        <td>Người nhận hàng</td>
        <td><?php echo $_smarty_tpl->tpl_vars['detail']->value['moname'];?>
</td>
        </tr>
        <tr>
        <td>Địa chỉ nhận hàng <i class="fa fa-map-marker light-orange bigger-110"></i></td>
        <td><?php echo $_smarty_tpl->tpl_vars['detail']->value['moaddress'];?>
</td>
        </tr>
        <tr>
        <td>Điện thoại người nhận <i class="fa fa-phone light-green bigger-110"></i></td>
        <td><?php echo $_smarty_tpl->tpl_vars['detail']->value['motel'];?>
</td>
        </tr>
        <tr>
        <td>Số lượng</td>
        <td><?php echo $_smarty_tpl->tpl_vars['soluong']->value;?>
</td>
        </tr>
        <tr>
        <td>Số tiền <i class="fa fa-dollar green bigger-110"></i></td>
        <td><?php echo number_format($_smarty_tpl->tpl_vars['giatien']->value);?>
</td>
        </tr>
        <tr>
        <td>Shipping <i class="fa fa-dollar green bigger-110"></i></td>
        <td><?php echo number_format($_smarty_tpl->tpl_vars['shipping']->value);?>
</td>
        </tr>
        <tr>
        <td>Thành tiền <i class="fa fa-dollar green bigger-110"></i></td>
        <td><?php echo number_format($_smarty_tpl->tpl_vars['giatien']->value+$_smarty_tpl->tpl_vars['shipping']->value);?>
</span>
        </tr>
        <tr>
    </table>
    
    
</div>
</div>
</div><?php }} ?>