<?php /* Smarty version Smarty-3.1.13, compiled from "/Users/dinhhoaibao/Sites/mealens/views/general/register.tpl" */ ?>
<?php /*%%SmartyHeaderCode:198997642259509cb33682d9-91808833%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6b8ededbc809814e75a6930d1cf509b9ce186b6f' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/general/register.tpl',
      1 => 1499090107,
      2 => 'file',
    ),
    'af0fda9ea677ca3f1c9dc6db7c00ab6b823810f7' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/layouts/home/master.tpl',
      1 => 1499090107,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '198997642259509cb33682d9-91808833',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_59509cb341ae14_28023034',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59509cb341ae14_28023034')) {function content_59509cb341ae14_28023034($_smarty_tpl) {?><!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=-100%, user-scalable=yes" />
<meta name="format-detection" content="telephone=no">
<title> MEAL - Enjoying and Sharing</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="stylesheet" href="/assets/css/styles.css">
<link rel="stylesheet" href="/assets/css/responsive.css">
<link rel="stylesheet" href="/assets/css/slick.css">

</head>
<body id="index" class="under">
<div id="wrapper">
    <?php echo $_smarty_tpl->getSubTemplate ('home/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    
<div id="mainvisual">
<h2>&nbsp;</h2>
</div>
<div id="main">
    <div class="container">
        <ul class="topic-path">
        <li>VUI LÒNG ĐĂNG KÝ THÔNG TIN KHI MUA HÀNG HOẶC ĐẶT CƠM</li>
        </ul>
        <div class="section">
            <ul class="register-cnt clearfix">
                <li>
                    <p>Họ và tên *</p>
                    <p><input type="text" id="name" name="name"></p>
                </li>
                <li>
                    <p>Email *</p>
                    <p><input type="text" id="mail" name="mail"></p>
                </li>
                <li>
                    <p>Số điện thoại *</p>
                    <p><input type="text" id="tel" name="tel"></p>
                </li>
                <li>
                    <p>Địa chỉ giao nhận *</p>
                    <p><input type="text" id="address" name="address"></p>
                </li>
                <li>
                    <ul class="clearfix">
                    <li>
                        <p>Mã người giới thiệu</p>
                        <p><input type="text" id="ref" name="ref"></p>
                    </li>
                    </ul>
                    <p>* Mật khẩu mặc định là "<span class="bold">mealens</span>", vui lòng thay đổi sau khi đăng nhập.</p>
                </li>
            </ul>
            <p class="center"><a href="javascript:void(-1);" onclick="register()" class="btn-link">ĐĂNG KÝ</a></p>
        </div>
    </div>
</div>

    <?php echo $_smarty_tpl->getSubTemplate ('home/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</div>
<script src="/assets/js/jquery.js" type="text/javascript"></script>
<script src="/assets/js/common.js"></script>
<script src="/assets/js/slick.js"></script>
<script type="text/javascript">
function login() {
    $.ajax({
        url : "<?php echo site_url();?>
login",
        type: "POST",
        data: {
            'isSent': 'OK',
            'username': $('#username').val(),
            'password': $('#password').val()
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
            if (data.error_code == '200') location.reload();
            /*
            if (data.error_code == '200') {
                var res =   '<ul class="login account">'+
                            '<li><a href="<?php echo site_url();?>
profile">Tài khoản</a></li>'+
                            '<li><a href="<?php echo site_url();?>
lunch/cart">Giỏ hàng</a></li>'+
                            '<li><a href="<?php echo site_url();?>
logout">Thoát</a></li>'+
                            '</ul>';
                $('#profile').html(res);
            }
            */
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the login');
        }
    });
}
</script>

<script type="text/javascript">
function register() {
    $.ajax({
        url : "<?php echo site_url();?>
register",
        type: "POST",
        data: {
            'isSent': 'OK',
            'name': $('#name').val(),
            'mail': $('#mail').val(),
            'tel': $('#tel').val(),
            'address': $('#address').val(),
            'ref': $('#ref').val()
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
            if (data.error_code == '200') window.location.href = "<?php echo site_url();?>
lunch";
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Co loi xay ra');
        }
    });
}
</script>

</body>
</html><?php }} ?>