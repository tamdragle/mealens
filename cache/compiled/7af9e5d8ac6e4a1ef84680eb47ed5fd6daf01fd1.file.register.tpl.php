<?php /* Smarty version Smarty-3.1.13, compiled from "/Users/dinhhoaibao/Sites/mealens/modules/meal/views/menu/register.tpl" */ ?>
<?php /*%%SmartyHeaderCode:634351239594c8d462cc402-33222715%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7af9e5d8ac6e4a1ef84680eb47ed5fd6daf01fd1' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/modules/meal/views/menu/register.tpl',
      1 => 1499090107,
      2 => 'file',
    ),
    'e562e6fefacff9e7ea47ea7394447121d702aa45' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/layouts/master.tpl',
      1 => 1499090107,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '634351239594c8d462cc402-33222715',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_594c8d463aa550_72027819',
  'variables' => 
  array (
    'assets_path' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_594c8d463aa550_72027819')) {function content_594c8d463aa550_72027819($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include '/Users/dinhhoaibao/Sites/ci3x/libraries/Smarty/plugins/function.html_options.php';
?><!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<title>MEAL - Enjoying and Sharing</title>
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<!-- bootstrap & fontawesome -->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/bootstrap.min.css" />
<link rel="stylesheet" href="/assets/fonts/font-awesome.css" />
<!-- text fonts -->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/fonts.googleapis.com.css" />
<!-- ace styles -->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<!--[if lte IE 9]>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-part2.min.css" class="ace-main-stylesheet" />
<![endif]-->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-skins.min.css" />
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-rtl.min.css" />
<!--[if lte IE 9]>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-ie.min.css" />
<![endif]-->
<!-- inline styles related to this page -->

<!-- page specific plugin styles -->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/bootstrap-duallistbox.min.css" />
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/bootstrap-multiselect.min.css" />
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/select2.min.css" />

<!-- ace settings handler -->
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/ace-extra.min.js"></script>
<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
<!--[if lte IE 8]>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/html5shiv.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/respond.min.js"></script>
<![endif]-->
</head>
<body class="no-skin">
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_changepwd" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <form action="<?php echo site_url();?>
admin/user/changePWD" id="formChangePWD" method="post">
            <input type="hidden" value="OK" id="isSent" name="isSent" />
            <div class="modal-body form">
                <div class="form-group">
                    <label>Mật khẩu cũ:</label>
                    <input class="form-control" type="password" id="password_old" name="password_old" />
                </div>
                <div class="form-group">
                    <label>Mật khẩu mới:</label>
                    <input class="form-control" type="password" id="password_new" name="password_new" />
                </div>
                <div class="form-group">
                    <label>Xác nhận mật khẩu mới:</label>
                    <input class="form-control" type="password" id="password_new_confirm" name="password_new_confirm" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-xs btn-primary">Submit</button>
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="main-container ace-save-state" id="main-container">
<?php echo $_smarty_tpl->getSubTemplate ('sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li><a href="#">Cơm văn phòng</a></li>
<li><a href="<?php echo site_url();?>
meal/menu?month=<?php echo $_smarty_tpl->tpl_vars['month']->value;?>
&year=<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
">Lịch thực đơn</a></li>
<li class="active">Thêm thực đơn</li>
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<h3 class="header smaller lighter blue">Thêm thực đơn <span class="pink"><?php echo $_smarty_tpl->tpl_vars['date']->value;?>
</span></h3>
<div class="clearfix">
<div class="pull-left tableTools-container"></div>
</div>
<form class="form-horizontal" role="form" id="validation-form" action="" method="post">
<input type="hidden" id="isSent" name="isSent" value="OK" />
<input type="hidden" id="function" name="function" value="<?php echo $_smarty_tpl->tpl_vars['function']->value;?>
" />
<input type="hidden" id="mid" name="mid" value="<?php echo $_smarty_tpl->tpl_vars['mid']->value;?>
" />
<input type="hidden" id="z" name="z" value="<?php echo $_smarty_tpl->tpl_vars['z']->value;?>
" />
<input type="hidden" id="year" name="year" value="<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
" />
<div class="form-group">
<div class="col-sm-12">
<select multiple="multiple" size="10" name="dualdish[]" id="dualdish">
   <?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['option_dish']->value),$_smarty_tpl);?>

</select>
<div class="hr hr-16 hr-dotted"></div>
</div>
</div>
<div class="clearfix form-actions">
<div class="col-md-offset-3 col-md-9">
<button class="btn btn-xs btn-info" type="submit">
<i class="ace-icon fa fa-check bigger-110"></i>
Submit
</button>
<button class="btn btn-xs" type="reset">
<i class="ace-icon fa fa-undo bigger-110"></i>
Reset
</button>
</div>
</div>
</form>
<!-- PAGE CONTENT ENDS -->
</div>
</div>
</div>
</div>
</div>

<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</div>

<!-- basic scripts -->
<!--[if !IE]> -->
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery-2.1.4.min.js"></script>
<!-- <![endif]-->
<!--[if IE]>
    <script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/bootstrap.min.js"></script>
<!-- ace scripts -->
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/ace-elements.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/ace.min.js"></script>
<!-- inline scripts related to this page -->
<script type="text/javascript">
function changePWD() {
    $('#formChangePWD')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_changepwd').modal('show'); // show bootstrap modal
    $('.modal-title').text('Đổi mật khẩu'); // Set Title to Bootstrap modal title
}
</script>

<!-- page specific plugin scripts -->
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery.bootstrap-duallistbox.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery.raty.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/bootstrap-multiselect.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/select2.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery-typeahead.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar_meal").addClass("active open");
    $("#sidebar_meal_menu").addClass("active");
    
    $('select[name="dualdish[]"]').bootstrapDualListbox();
});
</script>

</body>
</html><?php }} ?>