<?php /* Smarty version Smarty-3.1.13, compiled from "/Users/dinhhoaibao/Sites/mealens/views/layouts/sidebar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1245707312594c045a00c144-51193777%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bf8bb1e99db8993c3ae0acc755944e182729f70c' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/layouts/sidebar.tpl',
      1 => 1499090107,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1245707312594c045a00c144-51193777',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_594c045a025483_17419055',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_594c045a025483_17419055')) {function content_594c045a025483_17419055($_smarty_tpl) {?><div id="sidebar" class="sidebar responsive ace-save-state">
<div class="sidebar-shortcuts" id="sidebar-shortcuts">
<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
<button class="btn btn-success"><i class="ace-icon fa fa-signal"></i></button>
<button class="btn btn-info"><i class="ace-icon fa fa-pencil"></i></button>
<button class="btn btn-warning"><i class="ace-icon fa fa-users"></i></button>
<button class="btn btn-danger"><i class="ace-icon fa fa-cogs"></i></button>
</div>

<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
<span class="btn btn-success"></span>
<span class="btn btn-info"></span>
<span class="btn btn-warning"></span>
<span class="btn btn-danger"></span>
</div>
</div>

<ul class="nav nav-list">
<li id="sidebar_quantri">
<a href="#" class="dropdown-toggle"><i class="menu-icon fa fa-cogs"></i><span class="menu-text">Quản trị hệ thống</span><b class="arrow fa fa-angle-down"></b></a>
<b class="arrow"></b>
    <ul class="submenu">
    <li id="sidebar_quantri_module">
    <a href="<?php echo site_url();?>
admin/module"><i class="menu-icon fa fa-caret-right"></i>Quản lý module</a>
    <b class="arrow"></b>
    </li>
    <li id="sidebar_quantri_user">
    <a href="<?php echo site_url();?>
admin/user"><i class="menu-icon fa fa-caret-right"></i>Quản lý người dùng</a>
    <b class="arrow"></b>
    </li>
    </ul>
</li>
<li id="sidebar_company"><a href="<?php echo site_url();?>
company"><i class="menu-icon fa fa-list"></i><span class="menu-text">Danh sách công ty</span></a></li>
<li id="sidebar_users"><a href="<?php echo site_url();?>
users"><i class="menu-icon fa fa-users"></i><span class="menu-text">Thành viên</span></a></li>
<li id="sidebar_meal">
<a href="#" class="dropdown-toggle"><i class="menu-icon fa fa-coffee"></i><span class="menu-text">Cơm văn phòng</span><b class="arrow fa fa-angle-down"></b></a>
<b class="arrow"></b>
    <ul class="submenu">
    <li id="sidebar_meal_dish">
    <a href="<?php echo site_url();?>
meal/dish"><i class="menu-icon fa fa-caret-right"></i>Món ăn</a>
    <b class="arrow"></b>
    </li>
    <li id="sidebar_meal_menu">
    <a href="<?php echo site_url();?>
meal/menu"><i class="menu-icon fa fa fa-caret-right"></i>Thực đơn</a>
    <b class="arrow"></b>
    </li>
    <li id="sidebar_meal_orders">
    <a href="<?php echo site_url();?>
meal/orders"><i class="menu-icon fa fa fa-caret-right"></i>Đơn hàng</a>
    <b class="arrow"></b>
    </li>
    </ul>
</li>
<li id="sidebar_image"><a href="<?php echo site_url();?>
images"><i class="menu-icon fa fa-image"></i><span class="menu-text">Quan ly hinh anh</span></a></li>
</ul><!-- /.nav-list -->

<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
</div>

</div>
<?php }} ?>