<?php /* Smarty version Smarty-3.1.13, compiled from "/Users/dinhhoaibao/Sites/mealens/views/profile/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:832854135594c19e98651f6-01924100%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '62a0cc2c80f19c61a37306b0181238a43980a1e7' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/profile/index.tpl',
      1 => 1499315930,
      2 => 'file',
    ),
    'af0fda9ea677ca3f1c9dc6db7c00ab6b823810f7' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/layouts/home/master.tpl',
      1 => 1499090107,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '832854135594c19e98651f6-01924100',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_594c19e9975585_14359155',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_594c19e9975585_14359155')) {function content_594c19e9975585_14359155($_smarty_tpl) {?><!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=-100%, user-scalable=yes" />
<meta name="format-detection" content="telephone=no">
<title> MEAL - Enjoying and Sharing</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="stylesheet" href="/assets/css/styles.css">
<link rel="stylesheet" href="/assets/css/responsive.css">
<link rel="stylesheet" href="/assets/css/slick.css">

<link rel="stylesheet" href="/assets/datatables/jquery.dataTables.css" />
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

</head>
<body id="index" class="under">
<div id="wrapper">
    <?php echo $_smarty_tpl->getSubTemplate ('home/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    
<div id="dialogbox"></div>
<div id="mainvisual">
<h2>&nbsp;</h2>
</div>
<div id="main">
<div class="container">
<ul class="topic-path">
<li><a href="./">Trang chủ</a></li>
<li><a href="">Tài khoản của tôi</a></li>
<li>Thông tin cá nhân</li>
</ul>
<div class="myacc-box">
    <ul class="myacc-ttl">
        <li>THÔNG TIN CỦA BẠN</li>
        <li><a href="javascript:void(-1);" onclick="doimatkhau();">Đổi mật khẩu</a></li>
    </ul>
    <div id="xxx">
        <div class="myacc-info clearfix">
            <ul class="myacc-navi">
                <li><a id="mlunch" href="javascript:void(-1);" onclick="load('lunch')">ĐƠN HÀNG CƠM TRƯA</a></li>
                <li><a id="mother" href="javascript:void(-1);">ĐƠN HÀNG KHÁC</a></li>
                <li><a id="mfriend" href="javascript:void(-1);" onclick="load('friend')">DANH SÁCH BẠN BÈ</a></li>
            </ul>
            <div class="myacc-cnt reverse-col">
            <dl class="acc-info">
            <dd>
            <ul>
                <li><span>Ngày tham gia:</span><span><?php echo date("d/m/Y",strtotime($_smarty_tpl->tpl_vars['user']->value->ureg));?>
</span></li>
                <li><span>Email:</span><span class="txt-green"><?php echo $_smarty_tpl->tpl_vars['user']->value->umail;?>
</span></li>
                <li><span>Điện thoại</span><span><?php echo $_smarty_tpl->tpl_vars['user']->value->utel;?>
</span></li>
                <li><span>Địa chỉ giao nhận:</span><span><?php echo $_smarty_tpl->tpl_vars['user']->value->uaddress;?>
</span></li>
                <li><span>Tiền khả dụng:</span><a href="javascript:void(-1);" onclick="load('wallet')"><?php echo number_format($_smarty_tpl->tpl_vars['user']->value->uwallet);?>
</a></li>
            </ul>
            </dd>
            </dl>
            <p class="myacc-avatar"><img src="/assets/images/myavatar.jpg" alt=""><br><a href="javascript:void(-1);"><?php echo $_smarty_tpl->tpl_vars['user']->value->uname;?>
</a></p>
            </div>
        </div>
        <div class="friend-list">
            <div class="tbl-scroll" id="loadList"></div>
        </div>
    </div>
</div>
</div>
</div>

    <?php echo $_smarty_tpl->getSubTemplate ('home/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</div>
<script src="/assets/js/jquery.js" type="text/javascript"></script>
<script src="/assets/js/common.js"></script>
<script src="/assets/js/slick.js"></script>
<script type="text/javascript">
function login() {
    $.ajax({
        url : "<?php echo site_url();?>
login",
        type: "POST",
        data: {
            'isSent': 'OK',
            'username': $('#username').val(),
            'password': $('#password').val()
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
            if (data.error_code == '200') location.reload();
            /*
            if (data.error_code == '200') {
                var res =   '<ul class="login account">'+
                            '<li><a href="<?php echo site_url();?>
profile">Tài khoản</a></li>'+
                            '<li><a href="<?php echo site_url();?>
lunch/cart">Giỏ hàng</a></li>'+
                            '<li><a href="<?php echo site_url();?>
logout">Thoát</a></li>'+
                            '</ul>';
                $('#profile').html(res);
            }
            */
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the login');
        }
    });
}
</script>

<script src="/assets/datatables/jquery.dataTables.min.js"></script>
<script src="/assets/datatables/dataTables.bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript">
function reload_table() {
    table.ajax.reload(null,false); //reload datatable ajax 
}
function load(list) {
    $('#loadList').html("<center><img src='/assets/images/loading.gif' /></center>");
    var url = '';
    switch (list) {
        case 'friend':
            url = '<?php echo site_url();?>
profile/friends';
            $("#mfriend").addClass("active");
            $("#mlunch").removeClass("active");
            $("#mother").removeClass("active");
            break;
        case 'wallet':
            url = '<?php echo site_url();?>
profile/wallet';
            $("#mfriend").removeClass("active");
            $("#mlunch").removeClass("active");
            $("#mother").removeClass("active");
            break;
        case 'lunch':
            url = '<?php echo site_url();?>
profile/lunch';
            $("#mfriend").removeClass("active");
            $("#mlunch").addClass("active");
            $("#mother").removeClass("active");
            break;
        case 'order':
            url = '<?php echo site_url();?>
profile/friends';
            $("#mfriend").removeClass("active");
            $("#mlunch").removeClass("active");
            $("#mother").addClass("active");
            break;
    }
    if (url != '') {
        $.get(url, {}, 
        function(response) {
            $('#loadList').html(response);
        });
    }
    else alert('Chuc nang khong ton tai!');
}
function load_list(list) {
    var url;
    switch (list) {
        case 'friend':
            url = '<?php echo site_url();?>
profile/friends_list';
            break;
        case 'lunch':
            url = '<?php echo site_url();?>
profile/lunch_list';
            break;
        case 'wallet':
            url = '<?php echo site_url();?>
profile/wallet_list';
            break;
    }
    if (url != '') {
        table = $('#table').DataTable({ 
            "processing": true,
            "serverSide": true,
            "order": [],

            "ajax": {
                "url": url,
                "type": "POST"
            },
            "columnDefs": [
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            ],
        });
    }
}
function viewDetail(id, donhang) {
    $.get('<?php echo site_url();?>
profile/lunch_detail?id='+ id, {},
    function(response) {
        if (response.error_code == '201') alert(response.message);
        else $('#dialogbox').html(response);
    });
    $('#dialogbox').dialog({
        autoOpen:true,
        modal:true,
        title: "Đơn hàng của ngày "+ donhang,
        width: "60%",
        maxHeight: 400,
        position: { my: 'top', at: 'top+10' },
        open: function( event, ui ) {
            //alert('hello');
        }
    });    
}
function doimatkhau() {
    $.get('<?php echo site_url();?>
profile/chgPwd', {},
    function(response) {
        $('#xxx').html(response);
    });
}
function chgpwd() {
    $.ajax({
        url : "<?php echo site_url();?>
profile/chgPwd",
        type: "POST",
        data: {
            'isSent': 'OK',
            'oldpwd': $('#oldpwd').val(),
            'newpwd': $('#newpwd').val(),
            'newpwd_confirm': $('#newpwd_confirm').val()
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
            if (data.error_code == '200') window.location.href = "<?php echo site_url();?>
profile";
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Co loi xay ra');
        }
    });
}
</script>

</body>
</html><?php }} ?>