<?php /* Smarty version Smarty-3.1.13, compiled from "/Users/dinhhoaibao/Sites/mealens/views/lunch/cart.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1785324623594c12f74f7ad8-87089658%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8500111be1f50cf5a81c33ca9c78a5e654530e77' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/lunch/cart.tpl',
      1 => 1499090107,
      2 => 'file',
    ),
    'af0fda9ea677ca3f1c9dc6db7c00ab6b823810f7' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/layouts/home/master.tpl',
      1 => 1499090107,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1785324623594c12f74f7ad8-87089658',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_594c12f76b43a6_58290919',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_594c12f76b43a6_58290919')) {function content_594c12f76b43a6_58290919($_smarty_tpl) {?><!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=-100%, user-scalable=yes" />
<meta name="format-detection" content="telephone=no">
<title> MEAL - Enjoying and Sharing</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="stylesheet" href="/assets/css/styles.css">
<link rel="stylesheet" href="/assets/css/responsive.css">
<link rel="stylesheet" href="/assets/css/slick.css">

</head>
<body id="index" class="under">
<div id="wrapper">
    <?php echo $_smarty_tpl->getSubTemplate ('home/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    
<div id="mainvisual">
<h2>&nbsp;</h2>
</div>
<div id="main">
<div class="container">
<ul class="topic-path">
<li><a href="<?php echo site_url();?>
lunch">Cơm trưa</a></li>
<li>Giỏ hàng</li>
</ul>
<div class="clearfix">
<p>Nhập mã CODE để được giảm giá khi chọn order tại website:</p>
<p class="clearfix"><input type="text" placeholder="Enter your coupon here" class="coupon"><a href="" class="btn-link">MÃ GIẢM GIÁ</a></p>
</div>
<div class="tbl-scroll mb30">
<table class="checkout-cnt center">
<tr>
<th>Tên món ăn</th>
<th>Đơn giá</th>
<th>Số lượng</th>
<th>Thành tiền</th>
<th></th>
</tr>
<?php if ($_smarty_tpl->tpl_vars['carts']->value){?>
    <?php  $_smarty_tpl->tpl_vars['cart'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cart']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['carts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cart']->key => $_smarty_tpl->tpl_vars['cart']->value){
$_smarty_tpl->tpl_vars['cart']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['cart']->key;
?>
        <tr><td colspan="5" class="left"><h2><?php echo date("d-m-Y",strtotime($_smarty_tpl->tpl_vars['menu']->value[$_smarty_tpl->tpl_vars['k']->value]['mdate']));?>
</h2></td></tr>
        <?php if (isset($_smarty_tpl->tpl_vars["total"])) {$_smarty_tpl->tpl_vars["total"] = clone $_smarty_tpl->tpl_vars["total"];
$_smarty_tpl->tpl_vars["total"]->value = 0; $_smarty_tpl->tpl_vars["total"]->nocache = null; $_smarty_tpl->tpl_vars["total"]->scope = 0;
} else $_smarty_tpl->tpl_vars["total"] = new Smarty_variable(0, null, 0);?>
        <?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cart']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value){
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
            <tr>
            <td>
                <?php $_smarty_tpl->smarty->_tag_stack[] = array('php', array()); $_block_repeat=true; echo smarty_php_tag(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                    if (is_file("<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/thumb_<?php echo $_smarty_tpl->tpl_vars['c']->value['pic'];?>
")) echo "<span><img src='/<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/thumb_<?php echo $_smarty_tpl->tpl_vars['c']->value['pic'];?>
' /></span>";
                    elseif (is_file("<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/<?php echo $_smarty_tpl->tpl_vars['c']->value['pic'];?>
")) echo "<span><img src='/<?php echo $_smarty_tpl->tpl_vars['path_upload']->value;?>
meal/<?php echo $_smarty_tpl->tpl_vars['c']->value['pic'];?>
' /></span>";
                    else echo '';
                <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_php_tag(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                <span><?php echo $_smarty_tpl->tpl_vars['c']->value['name'];?>
</span>
            </td>
            <td><?php echo number_format($_smarty_tpl->tpl_vars['c']->value['price']);?>
</td>
            <td><input id="<?php echo $_smarty_tpl->tpl_vars['c']->value['rowid'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['c']->value['qty'];?>
" maxlength="3" size="10" onchange="updateCart('<?php echo $_smarty_tpl->tpl_vars['c']->value['rowid'];?>
')"></td>
            <td><?php echo number_format($_smarty_tpl->tpl_vars['c']->value['subtotal']);?>
</td>
            <td><a href="javascript:void(-1)" onclick="delCart('<?php echo $_smarty_tpl->tpl_vars['c']->value['rowid'];?>
', 'row')"><img src="/assets/images/ico_cancel_off.png" alt=""></a></td>
            </tr>
            <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['total']->value;?>
<?php $_tmp1=ob_get_clean();?><?php if (isset($_smarty_tpl->tpl_vars["total"])) {$_smarty_tpl->tpl_vars["total"] = clone $_smarty_tpl->tpl_vars["total"];
$_smarty_tpl->tpl_vars["total"]->value = $_tmp1+$_smarty_tpl->tpl_vars['c']->value['subtotal']; $_smarty_tpl->tpl_vars["total"]->nocache = null; $_smarty_tpl->tpl_vars["total"]->scope = 0;
} else $_smarty_tpl->tpl_vars["total"] = new Smarty_variable($_tmp1+$_smarty_tpl->tpl_vars['c']->value['subtotal'], null, 0);?>
        <?php } ?>
        <tr>
        <td colspan="5" class="total">
        <p><span>TỔNG CỘNG:</span> <span><?php echo number_format($_smarty_tpl->tpl_vars['total']->value);?>
</span></p>
        <ul>
            <li><a href="javascript:void(-1)" onclick="checkoutCart(<?php echo $_smarty_tpl->tpl_vars['menu']->value[$_smarty_tpl->tpl_vars['k']->value]['mid'];?>
)" class="btn-link">Hoàn tất (<?php echo date("d/m/Y",strtotime($_smarty_tpl->tpl_vars['menu']->value[$_smarty_tpl->tpl_vars['k']->value]['mdate']));?>
)</a></li>
        <li><a href="javascript:void(-1)" onclick="delCart(<?php echo $_smarty_tpl->tpl_vars['menu']->value[$_smarty_tpl->tpl_vars['k']->value]['mid'];?>
, 'date')">Xoá giỏ hàng (<?php echo date("d/m/Y",strtotime($_smarty_tpl->tpl_vars['menu']->value[$_smarty_tpl->tpl_vars['k']->value]['mdate']));?>
)</a></li>
        </ul>
        </td>
        </tr>
    <?php } ?>
<?php }else{ ?>
    <tr>
    <td colspan="5" class="center red">Chưa có dữ liệu</td>  
    </tr>
<?php }?>
</table>
</div>
</div>
</div>

    <?php echo $_smarty_tpl->getSubTemplate ('home/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</div>
<script src="/assets/js/jquery.js" type="text/javascript"></script>
<script src="/assets/js/common.js"></script>
<script src="/assets/js/slick.js"></script>
<script type="text/javascript">
function login() {
    $.ajax({
        url : "<?php echo site_url();?>
login",
        type: "POST",
        data: {
            'isSent': 'OK',
            'username': $('#username').val(),
            'password': $('#password').val()
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
            if (data.error_code == '200') location.reload();
            /*
            if (data.error_code == '200') {
                var res =   '<ul class="login account">'+
                            '<li><a href="<?php echo site_url();?>
profile">Tài khoản</a></li>'+
                            '<li><a href="<?php echo site_url();?>
lunch/cart">Giỏ hàng</a></li>'+
                            '<li><a href="<?php echo site_url();?>
logout">Thoát</a></li>'+
                            '</ul>';
                $('#profile').html(res);
            }
            */
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the login');
        }
    });
}
</script>

<script type="text/javascript">
$(document).ready(function() {
    $('body').addClass("checkout");
    $("#menu_lunch").addClass("active");
});
function updateCart(cartID) {
    $.ajax({
        url : "<?php echo site_url();?>
lunch/updCart",
        type: "POST",
        data: {
            'cartID': cartID,
            'cartVal': $('#'+cartID).val()
        },
        dataType: "JSON",
        success: function(data) {
            //alert(data.message);
            //loadCart();
            window.location.href = "<?php echo site_url();?>
lunch/cart";
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the cap nhat');
        }
    });
}
function delCart(cartID, cartOpt) {
    $.ajax({
        url : "<?php echo site_url();?>
lunch/delCart",
        type: "POST",
        data: {
            'cartID': cartID,
            'cartOpt': cartOpt
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
            //loadCart();
            window.location.href = "<?php echo site_url();?>
lunch/cart";
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the cap nhat');
        }
    });
}
function checkoutCart(menuID) {
    <?php if ($_smarty_tpl->tpl_vars['login']->value=='YES'){?>
        window.location.href = "<?php echo site_url();?>
lunch/checkout?menu="+menuID;
    <?php }else{ ?>
        alert('Vui lòng đăng nhập trước');
    <?php }?>
}
</script>

</body>
</html><?php }} ?>