<?php /* Smarty version Smarty-3.1.13, compiled from "/Users/dinhhoaibao/Sites/mealens/modules/users/views/detail.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1630958437594c913ab8a477-54281031%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8a6d9d7048bf1533752d0572c75b17ca600cae0f' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/modules/users/views/detail.tpl',
      1 => 1499090107,
      2 => 'file',
    ),
    'e562e6fefacff9e7ea47ea7394447121d702aa45' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/layouts/master.tpl',
      1 => 1499090107,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1630958437594c913ab8a477-54281031',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_594c913acd3085_58229229',
  'variables' => 
  array (
    'assets_path' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_594c913acd3085_58229229')) {function content_594c913acd3085_58229229($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<title>MEAL - Enjoying and Sharing</title>
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<!-- bootstrap & fontawesome -->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/bootstrap.min.css" />
<link rel="stylesheet" href="/assets/fonts/font-awesome.css" />
<!-- text fonts -->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/fonts.googleapis.com.css" />
<!-- ace styles -->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<!--[if lte IE 9]>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-part2.min.css" class="ace-main-stylesheet" />
<![endif]-->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-skins.min.css" />
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-rtl.min.css" />
<!--[if lte IE 9]>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-ie.min.css" />
<![endif]-->
<!-- inline styles related to this page -->

<!-- ace settings handler -->
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/ace-extra.min.js"></script>
<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
<!--[if lte IE 8]>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/html5shiv.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/respond.min.js"></script>
<![endif]-->
</head>
<body class="no-skin">
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_changepwd" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <form action="<?php echo site_url();?>
admin/user/changePWD" id="formChangePWD" method="post">
            <input type="hidden" value="OK" id="isSent" name="isSent" />
            <div class="modal-body form">
                <div class="form-group">
                    <label>Mật khẩu cũ:</label>
                    <input class="form-control" type="password" id="password_old" name="password_old" />
                </div>
                <div class="form-group">
                    <label>Mật khẩu mới:</label>
                    <input class="form-control" type="password" id="password_new" name="password_new" />
                </div>
                <div class="form-group">
                    <label>Xác nhận mật khẩu mới:</label>
                    <input class="form-control" type="password" id="password_new_confirm" name="password_new_confirm" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-xs btn-primary">Submit</button>
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="main-container ace-save-state" id="main-container">
<?php echo $_smarty_tpl->getSubTemplate ('sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li><a href="<?php echo site_url();?>
users">Danh sách thành viên</a></li>
<li class="active">Chi tiết thành viên</li>
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<h3 class="header smaller lighter blue"><?php echo $_smarty_tpl->tpl_vars['profile']->value->uname;?>
</h3>
<div class="clearfix">
<div class="pull-left tableTools-container"></div>
</div>
<div class="col-xs-12 col-sm-3 center">
<span class="profile-picture"><img class="editable img-responsive" alt="<?php echo $_smarty_tpl->tpl_vars['profile']->value->uname;?>
's Avatar" id="avatar2" src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
images/avatars/profile-pic.jpg" /></span>
<div class="space space-4"></div>
<a href="#" class="btn btn-xs btn-block btn-primary"><i class="ace-icon fa fa-envelope-o bigger-110"></i> Gởi tin nhắn</a>
<a href="#" class="btn btn-xs btn-block btn-success" onclick="add_amount()"><i class="ace-icon fa fa-dollar bigger-110"></i> Nạp thêm tiền</a>
</div>
<div class="col-xs-12 col-sm-9">
<div class="profile-user-info">
<div class="profile-info-row">
<div class="profile-info-name">Địa chỉ</div>
<div class="profile-info-value">
<i class="fa fa-map-marker light-orange bigger-110"></i>
<span><?php echo $_smarty_tpl->tpl_vars['profile']->value->uaddress;?>
</span>
</div>
</div>
<div class="profile-info-row">
<div class="profile-info-name">Ngày tham gia</div>
<div class="profile-info-value">
<span><?php echo date('d/m/Y',strtotime($_smarty_tpl->tpl_vars['profile']->value->ureg));?>
</span>
</div>
</div>
<div class="profile-info-row">
<div class="profile-info-name">Email</div>
<div class="profile-info-value">
<i class="fa fa-envelope light-blue bigger-110"></i>
<span><a href="#" target="_blank"><?php echo $_smarty_tpl->tpl_vars['profile']->value->umail;?>
</a></span>
</div>
</div>
<div class="profile-info-row">
<div class="profile-info-name">Điện thoại</div>
<div class="profile-info-value">
<i class="fa fa-phone light-green bigger-110"></i>
<span><?php echo $_smarty_tpl->tpl_vars['profile']->value->utel;?>
</span>
</div>
</div>
</div>
<div class="hr hr-8 dotted"></div>
<div class="profile-user-info">
<?php if ($_smarty_tpl->tpl_vars['parent']->value){?>
<div class="profile-info-row">
<div class="profile-info-name">Người giới thiệu</div>
<div class="profile-info-value">
<i class="fa fa-user-secret blue bigger-110"></i>
<span><a href="<?php echo site_url();?>
users/detail?id=<?php echo $_smarty_tpl->tpl_vars['parent']->value->uid;?>
"><?php echo $_smarty_tpl->tpl_vars['parent']->value->uname;?>
</a></span>
</div>
</div>
<?php }?>
<div class="profile-info-row">
<div class="profile-info-name">Tiền khả dụng</div>
<div class="profile-info-value">
<i class="fa fa-dollar green bigger-110"></i>
<span><a href="javascript:;" onclick="load('wallet', '<?php echo $_smarty_tpl->tpl_vars['profile']->value->uid;?>
');" class="green" id="wallet"><?php echo number_format($_smarty_tpl->tpl_vars['profile']->value->uwallet);?>
</a></span>
</div>
</div>
<div class="profile-info-row">
<div class="profile-info-name">Bạn bè</div>
<div class="profile-info-value">
<i class="fa fa-users light-orange bigger-110"></i>
<span><a href="javascript:;" onclick="load('friend', '<?php echo $_smarty_tpl->tpl_vars['profile']->value->uid;?>
');"><?php echo $_smarty_tpl->tpl_vars['friends']->value;?>
</a></span>
</div>
</div>
</div>
</div><!-- /.col -->
<!-- PAGE CONTENT ENDS -->
</div>
</div>
<div class="space-8"></div>
<div class="row">
<div id="saoke" class="col-xs-12"></div>
</div>
</div>
</div>
</div>

<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</div>

<!-- basic scripts -->
<!--[if !IE]> -->
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery-2.1.4.min.js"></script>
<!-- <![endif]-->
<!--[if IE]>
    <script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/bootstrap.min.js"></script>
<!-- ace scripts -->
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/ace-elements.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/ace.min.js"></script>
<!-- inline scripts related to this page -->
<script type="text/javascript">
function changePWD() {
    $('#formChangePWD')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_changepwd').modal('show'); // show bootstrap modal
    $('.modal-title').text('Đổi mật khẩu'); // Set Title to Bootstrap modal title
}
</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <form id="form" method="post">
            <input type="hidden" id="isSent" name="isSent" value="OK" />
            <input type="hidden" id="uid" name="uid" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->uid;?>
" />
            <div class="modal-body form">
                <div class="form-group">
                    <label>Số tiền:</label>
                    <input class="form-control" type="text" id="amount" name="amount" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="naptien()" class="btn btn-xs btn-primary">Submit</button>
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
plugins/datatables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar_users").addClass("active");
});
$('input#amount').keyup(function(event) {
  // skip for arrow keys
  if(event.which >= 37 && event.which <= 40) return;

  // format number
  $(this).val(function(index, value) {
    return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  });
});
function reload_table() {
    table.ajax.reload(null,false); //reload datatable ajax 
}
function add_amount() {
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Nạp thêm tiền'); // Set Title to Bootstrap modal title
}
function naptien() {
    $.ajax({
        url : "<?php echo site_url();?>
users/naptien",
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data) {
           //if success close modal and reload ajax table
           $('#modal_form').modal('hide');
           if (data.status == 'denied') document.location.href = '<?php echo site_url();?>
admin/denied?w=write';
           else {
               var wallet = $('#wallet').text();
               var amount = $('#amount').val();
               var sum = parseInt(wallet.replace(/,/g, "")) + parseInt(amount.replace(/,/g, ""));
               $('#wallet').text(sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
               //reload_table();
               load('wallet', $('#uid').val());
           }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the nap tien');
        }
    });
}
function load(list, id) {
    $('#saoke').html("<center><i class='ace-icon fa fa-spinner fa-spin orange bigger-125'></i></center>");
    var url = '';
    switch (list) {
        case 'friend':
            url = '<?php echo site_url();?>
users/friends?id='+ id;
            break;
        case 'wallet':
            url = '<?php echo site_url();?>
users/saoke?id='+ id;
            break;
    }
    if (url != '') {
        $.get(url, {}, 
        function(response) {
            if (response == 'denied') document.location.href = site_url + 'denied?w=read';
            else $('#saoke').html(response);
        });
    }
    else alert('Chuc nang khong ton tai!');
}
function load_list(list, id) {
    var url;
    switch (list) {
        case 'friend':
            url = '<?php echo site_url();?>
users/friends_list?id='+ id;
            break;
        case 'wallet':
            url = '<?php echo site_url();?>
users/saoke_list?id='+ id;
            break;
    }
    if (url != '') {
        table = $('#table').DataTable({ 
            "processing": true,
            "serverSide": true,
            "order": [],

            "ajax": {
                "url": url,
                "type": "POST"
            },
            "columnDefs": [
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            ],
        });
    }
}
</script>

</body>
</html><?php }} ?>