<?php /* Smarty version Smarty-3.1.13, compiled from "/Users/dinhhoaibao/Sites/mealens/modules/users/views/action.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16330099085950b3a907a405-31130411%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ec1c4728663232283c5601b5fe2a275f66fa0db5' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/modules/users/views/action.tpl',
      1 => 1496338676,
      2 => 'file',
    ),
    'e562e6fefacff9e7ea47ea7394447121d702aa45' => 
    array (
      0 => '/Users/dinhhoaibao/Sites/mealens/views/layouts/master.tpl',
      1 => 1495700095,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16330099085950b3a907a405-31130411',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'assets_path' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5950b3a91948a6_78594390',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5950b3a91948a6_78594390')) {function content_5950b3a91948a6_78594390($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<title>MEAL - Enjoying and Sharing</title>
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<!-- bootstrap & fontawesome -->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/bootstrap.min.css" />
<link rel="stylesheet" href="/assets/fonts/font-awesome.css" />
<!-- text fonts -->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/fonts.googleapis.com.css" />
<!-- ace styles -->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<!--[if lte IE 9]>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-part2.min.css" class="ace-main-stylesheet" />
<![endif]-->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-skins.min.css" />
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-rtl.min.css" />
<!--[if lte IE 9]>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
css/ace-ie.min.css" />
<![endif]-->
<!-- inline styles related to this page -->

<!-- ace settings handler -->
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/ace-extra.min.js"></script>
<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
<!--[if lte IE 8]>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/html5shiv.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/respond.min.js"></script>
<![endif]-->
</head>
<body class="no-skin">
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_changepwd" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <form action="<?php echo site_url();?>
admin/user/changePWD" id="formChangePWD" method="post">
            <input type="hidden" value="OK" id="isSent" name="isSent" />
            <div class="modal-body form">
                <div class="form-group">
                    <label>Mật khẩu cũ:</label>
                    <input class="form-control" type="password" id="password_old" name="password_old" />
                </div>
                <div class="form-group">
                    <label>Mật khẩu mới:</label>
                    <input class="form-control" type="password" id="password_new" name="password_new" />
                </div>
                <div class="form-group">
                    <label>Xác nhận mật khẩu mới:</label>
                    <input class="form-control" type="password" id="password_new_confirm" name="password_new_confirm" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-xs btn-primary">Submit</button>
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="main-container ace-save-state" id="main-container">
<?php echo $_smarty_tpl->getSubTemplate ('sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="main-content">
<div class="main-content-inner">
<!--PATH BEGINS-->
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a></li>
<li><a href="<?php echo site_url();?>
users">Danh sách thành viên</a></li>
<li class="active"><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</li>
</ul>
<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon"><input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" /><i class="ace-icon fa fa-search nav-search-icon"></i></span>
</form>
</div>
</div>
<!--PATH ENDS-->
<div class="page-content">
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<h3 class="header smaller lighter blue"><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h3>
<div class="clearfix">
<div class="pull-left tableTools-container"></div>
</div>
<form class="form-horizontal" role="form" id="validation-form" action="" method="post" enctype="multipart/form-data">
<input type="hidden" id="isSent" name="isSent" value="OK" />
<input type="hidden" id="id" name="id" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->uid;?>
" />
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="name">Họ tên:</label>
<div class="col-sm-9">
<input type="text" id="name" name="name" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->uname;?>
" class="input-xlarge" />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="address">Địa chỉ:</label>
<div class="col-sm-9">
<input type="text" id="address" name="address" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->uaddress;?>
" class="form-control" />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="email">Email</label>
<div class="col-sm-9">
<span class="input-icon">
<input type="text" id="email" name="email" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->umail;?>
" class="input-xlarge" />
<i class="ace-icon fa fa-envelope blue"></i>
</span>
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="tel">Phone</label>
<div class="col-sm-9">
<span class="input-icon">
<input type="text" id="tel" name="tel" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->utel;?>
" />
<i class="ace-icon fa fa-phone blue"></i>
</span>
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="picture">Picture</label>
<div class="col-sm-9">
<input type="file" id="picture" name="picture" />
</div>
</div>
<?php if (!$_smarty_tpl->tpl_vars['user']->value->uid){?>
<div class="form-group">
<label class="col-sm-3 control-label no-padding-right" for="parent">Người giới thiệu</label>
<div class="col-sm-9" style="position:relative;">
<span class="input-icon">
<input type="text" id="parent" name="parent" class="input-xlarge" readonly="true" value="" />
<input type="hidden" id="idparent" name="idparent" />
<i class="ace-icon fa fa-user-secret blue"></i>
</span>
<div style="position:absolute; top:8px; left:292px;"><a href="javascript:void(-1)" onclick="selectParent()">Select</a></div>
</div>
</div>
<?php }?>
<div class="clearfix form-actions">
<div class="col-md-offset-3 col-md-9">
<button class="btn btn-xs btn-info" type="submit">
<i class="ace-icon fa fa-check bigger-110"></i>
Submit
</button>
<button class="btn btn-xs" type="reset">
<i class="ace-icon fa fa-undo bigger-110"></i>
Reset
</button>
</div>
</div>
</form>
<!-- PAGE CONTENT ENDS -->
</div>
</div>
</div>
</div>
</div>

<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</div>

<!-- basic scripts -->
<!--[if !IE]> -->
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery-2.1.4.min.js"></script>
<!-- <![endif]-->
<!--[if IE]>
    <script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/bootstrap.min.js"></script>
<!-- ace scripts -->
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/ace-elements.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/ace.min.js"></script>
<!-- inline scripts related to this page -->
<script type="text/javascript">
function changePWD() {
    $('#formChangePWD')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_changepwd').modal('show'); // show bootstrap modal
    $('.modal-title').text('Đổi mật khẩu'); // Set Title to Bootstrap modal title
}
</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <form id="form" method="post">
            <div class="modal-body form">
                <div class="input-group">
                <span class="input-group-addon">
                <i class="ace-icon fa fa-check"></i>
                </span>
                <input type="text" id="searchInfo" name="searchInfo" class="form-control search-query" placeholder="Nhập ID, Email, Điện thoại để tìm" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-purple btn-sm">
                <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                Search
                </button>
                </span>
                </div>
            </div>
            <div class="modal-footer">
                <table class="table table-bordered table-striped search_member">
                <tr class="tr_top" style="background-color:#3c8dbc; color:#fff;">
                <th>Tên</th>
                <th>Email</th>
                <th>Điện thoại</th>
                </tr>
                </table>                
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<!-- page specific plugin scripts -->
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/wizard.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery.validate.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery-additional-methods.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/bootbox.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/jquery.maskedinput.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['assets_path']->value;?>
js/select2.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#sidebar_users").addClass("active");
    $('#picture').ace_file_input({
        style: 'well',
        btn_choose: 'Drop files here or click to choose',
        btn_change: null,
        no_icon: 'ace-icon fa fa-cloud-upload',
        allowExt: ["jpeg", "jpg", "png", "gif" , "bmp"],
	allowMime: ["image/jpg", "image/jpeg", "image/png", "image/gif", "image/bmp"],
        droppable: true,
        thumbnail: 'small'//large | fit
        //,icon_remove:null//set null, to hide remove/reset button
        /**,before_change:function(files, dropped) {
                //Check an example below
                //or examples/file-upload.html
                return true;
        }*/
        /**,before_remove : function() {
                return true;
        }*/
        ,
        preview_error : function(filename, error_code) {
            //name of the file that failed
            //error_code values
            //1 = 'FILE_LOAD_FAILED',
            //2 = 'IMAGE_LOAD_FAILED',
            //3 = 'THUMBNAIL_FAILED'
            //alert(error_code);
        }
    }).on('change', function(){
            //console.log($(this).data('ace_input_files'));
            //console.log($(this).data('ace_input_method'));
    });
    $('#validation-form').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        ignore: "",
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email:true
            },
            tel: {
                required: true
            }
        },
        messages: {
            email: {
                required: "Email is required",
                email: "Please provide a valid email."
            }
        },
        submitHandler: function (form) {
            $("button[type='submit']").attr("disabled", true).text("Please wait...");
            form.submit();
        },
        invalidHandler: function (form) {
        }
    });

    $( '.btn-sm' ).on("click",function() {
        $.ajax({
            type: "POST",
            url: '<?php echo site_url();?>
users/select',
            data: {
                isSent:'OK',
                searchInfo:$("#searchInfo").val()
            },
            success: function(response) {
                var dataJson = JSON.parse(response);
                if (dataJson.error_code == 200){
                    $(".search_member tr").not(".tr_top").remove();
                    for (var i = 0; i < dataJson.data.length; i++) {
                        $(".tr_top").after(''+
                            '<tr style="text-align:left;">'+
                            '<td><a href="javascript:addVal(\''+ dataJson.data[i].id +'\', \''+ dataJson.data[i].name +'\')">'+ dataJson.data[i].name +'</a></td>'+
                            '<td><a href="javascript:addVal(\''+ dataJson.data[i].id +'\', \''+ dataJson.data[i].mail +'\')">'+ dataJson.data[i].mail +'</a></td>'+
                            '<td><a href="javascript:addVal(\''+ dataJson.data[i].id +'\', \''+ dataJson.data[i].tel +'\')">'+ dataJson.data[i].tel +'</a></td>'+
                            '</tr>');
                    }
                } else {
                    $(".search_member tr").not(".tr_top").remove();
                    $(".tr_top").after('<tr valign="top"><td colspan="3" style="text-align:center; color:red;">'+ dataJson.message +'</td></tr>');
                }
            }
        });
        return false;
    });
});
function selectParent() {
    $('#form')[0].reset();
    $(".search_member tr").not(".tr_top").remove();
    $('#modal_form').modal('show');
    $('.modal-title').text('Chọn người giới thiệu');
}
function addVal(id, search, field) {
    $('#idparent').val(id);
    $('#parent').val($.trim(search));
    $('#modal_form').modal('hide');
}
</script>

</body>
</html><?php }} ?>