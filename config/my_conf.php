<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$config['path_upload'] = '_uploads/';
$config['trans_type'] = array(
    'Không xác định',//0
    'Nạp tiền',//1
    'Mua hàng',//2
    'Hoàn tiền',//3
    'Hoa hồng nạp tiền',//4
    'Khuyến mãi',//5
    'Hoa hồng mua hàng'//6
);
$config['order_type'] = array(
    'Thanh toán khi nhận hàng',//1
    'Thanh toán bằng mealens'//2
);
$config['order_status'] = array(
    'Đơn hàng mới',//0
    'Đã xác thực',//1
    'Đang giao hàng',//2
    'Đã giao hàng',//3
    'Đã huỷ',//4
    'Không giao được',//5
    'Đã thanh toán'//6
);
