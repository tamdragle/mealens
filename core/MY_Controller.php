<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends MX_Controller {
    var $permissions;
    var $permarr;
    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('ssAdminId')) {
            $this->load->model('admin/mod_permission');
            $this->load->library('permission');
            $permissions = $this->mod_permission->_getPermissions(array(
                _MOD_ADMIN,
                _MOD_SALES,
                _MOD_CANES,
                _MOD_MEAL_DISH,
                _MOD_MEAL_MENU,
                _MOD_MEAL_MEMBER,
                _MOD_COMPANY,
                _MOD_MEAL_ORDER,
                _MOD_IMAGES,
                _MOD_USERS
            ), $this->session->userdata('ssAdminId'));
            foreach ($permissions as $permission) {
                $this->permissions[$permission['amid']] = $permission['value'];
            }
            $this->parser->assign('ssAdmin', array(
                'ssAdminId' => $this->session->userdata('ssAdminId'),
                'ssAdminLogin' => $this->session->userdata('ssAdminLogin'),
                'ssAdminFullname' => $this->session->userdata('ssAdminFullname')
            ));
        }
        if ($this->session->userdata('ssId')) {
            $this->parser->assign('login', 'YES');
            $this->parser->assign('ssUser', array(
                'ssId' => $this->session->userdata('ssId'),
                'ssFullname' => $this->session->userdata('ssFullname'),
                'ssTel' => $this->session->userdata('ssTel'),
                'ssAddress' => $this->session->userdata('ssAddress')                
            ));
        }
        $this->parser->assign('assets_path', _ASSETS_PATH);
        //$this->_module = $this->router->fetch_module();
    }
}
class API_Controller extends REST_Controller {
    function __construct() {
        parent::__construct();
    }
}
/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */