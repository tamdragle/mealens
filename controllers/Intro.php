<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Intro extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }
    function index() {
        $this->parser->parse('general/intro');
    }
}