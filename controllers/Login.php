<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mod_user');
    }
    function index() {
        if ($this->input->post('isSent') == 'OK') {
            $salt = $this->mod_user->_getSalt($this->input->post('username'));
            if ($salt) {
                $user = $this->mod_user->_login(array(
                    'uid' => $salt['uid'],
                    'password' => $this->input->post('password'),
                    'salt' => $salt['usalt']
                ));
                if ($user) {
                    $this->session->set_userdata(array(
                        'ssId' => $user['uid'],
                        'ssFullname' => $user['uname'],
                        'ssTel' => $user['utel'],
                        'ssAddress' => $user['uaddress']
                    ));
                    echo '{"error_code":200,"message":"Login thành công"}';
                }
                else echo '{"error_code":202,"message":"Mật khẩu không đúng"}';
            }
            else echo '{"error_code":201,"message":"Không tìm thấy thông tin user"}';
        }
        exit;
    }
}