<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mod_user');
    }
    function index() {
        $this->parser->assign('user', $this->mod_user->_get($this->session->userdata('ssId')));
        $this->parser->parse('profile/index');
    }
    function friends() {
        $this->parser->parse('profile/friends');
    }
    function friends_list() {
        $this->mod_user->parent = $this->session->userdata('ssId');
        $list = $this->mod_user->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $u) {
            $no++;
            $row = array();
            $row[] = "<a href='#'>{$u->uname}</a>";
            $row[] = date('d/m/Y', strtotime($u->ureg));

            $data[] = $row;
        }

        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mod_user->count_all(),
            "recordsFiltered" => $this->mod_user->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    function lunch() {
        $this->parser->parse('profile/lunch');
    }
    function lunch_list() {
        $this->config->load('my_conf');
        $status = $this->config->item('order_status');

        $this->load->model('mod_lunch');
        $this->mod_lunch->uid = $this->session->userdata('ssId');
        $list = $this->mod_lunch->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $o) {
            $no++;
            $row = array();
            $row[] = "<a href='javascript:void(-1);' onclick='viewDetail({$o->moid}, \"".date('d/m/Y', strtotime($o->mdate))."\")'>".date('d/m/Y H:i:s', strtotime($o->modate))."</a>";
            $row[] = $o->soluong;
            $row[] = ($o->tong < SHIPPING_VALUE) ? number_format($o->tong + SHIPPING_FEE) : number_format($o->tong);
            $row[] = date('d/m/Y', strtotime($o->mdate));
            $row[] = $status[$o->mostatus];

            $data[] = $row;
        }

        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mod_lunch->count_all(),
            "recordsFiltered" => $this->mod_lunch->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    function lunch_detail() {
        $this->load->model('mod_lunch');
        $this->config->load('my_conf');
        $this->parser->assign('shipping_value', SHIPPING_VALUE);
        $this->parser->assign('shipping_fee', SHIPPING_FEE);
        $this->parser->assign('path_upload', $this->config->item('path_upload'));
        $this->parser->assign('detail', $this->mod_lunch->_getOrderDetail($this->input->get('id')));
        $this->parser->assign('details', $this->mod_lunch->_getOrderDetails($this->input->get('id')));
        $this->parser->parse('profile/lunch_detail');
    }
    function wallet() {
        $this->parser->parse('profile/wallet');
    }
    function wallet_list() {
        $this->load->model('mod_saoke');
        $this->mod_saoke->id = $this->session->userdata('ssId');
        $list = $this->mod_saoke->get_datatables();
        $data = array();
        
        $this->config->load('my_conf');
        $type = $this->config->item('trans_type');
        
        $no = @$_POST['start'];
        foreach ($list as $t) {
            $no++;
            $row = array();
            $row[] = date('d/m/Y H:i:s', strtotime($t->trandate));
            $row[] = number_format($t->tranvalue);
            $row[] = $type[$t->trantype];
            $row[] = $t->trannote;
            $row[] = number_format($t->tranbalance);
            $row[] = $t->user;

            $data[] = $row;
        }

        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mod_saoke->count_all(),
            "recordsFiltered" => $this->mod_saoke->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);        
    }
    function chgPwd() {
        if ($this->input->post('isSent') == 'OK') {
            $params = $this->input->post();
            if ($params['oldpwd'] == '') {
                echo '{"error_code":201,"message":"Vui lòng nhập mật khẩu cũ"}';
                exit;
            }
            //Email
            if ($params['newpwd'] == '') {
                echo '{"error_code":201,"message":"Vui lòng nhập mật khẩu mới"}';
                exit;
            }
            if ($params['newpwd'] != $params['newpwd_confirm']) {
                echo '{"error_code":201,"message":"Xác nhận mật khẩu không chính xác"}';
                exit;
            }
            $user = $this->mod_user->_get($this->session->userdata('ssId'));
            if (md5(md5($params['oldpwd']).$user->usalt) != $user->upwd) {
                echo '{"error_code":201,"message":"Mật khẩu cũ không đúng"}';
                exit;                                
            }
            if ($this->mod_user->_chgPwd(array(
                'id' => $this->session->userdata('ssId'),
                'salt' => $user->usalt,
                'pwd' => $params['newpwd_confirm']
            ))) {
                echo '{"error_code":200,"message":"Đổi mật khẩu thành công"}';
            }
            else {
                echo '{"error_code":201,"message":"Không đổi được mật khẩu"}';
            }
            exit;                                
        }
        $this->parser->parse('profile/chgpwd');
    }
}