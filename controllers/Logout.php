<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }
    function index() {
        if (!$this->session->userdata('ssId')) redirect(site_url().'login');
        $this->session->sess_destroy();
        redirect(site_url().'lunch');
    }
}