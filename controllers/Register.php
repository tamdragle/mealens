<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Register extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mod_user');
    }
    function index() {
        if ($this->input->post('isSent') == 'OK') {
            $params = $this->input->post();
            if ($params['name'] == '') {
                echo '{"error_code":201,"message":"Vui lòng nhập tên"}';
                exit;
            }
            //Email
            if ($params['mail'] == '') {
                echo '{"error_code":201,"message":"Vui lòng nhập email"}';
                exit;
            }
            if (!filter_var($params['mail'], FILTER_VALIDATE_EMAIL)) {
                echo '{"error_code":201,"message":"Vui lòng nhâoj đúng định dạng email"}';
                exit;                
            }
            if ($this->mod_user->_isExists('email', $params['mail'])) {
                echo '{"error_code":201,"message":"Email đã có người sử dụng"}';
                exit;                                
            }
            //Tel
            if ($params['tel'] == '') {
                echo '{"error_code":201,"message":"Vui lòng nhập số điện thoại"}';
                exit;
            }
            if (!is_numeric($params['tel'])) {
                echo '{"error_code":201,"message":"Vui lòng nhâoj đúng định dạng số điẹn thoại"}';
                exit;
            }
            if ($this->mod_user->_isExists('tel', $params['tel'])) {
                echo '{"error_code":201,"message":"Điện thoại đã có người sử dụng"}';
                exit;                                
            }
            //Address
            if ($params['address'] == '') {
                echo '{"error_code":201,"message":"Vui lòng nhập địa chỉ nhận hàng"}';
                exit;
            }
            //Kiem tra thong tin nguoi gioi thieu
            $params['salt'] = getRandom(15);
            $params['pwd'] = 'mealens';
            $ref = $this->mod_user->_getRef($params['ref']);
            if ($ref) {
                $params['parent'] = $ref['uid'];
                $params['ref'] = (!is_null($ref['uref'])) ? $ref['uref'].$ref['uid'].'-' : $ref['uid'].'-';
                $params['level'] = $ref['ulevel'] + 1;              
            }
            else {
                $params['parent'] = 0;
                $params['ref'] = '-';
                $params['level'] = 0;              
            }
            if (PROMOTION_REG) {
                $params['wallet'] = PROMOTION_REG_VALUE;
            }
            if ($this->mod_user->_register($params)) {
                echo '{"error_code":200,"message":"Đăng ký thành công, vui lòng đăng nhập để dặt cơm hoặc mua hàng"}';
            }
            else {
                echo '{"error_code":201,"message":"Không thể ghi dữ liệu"}';
            }
            exit;
        }
        $this->parser->parse('general/register');
    }
}