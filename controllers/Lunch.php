<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Lunch extends MY_Controller {

    public function __construct() {
        parent::__construct();
        //load model
        $this->load->model('mod_lunch');
        $this->load->library('cart');
        $this->config->load('my_conf');
        $this->parser->assign('path_upload', $this->config->item('path_upload'));
    }
    function index() {
        $this->parser->parse('lunch/soon');
    }
    function menu() {
        $z = ($this->input->get('z')) ? $this->input->get('z') : date('z');
        $year = date('Y');
        $type = $this->input->get('type');
        $this->mod_lunch->z = $z;
        $this->mod_lunch->year = $year;
        $this->mod_lunch->type = $type;
        $menu = $this->mod_lunch->_get($z, $year);
        if ($menu) {
            $this->mod_lunch->menu = $menu['mid'];
            $this->parser->assign('menu', $this->mod_lunch->_getDetail());
        }
        else {
            $this->parser->assign('menu', FALSE);
        }        
        $this->parser->assign('current', date('z'));
        $this->parser->assign('z', $z);
        $this->parser->assign('type', $type);
        $this->parser->assign('week', current_week());
        $this->parser->assign('types', $this->mod_lunch->_getType());

        $this->parser->parse('lunch/index');
    }
    function detail() {
        $this->parser->parse('lunch/detail');
    }
    function cart() {
        $cart = $this->cart->contents();
        $carts = array();
        foreach ($cart as $c) {
            $carts[$c['menu']][] = $c;
        }
        ksort($carts);
        $this->parser->assign('carts', $carts);
        $menu = array();
        foreach ($carts as $k => $cs) {
            $this->mod_lunch->id = $k;
            $menu[$k] = $this->mod_lunch->_get();
        }
        $this->parser->assign('menu', $menu);
        $this->parser->parse('lunch/cart');        
    }
    function addCart() {
        $mid = $this->input->post('menuID');
        $this->mod_lunch->id = $mid;
        $detail = $this->mod_lunch->_getDetail();
        $this->mod_lunch->id = $detail['mid'];
        $menu = $this->mod_lunch->_get();
        if ($menu['mz'] < date('z')) {
            echo '{"error_code":201,"message":"Không thể thêm được vào giỏ hàng (Quá ngày)"}';
            exit;
        }
        elseif (($menu['mz'] == date('z')) AND (date('H') > 10)) {
            echo '{"error_code":201,"message":"Không thể thêm được vào giỏ hàng (Quá giờ)"}';
            exit;
        }
        $insert_data = array(
            'id' => $detail['did'],
            'name' => $detail['dname'],
            'price' => $detail['mprice'],
            'qty' => 1,
            'menu' => $detail['mid'],
            'pic' => $detail['dpic']
        );		
        // This function add items into cart.
        if ($this->cart->insert($insert_data)) {
           echo '{"error_code":200,"message":"Đã thêm vào giỏ hàng"}';
        }
        else {
            echo '{"error_code":201,"message":"Không thêm được vào giỏ hàng"}';
        }
        exit;
    }
    function updCart() {
        $data = array(
            'rowid' => $this->input->post('cartID'),
            'qty'   => $this->input->post('cartVal'),
        );
        if ($this->cart->update($data)) {
            echo '{"error_code":200,"message":"Cập nhật giỏ hàng thành công"}';
        }
        else {
            echo '{"error_code":201,"message":"Không cập nhật được vào giỏ hàng"}';
        }
        exit;
    }
    function delCart() {
        $opt = $this->input->post('cartOpt');
        $id = $this->input->post('cartID');
        // Check rowid value.
        if ($opt === "all") {
            // Destroy data which store in session.
            $this->cart->destroy();
        } elseif ($opt === "date") {
            $data = array();
            $cart = $this->cart->contents();
            foreach ($cart as $c) {
                if ($c['menu'] == $id) {
                    $data[] = array(
                        'rowid' => $c['rowid'],
                        'qty' => 0
                    );
                }
                $this->cart->update($data);
            }
        } else {
            // Destroy selected rowid in session.
            $data = array(
                'rowid' => $id,
                'qty' => 0
            );
            // Update cart data, after cancle.
            $this->cart->update($data);
        }
        echo '{"error_code":200,"message":"Xoá thành công"}';
        exit;
    }
    function checkout() {
        $error = FALSE;
        //Lay thong tin menu
        $mid = $this->input->get('menu');
        $this->mod_lunch->id = $mid;
        $menu = $this->mod_lunch->_get();
        if (!$menu) {
            $this->parser->assign('message', 'Thực đơn không tồn tại');
        }
        else {
            if ($menu['mz'] < date('z')) {//Qua ngay
                $error = TRUE;
                $this->parser->assign('message', 'Thực đơn đã quá ngày, không thể hoàn tất đơn hàng');
            }
            elseif (($menu['mz'] == date('z')) AND date('H') > 10) {//Qua gio
                $error = TRUE;
                $this->parser->assign('message', 'Thực đơn đã quá giờ, không thể hoàn tất đơn hàng');
            }
            else {
                $this->parser->assign('menu', $menu);
                //Lay thong tin cart cua menu do
                $cart = $this->cart->contents();
                $carts = array();
                foreach ($cart as $c) {
                    if ($c['menu'] == $mid) $carts[] = $c;
                }
                $this->parser->assign('message', FALSE);
                $this->parser->assign('cart', $carts);                
            }
        }
        if ($error) {
            //Xoa don hang
            $data = array();
            $cart = $this->cart->contents();
            foreach ($cart as $c) {
                if ($c['menu'] == $mid) {
                    $data[] = array(
                        'rowid' => $c['rowid'],
                        'qty' => 0
                    );
                }
                $this->cart->update($data);
            }            
        }
        $this->parser->assign('shipping_value', SHIPPING_VALUE);
        $this->parser->assign('shipping_fee', SHIPPING_FEE);
        $this->parser->parse('lunch/checkout');
    }
    function orderCart() {
        $orders = $this->input->post();

        $oid = $this->mod_lunch->_getMaxOrder();
        $oid++;
        $orders['oid'] = $oid;
        $orders['user'] = $this->session->userdata('ssId');
        
        $dish = array();
        $clear = array();
        $carts = $this->cart->contents();
        foreach ($carts as $cart) {
            if ($cart['menu'] == $orders['menuID']) {
                $dish[] = array(
                    'id' => $cart['id'],
                    'quantity' => $cart['qty']
                );
                $clear[] = array(
                    'rowid' => $cart['rowid'],
                    'qty' => 0
                );
            }
        }
        $total = 0;
        if ($orders['thanhtoan'] == 2) {
            //Kiem tra tien
            $this->load->model('mod_user');
            $user = $this->mod_user->_get($this->session->userdata('ssId'));
            $cart = $this->cart->contents();
            foreach ($cart as $c) {
                if ($c['menu'] == $orders['menuID']) $total += $c['subtotal'];
            }
            if ($total < SHIPPING_VALUE) $total += SHIPPING_FEE;
            if ($user->uwallet < $total) { //Thieu tien
                echo '{"error_code":"201","message":"Bạn không đủ tiền trong tài khoản."}';
                exit;
            }
            $orders['total'] = $total;
            $orders['balance'] = $user->uwallet - $total;
        }
        if ($this->mod_lunch->_createOrder($orders, $dish)) {
            echo '{"error_code":"200","message":"Đặt hàng thành công."}';
            //clear cart
            $this->cart->update($clear);
        }
        else {
            echo '{"error_code":"201","message":"Đặt hàng không thành công."}';
        }
        exit;
    }
    function orders() {
        $this->parser->parse('lunch/orders');
    }
}