<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Error extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }
    function e404() {
        $this->parser->parse('errors/404');
    }
}