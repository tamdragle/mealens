<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Food extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }
    function index() {
        $this->parser->parse('food/index');
    }
}