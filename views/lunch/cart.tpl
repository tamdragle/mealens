{* Extend our master template *}
{extends file="home/master.tpl"}
{block name=script}
<script type="text/javascript">
$(document).ready(function() {
    $('body').addClass("checkout");
    $("#menu_lunch").addClass("active");
});
function updateCart(cartID) {
    $.ajax({
        url : "{site_url()}lunch/updCart",
        type: "POST",
        data: {
            'cartID': cartID,
            'cartVal': $('#'+cartID).val()
        },
        dataType: "JSON",
        success: function(data) {
            //alert(data.message);
            //loadCart();
            window.location.href = "{site_url()}lunch/cart";
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the cap nhat');
        }
    });
}
function delCart(cartID, cartOpt) {
    $.ajax({
        url : "{site_url()}lunch/delCart",
        type: "POST",
        data: {
            'cartID': cartID,
            'cartOpt': cartOpt
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
            //loadCart();
            window.location.href = "{site_url()}lunch/cart";
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the cap nhat');
        }
    });
}
function checkoutCart(menuID) {
    {if $login eq 'YES'}
        window.location.href = "{site_url()}lunch/checkout?menu="+menuID;
    {else}
        alert('Vui lòng đăng nhập trước');
    {/if}
}
</script>
{/block}
{block name=body}
<div id="mainvisual">
<h2>&nbsp;</h2>
</div>
<div id="main">
<div class="container">
<ul class="topic-path">
<li><a href="{site_url()}lunch">Cơm trưa</a></li>
<li>Giỏ hàng</li>
</ul>
<div class="clearfix">
<p>Nhập mã CODE để được giảm giá khi chọn order tại website:</p>
<p class="clearfix"><input type="text" placeholder="Enter your coupon here" class="coupon"><a href="" class="btn-link">MÃ GIẢM GIÁ</a></p>
</div>
<div class="tbl-scroll mb30">
<table class="checkout-cnt center">
<tr>
<th>Tên món ăn</th>
<th>Đơn giá</th>
<th>Số lượng</th>
<th>Thành tiền</th>
<th></th>
</tr>
{if $carts}
    {foreach from=$carts key=k item=cart}
        <tr><td colspan="5" class="left"><h2>{date("d-m-Y", strtotime($menu[$k]['mdate']))}</h2></td></tr>
        {assign var="total" value=0}
        {foreach from=$cart item=c}
            <tr>
            <td>
                {php}
                    if (is_file("{$path_upload}meal/thumb_{$c['pic']}")) echo "<span><img src='/{$path_upload}meal/thumb_{$c['pic']}' /></span>";
                    elseif (is_file("{$path_upload}meal/{$c['pic']}")) echo "<span><img src='/{$path_upload}meal/{$c['pic']}' /></span>";
                    else echo '';
                {/php}
                <span>{$c['name']}</span>
            </td>
            <td>{number_format($c['price'])}</td>
            <td><input id="{$c['rowid']}" value="{$c['qty']}" maxlength="3" size="10" onchange="updateCart('{$c['rowid']}')"></td>
            <td>{number_format($c['subtotal'])}</td>
            <td><a href="javascript:void(-1)" onclick="delCart('{$c['rowid']}', 'row')"><img src="/assets/images/ico_cancel_off.png" alt=""></a></td>
            </tr>
            {assign var="total" value={$total} + $c['subtotal']}
        {/foreach}
        <tr>
        <td colspan="5" class="total">
        <p><span>TỔNG CỘNG:</span> <span>{number_format($total)}</span></p>
        <ul>
            <li><a href="javascript:void(-1)" onclick="checkoutCart({$menu[$k]['mid']})" class="btn-link">Hoàn tất ({date("d/m/Y", strtotime($menu[$k]['mdate']))})</a></li>
        <li><a href="javascript:void(-1)" onclick="delCart({$menu[$k]['mid']}, 'date')">Xoá giỏ hàng ({date("d/m/Y", strtotime($menu[$k]['mdate']))})</a></li>
        </ul>
        </td>
        </tr>
    {/foreach}
{else}
    <tr>
    <td colspan="5" class="center red">Chưa có dữ liệu</td>  
    </tr>
{/if}
</table>
</div>
</div>
</div>
{/block}