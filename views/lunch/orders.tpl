{* Extend our master template *}
{extends file="home/master.tpl"}
{block name=css}
<link rel="stylesheet" href="/assets/datatables/jquery.dataTables.css" />
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
{/block}
{block name=script}
<script src="/assets/datatables/jquery.dataTables.min.js"></script>
<script src="/assets/datatables/dataTables.bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#menu_lunch").addClass("active");
    load_list();
});
/*
$("#dialogbox").dialog({
    autoOpen:false,
    modal:true,
    title: "Chi tiết đơn hàng",
    width: "60%",
    open: function( event, ui ) {
        //alert('hello');
    }
});
*/
function viewDetail(id, donhang) {
    $.get('{site_url()}profile/lunch_detail?id='+ id, {},
    function(response) {
        if (response.error_code == '201') alert(response.message);
        else $('#dialogbox').html(response);
    });
    $('#dialogbox').dialog({
        autoOpen:true,
        modal:true,
        title: "Đơn hàng của ngày "+ donhang,
        width: "60%",
        maxHeight: 600,
        position: { my: 'top', at: 'top+10' },
        open: function( event, ui ) {
            //alert('hello');
        }
    });    
}
function load_list() {
    var url = '{site_url()}profile/lunch_list';
    if (url != '') {
        table = $('#table').DataTable({ 
            "processing": true,
            "serverSide": true,
            "order": [],

            "ajax": {
                "url": url,
                "type": "POST"
            },
            "columnDefs": [
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            ],
        });
    }
}
</script>
{/block}
{block name=body}
<div id="dialogbox"></div>
<div id="mainvisual">
<h2>&nbsp;</h2>
</div>
<div id="main">
<div class="container">
    <ul class="topic-path">
        <li><a class="ajax" href="{site_url()}lunch">Cơm trưa</a></li>
        <li>Danh sách đơn hàng</li>
    </ul>
    <div class="payment clearfix">
        <p class="note-table sp">※ Di chuyển trái/phải để xem thêm</p>
        <table id="table" class="table table-bordered table-striped">
        <thead>
        <tr>
        <th>Ngày đặt</th>
        <th>Số lượng</th>
        <th>Giá tiền</th>
        <th>Menu ngày</th>
        <th>Trạng thái</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
        </table>
    </div>
</div>
</div>
{/block}