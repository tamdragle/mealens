{* Extend our master template *}
{extends file="home/master.tpl"}
{block name=script}
<script type="text/javascript">
$(document).ready(function() {
    $('body').addClass("checkout-payment");
    $("#menu_lunch").addClass("active");
});
function orderCart(menuID) {
    $.ajax({
        url : "{site_url()}lunch/orderCart",
        type: "POST",
        data: {
            'menuID': menuID,
            'tenkhachhang': $('#tenkhachhang').val(),
            'diachi': $('#diachi').val(),
            'dienthoai': $('#dienthoai').val(),
            'ghichu': $('#ghichu').val(),
            'thanhtoan': $('#thanhtoan').val()
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
            if (data.error_code == '200') window.location.href = "{site_url()}lunch/orders";
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Không thể hoàn tất');
        }
    });
}
</script>
{/block}
{block name=body}
<div id="mainvisual">
<h2>&nbsp;</h2>
</div>
<div id="main">
<div class="container">
{if $message}
    <ul class="topic-path">
    <li><a href="{site_url()}lunch">Cơm trưa</a></li>
    <li><a href="{site_url()}lunch">Giỏ hàng</a></li>
    <li>{$message}</li>
    </ul>
{else}
    <ul class="topic-path">
    <li><a href="{site_url()}lunch">Cơm trưa</a></li>
    <li><a href="{site_url()}lunch">Giỏ hàng</a></li>
    <li>Hoàn tất đơn hàng của ngày {date("d-m-Y", strtotime($menu['mdate']))}</li>
    </ul>
    <div class="payment clearfix">
    <dl class="payment-danhmuc">
    <dt>DANH MỤC ĐẶT HÀNG</dt>
    {assign var="total" value=0}
    {foreach from=$cart item=c}
        <dd>
        <ul>
        <li>
            {php}
                if (is_file("{$path_upload}meal/thumb_{$c['pic']}")) echo "<span><img src='/{$path_upload}meal/thumb_{$c['pic']}' /></span>";
                elseif (is_file("{$path_upload}meal/{$c['pic']}")) echo "<span><img src='/{$path_upload}meal/{$c['pic']}' /></span>";
                else echo '';
            {/php}
        </li>
        <li>{$c['name']}</li>
        <li>x{$c['qty']}</li>
        </ul>
        </dd>
        {assign var="total" value={$total} + $c['subtotal']}
    {/foreach}
    </dl>
    <div class="payment-cnt clearfix">
    <p>THÔNG TIN ĐẶT HÀNG</p>
    <div class="payment-cnt-l">
    <p class="mb10">TÊN KHÁCH HÀNG</p>
    <p><input type="text" id="tenkhachhang" name="tenkhachhang" placeholder="Tên của bạn" value="{$ssUser['ssFullname']}"></p>
    <p class="mb10">ĐỊA CHỈ GIAO HÀNG*</p>
    <p><input type="text" id="diachi" name="diachi" placeholder="Địa chỉ" value="{$ssUser['ssAddress']}"></p>
    <p class="mb10">SỐ ĐIỆN THOẠI *</p>
    <p><input type="text" id="dienthoai" name="dienthoai" placeholder="Điện thoại" value="{$ssUser['ssTel']}"></p>
    <p class="mb10">GHI CHÚ THÊM</p>
    <p><textarea id="ghichu" name="ghichu" placeholder="Gọi điện khi đem hàng tới"></textarea></p>
    </div>
    <div class="payment-cnt-r">
    <p class="mb10">HÌNH THỨC THANH TOÁN *</p>
    <p><select id="thanhtoan" name="thanhtoan">
            <option value="1">Thanh toán khi nhận hàng</option>
            <option value="2">Thanh toán bằng mealens</option>
    </select></p>
    {if $total < $shipping_value}
        {assign var="shipping" value=$shipping_fee}
    {else}
        {assign var="shipping" value=0}
    {/if}
    <ul class="payment-total">
    <li>GIÁ TIỀN:</li>
    <li>{number_format($total)}VNĐ</li>
    <li>PHÍ SHIPING:</li>
    <li>{number_format($shipping)}VNĐ</li>
    <li>GIẢM GIÁ:</li>
    <li>0VNĐ</li>
    <li>TỔNG CỘNG:</li>
    <li>{number_format($total + $shipping)}</li>
    </ul>
    </div>
    </div>
    </div>
    <p class="center"><a href="javascript:void(-1);" onclick="orderCart({$menu['mid']})" class="btn-link">HOÀN TẤT THÔNG TIN</a></p>
{/if}
</div>
</div>
{/block}