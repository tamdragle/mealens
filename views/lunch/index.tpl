{* Extend our master template *}
{extends file="home/master.tpl"}
{block name=script}
<script type="text/javascript">
$(document).ready(function() {
    $("#menu_lunch").addClass("active");
});
function loadCart() {
    $('#cart').html("<center><i class='ace-icon fa fa-spinner fa-spin orange bigger-125'></i></center>");
    $.get('{site_url()}lunch/viewCart', {},
    function(response) {
        if (response.error_code == '201') alert(response.message);
        else $('#cart').html(response);
    });
}
function addcart(menuID) {
    {if $login != 'YES'}
        alert('Vui lòng đăng nhập');
    {elseif $z < date('z')}
        alert('Đã quá ngày, Bạn không thể thêm vào giỏ hàng');
    {elseif ($z == date('z')) AND date('H') > 10}
        alert('Đã quá giờ, Bạn không thể thêm vào giỏ hàng');
    {else}
        $.ajax({
            url : "{site_url()}lunch/addCart",
            type: "POST",
            data: {
                'menuID': menuID
            },
            dataType: "JSON",
            success: function(data) {
                alert(data.message);
                //loadCart();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Khong the cap nhat');
            }
        });
    {/if}
}</script>
{/block}
{block name=body}
<div id="mainvisual">
    <ul>
        <li><img src="/assets/images/main_img01.jpg" alt=""></li>
        <li><img src="/assets/images/main_img01.jpg" alt=""></li>
        <li><img src="/assets/images/main_img01.jpg" alt=""></li>
    </ul>
    <div class="container">
        <p>CƠM NGON<br>MỖI NGÀY</p>
        <p></p>
    </div>
</div>
<div id="main">
    <div class="container">
        <p class="menu">THỰC ĐƠN THEO TUẦN</p>
        <ul class="menu-day">
            {foreach from=$week key=k item=v}
                <li><a href="{site_url()}lunch?z={date("z", $v)}" {if date("z", $v) == $z}class="active"{/if}>{$k}</a></li>
            {/foreach}
        </ul>
        <p class="center"><img src="/assets/images/ico_choice.jpg" alt=""></p>
        <ul class="menu-choice">
            {foreach from=$types item=t}
                <li><a href="{site_url()}lunch?z={$z}&type={$t['dtid']}" {if $t['dtid'] == $type}class="active"{/if}>{$t['dtname']}</a></li>
            {/foreach}
        </ul>
        {if $menu}
        <ul class="menu-list">
            {assign var="c" value=0}
            {foreach from=$menu item=m}
                {if (($c+1)-floor(($c+1)/2))%2 eq 0}
                    <li class="menu-list-content">
                        <a href="javascript:void(-1);" onclick="addcart({$m['id']})">
                            <p>{$m['dname']}</p>
                            <p>{$m['dfeature']}</p>
                            <p>{number_format($m['mprice'])}</p>
                            <span>CHỌN</span>
                        </a>
                    </li>
                    <li class="menu-list-img">
                        <a href="{site_url()}lunch/detail?id={$m['did']}">
                        {php}
                            if (is_file("{$path_upload}meal/thumb_{$m['dpic']}")) echo "<img src='/{$path_upload}meal/thumb_{$m['dpic']}' />";
                            elseif (is_file("{$path_upload}meal/{$m['dpic']}")) echo "<img src='/{$path_upload}meal/{$m['dpic']}' />";
                            else echo '';
                        {/php}
                        </a>
                    </li>
                {else}
                    <li class="menu-list-img">
                        <a href="{site_url()}lunch/detail?id={$m['did']}">
                        {php}
                            if (is_file("{$path_upload}meal/thumb_{$m['dpic']}")) echo "<img src='/{$path_upload}meal/thumb_{$m['dpic']}' />";
                            elseif (is_file("{$path_upload}meal/{$m['dpic']}")) echo "<img src='/{$path_upload}meal/{$m['dpic']}' />";
                            else echo '';
                        {/php}
                        </a>
                    </li>
                    <li class="menu-list-content">
                        <a href="javascript:void(-1);" onclick="addcart({$m['id']})">
                            <p>{$m['dname']}</p>
                            <p>{$m['dfeature']}</p>
                            <p>{number_format($m['mprice'])}</p>
                            <span>CHỌN</span>
                        </a>
                    </li>
                {/if}
                {assign var="c" value={$c} + 1}
            {/foreach}
        </ul>
        {else}
        <p class="menu">Chưa có thực đơn</p>
        {/if}
    </div>
    <div class="box01">
        <div class="container">
            <p class="image-r"><img src="/assets/images/idx_img01.jpg" alt=""></p>
            <p>Chúng tôi cung cấp</p>
            <p>Cơm văn phòng - giao tận nơi</p>
            <p>Đặt cơm online hoặc qua mobi app</p>
            <p class="mb10">Giao cơm bằng <span class="bold">bộ hộp cơm nhựa cao cấp</span> hoặc khay nhựa PP (không cần trả khay) tuyệt đối an toàn cho thực phẩm nóng.</p>
            <p class="mb10"><span class="bold">Giao hàng miễn phí</span> các quận: 1, 3, một phần các quận 2, 4, 5, 10, Tân Bình, Phú Nhuận, Bình Thạnh (liên hệ chúng tôi để biết chi tiết)</p>
            <p class="mb30">Vui lòng đặt cơm <span class="bold">trước 9h sáng mỗi ngày</span> để được phục vụ tốt nhất.</p>
            <span><a href="">Xem thêm để biết chi tiết</a></span>
        </div>
    </div>
    <div class="box02">
        <div class="container">
            <p>Tại sao bạn nên chọn cơm tại đây?</p>
            <p>VÌ CƠM NGON - GIÁ LẠI TỐT</p>
            <ul>
                <li>
                    <p><img src="/assets/images/idx_img03.png" alt=""></p>
                    <p>Giá hợp lý, chỉ từ 35.000 VNĐ<br>lại còn có nhiều khuyến mãi</p>
                </li>
                <li>
                    <p><img src="/assets/images/idx_img04.png" alt=""></p>
                    <p>Thực đơn phong phú,<br>đa dạng, luôn thay đổi</p>
                </li>
                <li>
                    <p><img src="/assets/images/idx_img05.png" alt=""></p>
                    <p>Nguồn gốc rõ ràng, nguyên liệu<br>tươi ngon và dinh dưỡng</p>
                </li>
            </ul>
            <span><a href="">Cung cấp suất ăn cho công ty, trường học - hỏi tại đây</a></span>
        </div>
    </div>
</div>
{/block}