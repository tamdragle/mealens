{* Extend our master template *}
{extends file="home/master.tpl"}
{block name=script}
<script type="text/javascript" src="/assets/js/gmaps.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=&callback=gmaps.renderAll"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('body').addClass("lienhe");
    $("#menu_contact").addClass("active");
});
</script>
{/block}
{block name=body}
<div id="mainvisual">
<h2>
    <div class="gMap gMapDisableScrollwheel gMapMinifyInfoWindow gMapZoom15">
        <div class="gMapCenter">
            <p class="gMapLatLng">10.791128829984036,106.68861055396724</p>
        </div>
        <div class="gMapMarker">
            <div class="gMapInfoWindow"><span class="bold">MealENS</span></div>
            <p class="gMapLatLng">10.7902857,106.68873929999995</p>
        </div>
    </div>    	
</h2>
</div>
<div id="main">
    <div class="container">
        <div class="section">
            <ul class="lh-contact">
                <li>
                    <dl>
                        <dt><img src="/assets/images/ico_map.jpg" alt=""></dt>
                        <dd>21/4 Đường số 2, Thạnh Mỹ Lợi, Q2, TP.HCM</dd>
                    </dl>
                </li>
                <li>
                    <dl>
                        <dt><img src="/assets/images/ico_call.jpg" alt=""></dt>
                        <dd>0932627357 - 0903334221</dd>
                    </dl>
                </li>
                <li>
                    <dl>
                        <dt><img src="/assets/images/ico_mail.jpg" alt=""></dt>
                        <dd><a href="mailto:contact@yourdomain.com">contact@mealens.com</a></dd>
                    </dl>
                </li>
            </ul>
            <p class="lh-ttl">“Sức khỏe của khách hàng là lợi ích của chúng tôi”</p>
        </div>
    </div>
</div>
{/block}