{* Extend our master template *}
{extends file="home/master.tpl"}
{block name=script}
<script type="text/javascript">
$(document).ready(function() {
    $('body').addClass("gioithieu");
    $("#menu_intro").addClass("active");
});
</script>
{/block}
{block name=body}
<div id="mainvisual">
<h2>&nbsp;</h2>
</div>
<div id="main">
    <div class="container">
        <div class="gioithieu-box01">
            <p><img src="/assets/images/gioithieu_img02.jpg" alt=""></p>
            <h3>GIỚI THIỆU</h3>
            <div>
                <p>Bạn là nhân viên văn phòng ? Bạn bận rộn không kịp chuẩn bị bữa trưa cho mình? Mỗi ngày bạn luôn đánh vật với câu hỏi “Trưa nay ăn gì?” Bạn ngại phải ra ngoài ăn với thời tiết lúc nắng lúc mưa của Sài Gòn? Bạn e dè khi phải dùng thực phẩm bẩn và tin tức về chúng cứ tràn lan trên mạng?...Có rất nhiều những trăn trở xoay quanh bữa trưa văn phòng hiện nay.</p>
                <p>Chúng tôi đã từng, đồng cảm, thấu hiểu và mong muốn mang đến một giải pháp hoàn hảo cho bạn. Sau quá trình cân nhắc và tìm hiểu, chúng tôi quyết định mang đến những dịch vụ tối ưu nhất với phương châm AN TOÀN – TIỆN LỢI – CHUYÊN NGHIỆP.</p>
                <p>Với dịch vụ CƠM TRƯA VĂN PHÒNG, chúng tôi mang bữa trưa gia đình đến tận công ty bạn với mức giá hợp lý nhất và nhiều ưu đãi. Được chế biến bằng nguồn nguyên liệu sạch cùng đội ngũ đầu bếp tận tâm, nhiều kinh nghiệm, đảm bảo an toàn và cam kết vệ sinh tuyệt đối, bạn hoàn toàn yên tâm khi dùng bữa.</p>
                <p>Đặc biệt hơn, với tiêu chí ăn chung càng vui, với mỗi người bạn giới thiệu bạn sẽ được tích điểm bằng 10% giá trị đơn hàng phát sinh. Số điểm tích lũy sẽ được sử dụng thanh toán đơn hàng cho chính bạn, càng đông – càng vui, bạn đã sẵn sàng thưởng thức và chia sẻ???</p>
            </div>
        </div>
    </div>
</div>
{/block}