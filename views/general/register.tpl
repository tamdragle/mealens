{* Extend our master template *}
{extends file="home/master.tpl"}
{block name=script}
<script type="text/javascript">
function register() {
    $.ajax({
        url : "{site_url()}register",
        type: "POST",
        data: {
            'isSent': 'OK',
            'name': $('#name').val(),
            'mail': $('#mail').val(),
            'tel': $('#tel').val(),
            'address': $('#address').val(),
            'ref': $('#ref').val()
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
            if (data.error_code == '200') window.location.href = "{site_url()}lunch";
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Co loi xay ra');
        }
    });
}
</script>
{/block}
{block name=body}
<div id="mainvisual">
<h2>&nbsp;</h2>
</div>
<div id="main">
    <div class="container">
        <ul class="topic-path">
        <li>VUI LÒNG ĐĂNG KÝ THÔNG TIN KHI MUA HÀNG HOẶC ĐẶT CƠM</li>
        </ul>
        <div class="section">
            <ul class="register-cnt clearfix">
                <li>
                    <p>Họ và tên *</p>
                    <p><input type="text" id="name" name="name"></p>
                </li>
                <li>
                    <p>Email *</p>
                    <p><input type="text" id="mail" name="mail"></p>
                </li>
                <li>
                    <p>Số điện thoại *</p>
                    <p><input type="text" id="tel" name="tel"></p>
                </li>
                <li>
                    <p>Địa chỉ giao nhận *</p>
                    <p><input type="text" id="address" name="address"></p>
                </li>
                <li>
                    <ul class="clearfix">
                    <li>
                        <p>Mã người giới thiệu</p>
                        <p><input type="text" id="ref" name="ref"></p>
                    </li>
                    </ul>
                    <p>* Mật khẩu mặc định là "<span class="bold">mealens</span>", vui lòng thay đổi sau khi đăng nhập.</p>
                </li>
            </ul>
            <p class="center"><a href="javascript:void(-1);" onclick="register()" class="btn-link">ĐĂNG KÝ</a></p>
        </div>
    </div>
</div>
{/block}