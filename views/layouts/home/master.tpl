<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=-100%, user-scalable=yes" />
<meta name="format-detection" content="telephone=no">
<title> MEAL - Enjoying and Sharing</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="stylesheet" href="/assets/css/styles.css">
<link rel="stylesheet" href="/assets/css/responsive.css">
<link rel="stylesheet" href="/assets/css/slick.css">
{block name=css}{/block}
</head>
<body id="index" class="under">
<div id="wrapper">
    {include file='home/header.tpl'}
    {block name=body}{/block}
    {include file='home/footer.tpl'}
</div>
<script src="/assets/js/jquery.js" type="text/javascript"></script>
<script src="/assets/js/common.js"></script>
<script src="/assets/js/slick.js"></script>
<script type="text/javascript">
function login() {
    $.ajax({
        url : "{site_url()}login",
        type: "POST",
        data: {
            'isSent': 'OK',
            'username': $('#username').val(),
            'password': $('#password').val()
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
            if (data.error_code == '200') location.reload();
            /*
            if (data.error_code == '200') {
                var res =   '<ul class="login account">'+
                            '<li><a href="{site_url()}profile">Tài khoản</a></li>'+
                            '<li><a href="{site_url()}lunch/cart">Giỏ hàng</a></li>'+
                            '<li><a href="{site_url()}logout">Thoát</a></li>'+
                            '</ul>';
                $('#profile').html(res);
            }
            */
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Khong the login');
        }
    });
}
</script>
{block name=script}{/block}
</body>
</html>