<div id="header">
    <div class="container">
    <dl class="h-top clearfix">
        <dt>
            <h1><img src="/assets/images/h_logo.png" alt="MealENS"></h1>
            <label class="menu-icon sp"><span></span></label>
        </dt>
        <dd>
            <ul class="pc">
                <li>Email: contact@mealens.com | Hotline: 0932627357 - 0903334221</li>
            </ul>
            <span id="profile">
            {if $login eq 'YES'}
                <ul class="login account">
                    <li><a href="{site_url()}profile">Tài khoản</a></li>
                    <li><a href="{site_url()}lunch/cart">Giỏ hàng</a></li>
                    <li><a href="{site_url()}logout">Thoát</a></li>
                </ul>
            {else}
                <ul class="login">
                <li class="login-btn">
                    <a href="#" class="dangnhap">Đăng nhập</a>
                    <div class="login-panel">
                        <p>ĐĂNG NHẬP TÀI KHOẢN</p>
                        <p>Tên đăng nhập *</p>
                        <p><input type="text" id="username" name="username"></p>
                        <p>Mật khẩu</p>
                        <p><input type="password" id="password" name="password"></p>
                        <p>Bạn quên mật khẩu? <a href="#">Bấm vào đây.</a></p>
                        <p class="center"><a href="javascript:void(-1)" class="btn-link" onclick="login()">ĐĂNG NHẬP</a></p>
                    </div>
                </li>
                <li><a href="{site_url()}register">Đăng ký</a></li>
                </ul>
            {/if}
            </span>
            <div class="gnavisp">
                <ul id="gnavi">
                    <li><a id="menu_intro" href="{site_url()}intro">GIỚI THIỆU</a></li>
                    <li><a id="menu_lunch" href="{site_url()}lunch">CƠM TRƯA</a></li>
                    <li><a id="menu_food" href="{site_url()}food">THỰC PHẨM SẠCH</a></li>
                    <!--<li><a id="menu_dongia" href="{site_url()}dongia">SẢN PHẨM ĐỒNG GIÁ</a></li>-->
                    <li><a id="menu_contact" href="{site_url()}contact">LIÊN HỆ</a></li>
                </ul>
            </div>
        </dd>
    </dl>
</div>
    <div class="sub-menu pc">
        <div class="container">
            <ul class="sub-menu-cnt">
                <li><span>Search</span><input type="text" placeholder="keyword"><span class="pc">or</span></li>
                <li><a>Món chính</a></li>
                <li><a>Món phụ</a></li>
                <li><a>Món thêm</a></li>
                <li><a>Tráng miệng</a></li>
            </ul>
        </div>
    </div>
</div>