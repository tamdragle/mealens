<div id="footer">
    <p id="totop"><a href="#wrapper"><img src="/assets/images/totop.jpg" alt="page top"></a></p>
    <div class="container clearfix">
        <div class="f-l">
            <p><img src="/assets/images/h_logo.png" width="280" alt=""></p>
            <ul>
                <li>21/4 Đường số 2, Thạnh Mỹ Lợi, Quận 2, TP.HCM</li>
                <li>0932627357 - 0903334221</li>
                <li><a href="mailto:contact@mealens.com">contact@mealens.com</a></li>
            </ul>
        </div>
        <div class="f-r">
            <dl>
                <dt>MENU</dt>
                <dd><a href="">Giới thiệu</a></dd>
                <dd><a href="">Thực phẩm sạch</a></dd>
            </dl>
            <dl>
                <dt>HỖ TRỢ</dt>
                <dd><a href="">Liên hệ</a></dd>
                <dd><a href="">FAQ</a></dd>
            </dl>
            <dl>
                <dt>TÀI KHOẢN</dt>
                <dd><a href="">Đăng nhập</a></dd>
                <dd><a href="">Giỏ hàng</a></dd>
                <dd><a href="">Tài khoản của bạn</a></dd>
            </dl>
            <dl class="pc">
                <dt>email thông báo</dt>
                <dd>
                    <p>Nhận email thông báo thực đơn mỗi ngày:</p>
                    <input type="text" placeholder="Email của bạn">
                </dd>
            </dl>
        </div>
    </div>
    <div class="f-b">
        <div class="container clearfix">
            <p class="copyright">Copyright &copy; 2017 Designed by MealENS. Allrights reserved.</p>
            <ul class="pc">
                <li><img src="/assets/images/ico_master.png" alt=""></li>
                <li><img src="/assets/images/ico_visa.png" alt=""></li>
                <li><img src="/assets/images/ico_paypal.png" alt=""></li>
                <li><img src="/assets/images/ico_discover.png" alt=""></li>
            </ul>
        </div>
    </div>
</div>