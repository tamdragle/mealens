<div class="tbl-scroll mb30">
<table class="checkout-cnt center">
<tr>
<th>Tên món ăn</th>
<th>Đơn giá</th>
<th>Số lượng</th>
<th>Thành tiền</th>
</tr>
{assign var="soluong" value=0}
{assign var="giatien" value=0}
{foreach from=$details item=d}
    <tr>
    <td>
        {php}
            if (is_file("{$path_upload}meal/thumb_{$d['dpic']}")) echo "<span><img src='/{$path_upload}meal/thumb_{$d['dpic']}' /></span>";
            elseif (is_file("{$path_upload}meal/{$d['dpic']}")) echo "<span><img src='/{$path_upload}meal/{$d['dpic']}' /></span>";
            else echo '';
        {/php}
        <span>{$d['dname']}</span>
    </td>
    <td>{number_format($d['gia'])}</td>
    <td>{$d['soluong']}</td>
    <td>{number_format($d['tong'])}</td>
    </tr>
    {assign var="soluong" value={$soluong} + $d['soluong']}
    {assign var="giatien" value={$giatien} + $d['tong']}
{/foreach}
</table>
</div>
{if $giatien < $shipping_value}
    {assign var="shipping" value=$shipping_fee}
{else}
    {assign var="shipping" value=0}
{/if}
<div class="tbl-scroll mb30">
<table class="checkout-cnt center">
<tr>
<th>THÔNG TIN ĐƠN HÀNG</th>
</tr>
<tr>
<td>
<ul>
    <li><span>Ngày đăng ký:</span><span class="txt-green">{date("d/m/Y", strtotime($detail['modate']))}</span></li>
    <li><span>Người nhận hàng:</span><span class="txt-green">{$detail['moname']}</span></li>
    <li><span>Địa chỉ nhận hàng:</span><span class="txt-green">{$detail['moaddress']}</span></li>
    <li><span>Điện thoại người nhận:</span><span class="txt-green">{$detail['motel']}</span></li>
    <li><span>Số lượng:</span><span class="txt-green">{$soluong}</span></li>
    <li><span>Số tiền:</span><span class="txt-green">{number_format($giatien)}</span></li>
    <li><span>Shipping:</span><span class="txt-green">{number_format($shipping)}</span></li>
    <li><span>Thành tiền:</span><span class="txt-green">{number_format($giatien + $shipping)}</span></li>
</ul>        
</td>
</tr>
</table>
</div>
