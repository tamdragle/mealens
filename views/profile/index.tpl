{* Extend our master template *}
{extends file="home/master.tpl"}
{block name=css}
<link rel="stylesheet" href="/assets/datatables/jquery.dataTables.css" />
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
{/block}
{block name=script}
<script src="/assets/datatables/jquery.dataTables.min.js"></script>
<script src="/assets/datatables/dataTables.bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript">
function reload_table() {
    table.ajax.reload(null,false); //reload datatable ajax 
}
function load(list) {
    $('#loadList').html("<center><img src='/assets/images/loading.gif' /></center>");
    var url = '';
    switch (list) {
        case 'friend':
            url = '{site_url()}profile/friends';
            $("#mfriend").addClass("active");
            $("#mlunch").removeClass("active");
            $("#mother").removeClass("active");
            break;
        case 'wallet':
            url = '{site_url()}profile/wallet';
            $("#mfriend").removeClass("active");
            $("#mlunch").removeClass("active");
            $("#mother").removeClass("active");
            break;
        case 'lunch':
            url = '{site_url()}profile/lunch';
            $("#mfriend").removeClass("active");
            $("#mlunch").addClass("active");
            $("#mother").removeClass("active");
            break;
        case 'order':
            url = '{site_url()}profile/friends';
            $("#mfriend").removeClass("active");
            $("#mlunch").removeClass("active");
            $("#mother").addClass("active");
            break;
    }
    if (url != '') {
        $.get(url, {}, 
        function(response) {
            $('#loadList').html(response);
        });
    }
    else alert('Chuc nang khong ton tai!');
}
function load_list(list) {
    var url;
    switch (list) {
        case 'friend':
            url = '{site_url()}profile/friends_list';
            break;
        case 'lunch':
            url = '{site_url()}profile/lunch_list';
            break;
        case 'wallet':
            url = '{site_url()}profile/wallet_list';
            break;
    }
    if (url != '') {
        table = $('#table').DataTable({ 
            "processing": true,
            "serverSide": true,
            "order": [],

            "ajax": {
                "url": url,
                "type": "POST"
            },
            "columnDefs": [
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            ],
        });
    }
}
function viewDetail(id, donhang) {
    $.get('{site_url()}profile/lunch_detail?id='+ id, {},
    function(response) {
        if (response.error_code == '201') alert(response.message);
        else $('#dialogbox').html(response);
    });
    $('#dialogbox').dialog({
        autoOpen:true,
        modal:true,
        title: "Đơn hàng của ngày "+ donhang,
        width: "60%",
        maxHeight: 400,
        position: { my: 'top', at: 'top+10' },
        open: function( event, ui ) {
            //alert('hello');
        }
    });    
}
function doimatkhau() {
    $.get('{site_url()}profile/chgPwd', {},
    function(response) {
        $('#xxx').html(response);
    });
}
function chgpwd() {
    $.ajax({
        url : "{site_url()}profile/chgPwd",
        type: "POST",
        data: {
            'isSent': 'OK',
            'oldpwd': $('#oldpwd').val(),
            'newpwd': $('#newpwd').val(),
            'newpwd_confirm': $('#newpwd_confirm').val()
        },
        dataType: "JSON",
        success: function(data) {
            alert(data.message);
            if (data.error_code == '200') window.location.href = "{site_url()}profile";
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Co loi xay ra');
        }
    });
}
</script>
{/block}
{block name=body}
<div id="dialogbox"></div>
<div id="mainvisual">
<h2>&nbsp;</h2>
</div>
<div id="main">
<div class="container">
<ul class="topic-path">
<li><a href="./">Trang chủ</a></li>
<li><a href="">Tài khoản của tôi</a></li>
<li>Thông tin cá nhân</li>
</ul>
<div class="myacc-box">
    <ul class="myacc-ttl">
        <li>THÔNG TIN CỦA BẠN</li>
        <li><a href="javascript:void(-1);" onclick="doimatkhau();">Đổi mật khẩu</a></li>
    </ul>
    <div id="xxx">
        <div class="myacc-info clearfix">
            <ul class="myacc-navi">
                <li><a id="mlunch" href="javascript:void(-1);" onclick="load('lunch')">ĐƠN HÀNG CƠM TRƯA</a></li>
                <li><a id="mother" href="javascript:void(-1);">ĐƠN HÀNG KHÁC</a></li>
                <li><a id="mfriend" href="javascript:void(-1);" onclick="load('friend')">DANH SÁCH BẠN BÈ</a></li>
            </ul>
            <div class="myacc-cnt reverse-col">
            <dl class="acc-info">
            <dd>
            <ul>
                <li><span>Ngày tham gia:</span><span>{date("d/m/Y", strtotime($user->ureg))}</span></li>
                <li><span>Email:</span><span class="txt-green">{$user->umail}</span></li>
                <li><span>Điện thoại</span><span>{$user->utel}</span></li>
                <li><span>Địa chỉ giao nhận:</span><span>{$user->uaddress}</span></li>
                <li><span>Tiền khả dụng:</span><a href="javascript:void(-1);" onclick="load('wallet')">{number_format($user->uwallet)}</a></li>
            </ul>
            </dd>
            </dl>
            <p class="myacc-avatar"><img src="/assets/images/myavatar.jpg" alt=""><br><a href="javascript:void(-1);">{$user->uname}</a></p>
            </div>
        </div>
        <div class="friend-list">
            <div class="tbl-scroll" id="loadList"></div>
        </div>
    </div>
</div>
</div>
</div>
{/block}