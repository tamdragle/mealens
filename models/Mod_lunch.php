<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
get_instance()->load->iface('DefaultInterface');
class Mod_lunch extends MY_Model implements DefaultInterface {
    var $id = FALSE;
    var $menu = FALSE;
    var $z = FALSE;
    var $year = FALSE;
    var $type = FALSE;

    var $table = 'orders_meal_list_by_user';
    var $uid = FALSE;
    var $column = array('modate','soluong','tong','mdate','mostatus'); //set column field database for order and search
    var $order = array('modate' => 'desc'); // default order 

    //Datatable
    private function _get_datatables_query() {
        $this->db->from($this->table);
        $this->db->where('uid', $this->uid);
        $i = 0;
        foreach ($this->column as $item) {//loop column 
            if (@$_POST['search']['value']) {//if datatable send POST for search
                if ($i === 0 ) {//first loop
                    $this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND. 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                //last loop
                if (count($this->column) - 1 == $i) $this->db->group_end(); //close bracket
            }
            $column[$i] = $item; //set column array variable to order processing
            $i++;
        }
        if (isset($_POST['order'])) {//here order processing
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        elseif (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables() {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all() {
        $this->db->from($this->table);
        $this->db->where('uid', $this->uid);
        return $this->db->count_all_results();
    }
    //End datatables

    function _get() {
        $this->db->select('*');
        $this->db->from('menu');
        if ($this->id) $this->db->where('mid', $this->id);
        else {
            $this->db->where('mz', $this->z);
            $this->db->where('myear', $this->year);
        }
        $query = $this->db->get();
        $result = $query->row_array();
        $query->free_result();
	return ($result) ? $result : FALSE;                
    }
    function _getType($type = FALSE) {
        $this->db->select('*');
        $this->db->from('dish_type');
        if ($type) $this->db->where('dtid', $type);
        $query = $this->db->get();
        $result = ($type) ? $query->row_array() : $query->result_array();
        $query->free_result();
	return ($result) ? $result : false;                
    }
    function _getDetail() {
        $this->db->select('m.*, d.dname, d.dpic, dfeature');
        $this->db->from('menu_detail AS m');
        $this->db->join('dish AS d', 'm.did = d.did');
        if ($this->id) {
            $this->db->where('m.id', $this->id);
        }
        else {
            $this->db->where('m.mid', $this->menu);
        }
        if ($this->type) $this->db->where('d.dtid', $this->type);
        $query = $this->db->get();
        $result = ($this->id) ? $query->row_array() : $query->result_array();
        $query->free_result();
	return ($result) ? $result : FALSE;        
    }
    function _getOrders($uid) {
        $this->db->select('*');
        $this->db->from('orders_meal_list_by_user');
        $this->db->where('uid', $uid);
        $query = $this->db->get();
        $result = $query->result_array();
        $query->free_result();
	return ($result) ? $result : false;
    }
    function _getOrderDetail($id) {
        $this->db->select('*');
        $this->db->from('meal_orders');
        $this->db->where('moid', $id);
        $query = $this->db->get();
        $result = $query->row_array();
        $query->free_result();
	return ($result) ? $result : FALSE;        
    }
    function _getOrderDetails($id) {
        $this->db->select('o.gia, o.soluong, o.tong, d.dname, d.dpic');
        $this->db->from('orders_meal AS o');
        $this->db->join('dish AS d', 'o.id_monan = d.did');
        $this->db->where('o.id_donhang', $id);
        $query = $this->db->get();
        $result = $query->result_array();
        $query->free_result();
	return ($result) ? $result : FALSE;        
    }
    function _getMaxOrder() {
        $this->db->select_max('moid', 'maxid');
        $this->db->from('meal_orders');
        $query = $this->db->get();
        $result = $query->row_array();
        $query->free_result();
	return ($result) ? $result['maxid'] : false;        
    }
    function _createOrder($orders, $dish) {
        $this->db->trans_start();
        $status = 0;
        //Tru tien neu thanh toan bang mealens
        if ($orders['thanhtoan'] == 2) {
            //Log mealens
            if (!$this->db->insert('mealens_logs', array(
                'lvalue' => $orders['total'],
                'moid' => $orders['oid'],
                'uid' => $orders['user']
            ))) return FALSE;
            //Cap nhat vi
            $this->db->set('uwallet', 'uwallet - ' . $orders['total'], FALSE);
            $this->db->where('uid', $orders['user']);
            $this->db->update('users');
            //Luu transaction
            if (!$this->db->insert('users_transaction', array(
                'tranvalue' => $orders['total'],
                'tranbalance' => $orders['balance'],
                'trantype' => 2,
                'uid' => $orders['user'],
                'aid' => $orders['user'],
                'moid' => $orders['oid']
            ))) return FALSE;
            $status = 6;
        }

        if (!$this->db->insert('meal_orders', array(
            'moid' => $orders['oid'],
            'mostatus' => $status,
            'motype' => $orders['thanhtoan'],
            'moname' => $orders['tenkhachhang'],
            'moaddress' => $orders['diachi'],
            'motel' => $orders['dienthoai'],
            'monote' => $orders['ghichu'],
            'mid' => $orders['menuID'],
            'uid' => $orders['user']
        ))) return FALSE;
        $insDish = array();
        foreach ($dish as $d) {
            $insDish[] = array(
                'moid' => $orders['oid'],
                'moquantity' => $d['quantity'],
                'did' => $d['id']
            );
        }
        if (count($insDish) > 0) $this->db->insert_batch('meal_orders_detail', $insDish);
        $this->db->trans_complete();
        return TRUE;
    }
}
/* End of file */