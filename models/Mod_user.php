<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
get_instance()->load->iface('DefaultInterface');
class Mod_user extends MY_Model implements DefaultInterface {
    var $parent = FALSE;
    var $table = 'users';
    var $column = array('uname','umail','utel','ureg'); //set column field database for order and search
    var $order = array('uid' => 'desc'); // default order 

    //Datatable
    private function _get_datatables_query() {
        $this->db->from($this->table);
        $this->db->where('uidparent', $this->parent);
        $i = 0;
        foreach ($this->column as $item) {//loop column 
            if (@$_POST['search']['value']) {//if datatable send POST for search
                if ($i === 0 ) {//first loop
                    $this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND. 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                //last loop
                if (count($this->column) - 1 == $i) $this->db->group_end(); //close bracket
            }
            $column[$i] = $item; //set column array variable to order processing
            $i++;
        }
        if (isset($_POST['order'])) {//here order processing
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        elseif (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables() {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all() {
        $this->db->from($this->table);
        $this->db->where('uidparent', $this->parent);
        return $this->db->count_all_results();
    }
    //End datatables
    
    function _get($id) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('uid', $id);
        $query = $this->db->get();
        $result = $query->first_row();
        $query->free_result();
	return ($result) ? $result : false;
    }
    function _getSalt($id) {
        $this->db->select('uid, usalt');
        $this->db->from('users');
        $this->db->or_where('umail', $id);
        $this->db->or_where('utel', $id);
        $query = $this->db->get();
        return $query->row_array();
    }
    function _getRef($id) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->or_where('umail', $id);
        $this->db->or_where('utel', $id);
        $query = $this->db->get();
        $result = $query->row_array();
        $query->free_result();
        return ($result) ? $result : FALSE;
    }
    function _login($login) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('uid', $login['uid']);
        $this->db->where('upwd', md5(md5($login['password']).$login['salt']));
        $query = $this->db->get();
        return $query->row_array();
    }
    function _register($values) {
        $this->db->trans_start();
        $insert = array(
            'uidparent' => $values['parent'],
            'uref' => $values['ref'],
            'ulevel' => $values['level'],
            'uname' => $values['name'],
            'umail' => $values['mail'],
            'utel' => $values['tel'],
            'uaddress' => $values['address'],
            'upwd' => md5(md5($values['pwd']).$values['salt']),
            'upayment' => md5(md5($values['pwd']).$values['salt']),
            'usalt' => $values['salt']
        );
        if (isset($values['wallet'])) $insert['uwallet'] = $values['wallet'];
        $this->db->insert('users', $insert);
        $uid = $this->db->insert_id();
        if (isset($values['wallet'])) {
            $this->db->insert('users_transaction', array(
                'tranvalue' => $values['wallet'],
                'tranbalance' => $values['wallet'],
                'trantype' => 5,
                'uid' => $uid,
                'aid' => $uid
            ));
        }
        $this->db->trans_complete();
        return TRUE;
    }
    function _chgPwd($values) {
        switch ($values['type']) {
            case 'login':
                $data = array(
                    'upwd' => md5(md5($values['pwd']).$values['salt'])
                );
                break;
            case 'payment':
                $data = array(
                    'upayment' => md5(md5($values['pwd']).$values['salt'])
                );
                break;
        }
        $this->db->where('uid', $values['id']);
        $this->db->update('users', $data);
        return ($this->db->affected_rows()) ? TRUE : FALSE;                
    }
    function _isExists($field, $val) {
        switch ($field) {
            case 'email':
                $key = 'umail';
                break;
            case 'tel':
                $key = 'utel';
                break;
        }
        $this->db->select('uid');
        $this->db->where($key, $val);
        $query = $this->db->get('users');
        return ($query->num_rows() > 0) ? TRUE : FALSE;
    }
}
/* End of file */