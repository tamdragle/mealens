$(function(){
	"use strict";
	var obj = {
		init : function(){
			this.smoothScroll();
			this.toTop();
			this.TopSlide();
			this.Gnavi();
		},
		
		smoothScroll : function(){
			$('a[href^="#"]').click(function () {
				if ($($(this).attr('href')).length) {
					var p = $($(this).attr('href')).offset();
						$('html,body').animate({
							scrollTop: p.top - 83
						}, 600);
				}
				return false;
			});
			var hash1= location.hash;
			var $root = $('html, body');
			if(hash1!==""){  
				var top01 = $(hash1).offset().top;
				if($(window).width() > 768){   
					$root.animate({ scrollTop:top01 - 83}, 600);
				}else{
					$root.animate({ scrollTop:top01 - 15}, 600);
				}
			}
		},
		
		toTop : function(){
			$("#totop").hide();
			$(window).scroll(function () {
				if ($(this).scrollTop() > 100) {
					$('#totop').fadeIn();
					$('.h-top dt').css('border-bottom','#ccc 1px solid');
				} else {
					$('#totop').fadeOut();
					$('.h-top dt').css('border-bottom','');
				}
			});
		},
		TopSlide : function(){
			$("#index #mainvisual ul").slick({
				slideToShow: 1,
				slidesToScroll: 1,
				infinite: true,
				centerMode: true,
				arrows: true,
				nextArrow: '<img class="slick-next" src="/assets/images/ico_next.png" alt="next">',
				prevArrow: '<img class="slick-prev" src="/assets/images/ico_previous.png" alt="previous">',
				dots: false,
				autoplay: true,
				variableWidth: true
			});
		},
		Gnavi : function(){
			$('.menu-icon').click(function(){
				$(this).toggleClass('active');
				$('.gnavisp').toggleClass('fixed');
			});
			$(window).bind("load resize scroll", function() {
				var vW = $(window).width();
				if(vW > 768){
					$('.menu-icon').removeClass('active');
					$('#gnavi').removeAttr('style');
					$('.gnavisp').removeClass('fixed');
				}
				else {
					$('#gnavi a').removeClass('active');
				}
			});
			$('.dangnhap').click(function(){
				$('.login-panel').slideToggle();
			});
			
		},
	
	};
	
	obj.init();
	
});